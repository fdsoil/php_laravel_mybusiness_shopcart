<?php

// Api url consumed from the backend by this App
return [    
    'client' => env('API_URL_CLIENT', ''),
    'common' => env('API_URL_COMMON', ''),
    'product' => env('API_URL_PRODUCT', '')    
];
