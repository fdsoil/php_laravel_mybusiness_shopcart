const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js').sourceMaps();
mix.js('resources/js/bootstrap.js', 'public/js');
mix.js('resources/js/appVue.js', 'public/js');

mix.js('resources/js/admin/menu/list.js', 'public/js/admin/menu');
mix.js('resources/js/admin/menu/create.js', 'public/js/admin/menu');
mix.js('resources/js/admin/menu/update.js', 'public/js/admin/menu');
mix.js('resources/js/admin/role/list.js', 'public/js/admin/role');
mix.js('resources/js/admin/role/create.js', 'public/js/admin/role');
mix.js('resources/js/admin/role/update.js', 'public/js/admin/role');

mix.js('resources/js/modules/Data.js', 'public/js/modules');
mix.js('resources/js/modules/Dates.js', 'public/js/modules');
mix.js('resources/js/modules/Dom.js', 'public/js/modules');
mix.js('resources/js/modules/FormatNumber.js', 'public/js/modules');
mix.js('resources/js/modules/Http.js', 'public/js/modules');
mix.js('resources/js/modules/Notification.js', 'public/js/modules');
mix.js('resources/js/modules/Tabs.js', 'public/js/modules');
mix.js('resources/js/modules/Table.js', 'public/js/modules');
mix.js('resources/js/modules/Times.js', 'public/js/modules');
mix.js('resources/js/modules/WaitElement.js', 'public/js/modules');

mix.js('resources/js/fdsoil/Arr.js', 'public/js/fdsoil');
mix.js('resources/js/fdsoil/Constraints.js', 'public/js/fdsoil');
mix.js('resources/js/fdsoil/msg.js', 'public/js/fdsoil');
mix.js('resources/js/fdsoil/Number.js', 'public/js/fdsoil');
mix.js('resources/js/fdsoil/Modal.js', 'public/js/fdsoil');
mix.js('resources/js/fdsoil/pad.js', 'public/js/fdsoil');
mix.js('resources/js/fdsoil/Select.js', 'public/js/fdsoil');
mix.js('resources/js/fdsoil/SelectMultiple.js', 'public/js/fdsoil');
mix.js('resources/js/fdsoil/Request.js', 'public/js/fdsoil');

mix.js('resources/js/Service/Service.js', 'public/js/Service');

mix.js('resources/js/Replacement/Replacement.js', 'public/js/Replacement');
mix.js('resources/js/Replacement/Detail.js', 'public/js/Replacement');
mix.js('resources/js/Replacement/Search.js', 'public/js/Replacement');

mix.js('resources/js/inicio.js', 'public/js');
mix.js('resources/js/init.js', 'public/js');
mix.js('resources/js/home.js', 'public/js');

mix.sass('resources/sass/app.scss', 'public/css');
