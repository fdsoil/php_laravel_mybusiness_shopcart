/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 12);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/modules/FormatNumber.js":
/*!**********************************************!*\
  !*** ./resources/js/modules/FormatNumber.js ***!
  \**********************************************/
/*! exports provided: FormatNumber */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"FormatNumber\", function() { return FormatNumber; });\nvar FormatNumber = function () {\n  return {\n    put: function put() {\n      var numVal = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;\n      var sepMil = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '.';\n      var sepDec = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : ',';\n      var cantDec = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;\n      var numStr = numVal.toFixed(cantDec).toString();\n      var regExp = /\\./;\n      var array = [];\n      if (regExp.test(numStr)) array = numStr.split('.');else {\n        array[0] = numStr;\n        array[1] = '0';\n      }\n      regExp = /(\\d+)(\\d{3})/;\n\n      while (regExp.test(array[0])) {\n        array[0] = array[0].replace(regExp, '$1' + sepMil + '$2');\n      }\n\n      return array[0] + (cantDec != 0 ? sepDec + array[1] : '');\n    },\n    \"delete\": function _delete(val1) {\n      var val2 = '';\n\n      for (var i = 0; i < val1.length; i++) {\n        if (val1.charAt(i) != '.') {\n          val2 = val2 + val1.charAt(i);\n        }\n      }\n\n      return val2.replace(',', '.');\n    }\n  };\n}();//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvbW9kdWxlcy9Gb3JtYXROdW1iZXIuanM/ODg2ZiJdLCJuYW1lcyI6WyJGb3JtYXROdW1iZXIiLCJwdXQiLCJudW1WYWwiLCJzZXBNaWwiLCJzZXBEZWMiLCJjYW50RGVjIiwibnVtU3RyIiwidG9GaXhlZCIsInRvU3RyaW5nIiwicmVnRXhwIiwiYXJyYXkiLCJ0ZXN0Iiwic3BsaXQiLCJyZXBsYWNlIiwidmFsMSIsInZhbDIiLCJpIiwibGVuZ3RoIiwiY2hhckF0Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQU8sSUFBTUEsWUFBWSxHQUFJLFlBQU07QUFFakMsU0FBTztBQUVIQyxPQUFHLEVBQUUsZUFBK0Q7QUFBQSxVQUFyREMsTUFBcUQsdUVBQTVDLENBQTRDO0FBQUEsVUFBekNDLE1BQXlDLHVFQUFoQyxHQUFnQztBQUFBLFVBQTNCQyxNQUEyQix1RUFBbEIsR0FBa0I7QUFBQSxVQUFiQyxPQUFhLHVFQUFILENBQUc7QUFDbEUsVUFBSUMsTUFBTSxHQUFHSixNQUFNLENBQUNLLE9BQVAsQ0FBZUYsT0FBZixFQUF3QkcsUUFBeEIsRUFBYjtBQUNBLFVBQUlDLE1BQU0sR0FBRyxJQUFiO0FBQ0EsVUFBSUMsS0FBSyxHQUFHLEVBQVo7QUFDQSxVQUFJRCxNQUFNLENBQUNFLElBQVAsQ0FBWUwsTUFBWixDQUFKLEVBQ0VJLEtBQUssR0FBR0osTUFBTSxDQUFDTSxLQUFQLENBQWEsR0FBYixDQUFSLENBREYsS0FFSztBQUNIRixhQUFLLENBQUMsQ0FBRCxDQUFMLEdBQVdKLE1BQVg7QUFDQUksYUFBSyxDQUFDLENBQUQsQ0FBTCxHQUFXLEdBQVg7QUFDRDtBQUNERCxZQUFNLEdBQUMsY0FBUDs7QUFDSCxhQUFPQSxNQUFNLENBQUNFLElBQVAsQ0FBWUQsS0FBSyxDQUFDLENBQUQsQ0FBakIsQ0FBUDtBQUNLQSxhQUFLLENBQUMsQ0FBRCxDQUFMLEdBQVdBLEtBQUssQ0FBQyxDQUFELENBQUwsQ0FBU0csT0FBVCxDQUFpQkosTUFBakIsRUFBeUIsT0FBT04sTUFBUCxHQUFnQixJQUF6QyxDQUFYO0FBREw7O0FBRUEsYUFBT08sS0FBSyxDQUFDLENBQUQsQ0FBTCxJQUFlTCxPQUFPLElBQUksQ0FBYixHQUFtQkQsTUFBTSxHQUFHTSxLQUFLLENBQUMsQ0FBRCxDQUFqQyxHQUF1QyxFQUFwRCxDQUFQO0FBQ0UsS0FoQkU7QUFrQkgsY0FBUSxpQkFBU0ksSUFBVCxFQUFlO0FBQ3hCLFVBQUlDLElBQUksR0FBQyxFQUFUOztBQUNBLFdBQU0sSUFBSUMsQ0FBQyxHQUFDLENBQVosRUFBZUEsQ0FBQyxHQUFJRixJQUFJLENBQUNHLE1BQXpCLEVBQWtDRCxDQUFDLEVBQW5DLEVBQXVDO0FBQ3JDLFlBQUlGLElBQUksQ0FBQ0ksTUFBTCxDQUFZRixDQUFaLEtBQWdCLEdBQXBCLEVBQXdCO0FBQ25CRCxjQUFJLEdBQUNBLElBQUksR0FBQ0QsSUFBSSxDQUFDSSxNQUFMLENBQVlGLENBQVosQ0FBVjtBQUNQO0FBQ0M7O0FBQ0QsYUFBT0QsSUFBSSxDQUFDRixPQUFMLENBQWEsR0FBYixFQUFrQixHQUFsQixDQUFQO0FBQ0U7QUExQkUsR0FBUDtBQThCRCxDQWhDMkIsRUFBckIiLCJmaWxlIjoiLi9yZXNvdXJjZXMvanMvbW9kdWxlcy9Gb3JtYXROdW1iZXIuanMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY29uc3QgRm9ybWF0TnVtYmVyID0gKCgpID0+IHtcblxuICByZXR1cm4geyAgXG4gICAgICBcbiAgICAgIHB1dDogZnVuY3Rpb24gKG51bVZhbCA9IDAsIHNlcE1pbCA9ICcuJywgc2VwRGVjID0gJywnLCBjYW50RGVjID0gMCkgeyAgICAgICAgXG4gICAgICAgIGxldCBudW1TdHIgPSBudW1WYWwudG9GaXhlZChjYW50RGVjKS50b1N0cmluZygpO1xuICAgICAgICBsZXQgcmVnRXhwID0gL1xcLi87XG4gICAgICAgIGxldCBhcnJheSA9IFtdOyAgICAgICBcbiAgICAgICAgaWYgKHJlZ0V4cC50ZXN0KG51bVN0cikpXG4gICAgICAgICAgYXJyYXkgPSBudW1TdHIuc3BsaXQoJy4nKTtcbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgYXJyYXlbMF0gPSBudW1TdHI7XG4gICAgICAgICAgYXJyYXlbMV0gPSAnMCc7XG4gICAgICAgIH1cbiAgICAgICAgcmVnRXhwPS8oXFxkKykoXFxkezN9KS87XG5cdCAgICB3aGlsZSAocmVnRXhwLnRlc3QoYXJyYXlbMF0pKSAgICAgICAgICAgXG4gICAgICAgICAgYXJyYXlbMF0gPSBhcnJheVswXS5yZXBsYWNlKHJlZ0V4cCwgJyQxJyArIHNlcE1pbCArICckMicpO1xuXHQgICAgcmV0dXJuIGFycmF5WzBdICsgKCAoIGNhbnREZWMgIT0gMCApID8gc2VwRGVjICsgYXJyYXlbMV0gOiAnJyApO1xuICAgICAgfSxcblxuICAgICAgZGVsZXRlOiBmdW5jdGlvbih2YWwxKSB7XG5cdCAgICBsZXQgdmFsMj0nJztcblx0ICAgIGZvciAoIGxldCBpPTA7IGkgPCAodmFsMS5sZW5ndGgpOyBpKysgKXtcblx0ICAgICAgaWYgKHZhbDEuY2hhckF0KGkpIT0nLicpe1xuICAgICAgICAgICAgdmFsMj12YWwyK3ZhbDEuY2hhckF0KGkpO1xuXHRcdCAgfVxuXHQgICAgfVxuXHQgICAgcmV0dXJuIHZhbDIucmVwbGFjZSgnLCcsICcuJyk7XG4gICAgICB9XG4gIFxuICB9XG5cbn0pKCk7XG5cblxuIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./resources/js/modules/FormatNumber.js\n");

/***/ }),

/***/ 12:
/*!****************************************************!*\
  !*** multi ./resources/js/modules/FormatNumber.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/laravel_mybusiness_shopcart/resources/js/modules/FormatNumber.js */"./resources/js/modules/FormatNumber.js");


/***/ })

/******/ });