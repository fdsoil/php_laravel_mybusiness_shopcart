/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 10);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/modules/Dates.js":
/*!***************************************!*\
  !*** ./resources/js/modules/Dates.js ***!
  \***************************************/
/*! exports provided: Dates */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Dates\", function() { return Dates; });\nvar Dates = function () {\n  return {\n    today: function today(format, separator) {\n      var f = new Date();\n      separator = separator == null ? '' : separator;\n\n      switch (format.toUpperCase()) {\n        case 'YYYYMMDD':\n          return f.getFullYear() + separator + pad(f.getMonth() + 1, 2) + separator + pad(f.getDate(), 2);\n          break;\n\n        case 'YYMMDD':\n          return f.getFullYear().toString().substr(2, 2) + separator + pad(f.getMonth() + 1, 2) + separator + pad(f.getDate(), 2);\n          break;\n\n        case 'DDMMYYYY':\n          return pad(f.getDate(), 2) + separator + pad(f.getMonth() + 1, 2) + separator + f.getFullYear();\n          break;\n\n        case 'DDMMYY':\n          return pad(f.getDate(), 2) + separator + pad(f.getMonth() + 1, 2) + separator + f.getFullYear().toString().substr(2, 2);\n          break;\n\n        default:\n          return f;\n      }\n    }\n  };\n}();//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvbW9kdWxlcy9EYXRlcy5qcz83YjIyIl0sIm5hbWVzIjpbIkRhdGVzIiwidG9kYXkiLCJmb3JtYXQiLCJzZXBhcmF0b3IiLCJmIiwiRGF0ZSIsInRvVXBwZXJDYXNlIiwiZ2V0RnVsbFllYXIiLCJwYWQiLCJnZXRNb250aCIsImdldERhdGUiLCJ0b1N0cmluZyIsInN1YnN0ciJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFPLElBQU1BLEtBQUssR0FBSSxZQUFNO0FBRTFCLFNBQU87QUFFTEMsU0FGSyxpQkFFQ0MsTUFGRCxFQUVTQyxTQUZULEVBRW9CO0FBQ3ZCLFVBQUlDLENBQUMsR0FBRyxJQUFJQyxJQUFKLEVBQVI7QUFDQUYsZUFBUyxHQUFHQSxTQUFTLElBQUksSUFBYixHQUFvQixFQUFwQixHQUF5QkEsU0FBckM7O0FBQ0EsY0FBU0QsTUFBTSxDQUFDSSxXQUFQLEVBQVQ7QUFDRSxhQUFLLFVBQUw7QUFDRSxpQkFBT0YsQ0FBQyxDQUFDRyxXQUFGLEtBQWtCSixTQUFsQixHQUE4QkssR0FBRyxDQUFJSixDQUFDLENBQUNLLFFBQUYsS0FBZSxDQUFuQixFQUF5QixDQUF6QixDQUFqQyxHQUFnRU4sU0FBaEUsR0FBNEVLLEdBQUcsQ0FBRUosQ0FBQyxDQUFDTSxPQUFGLEVBQUYsRUFBZSxDQUFmLENBQXRGO0FBQ0E7O0FBQ0YsYUFBSyxRQUFMO0FBQ0UsaUJBQU9OLENBQUMsQ0FBQ0csV0FBRixHQUFnQkksUUFBaEIsR0FBMkJDLE1BQTNCLENBQWtDLENBQWxDLEVBQW9DLENBQXBDLElBQXlDVCxTQUF6QyxHQUFxREssR0FBRyxDQUFJSixDQUFDLENBQUNLLFFBQUYsS0FBZSxDQUFuQixFQUF5QixDQUF6QixDQUF4RCxHQUF1Rk4sU0FBdkYsR0FBbUdLLEdBQUcsQ0FBRUosQ0FBQyxDQUFDTSxPQUFGLEVBQUYsRUFBZSxDQUFmLENBQTdHO0FBQ0E7O0FBQ0YsYUFBSyxVQUFMO0FBQ0UsaUJBQVFGLEdBQUcsQ0FBRUosQ0FBQyxDQUFDTSxPQUFGLEVBQUYsRUFBZSxDQUFmLENBQUgsR0FBd0JQLFNBQXhCLEdBQW9DSyxHQUFHLENBQUlKLENBQUMsQ0FBQ0ssUUFBRixLQUFlLENBQW5CLEVBQXlCLENBQXpCLENBQXZDLEdBQXNFTixTQUF0RSxHQUFrRkMsQ0FBQyxDQUFDRyxXQUFGLEVBQTFGO0FBQ0E7O0FBQ0YsYUFBSyxRQUFMO0FBQ0UsaUJBQVFDLEdBQUcsQ0FBRUosQ0FBQyxDQUFDTSxPQUFGLEVBQUYsRUFBZSxDQUFmLENBQUgsR0FBd0JQLFNBQXhCLEdBQW9DSyxHQUFHLENBQUlKLENBQUMsQ0FBQ0ssUUFBRixLQUFlLENBQW5CLEVBQXlCLENBQXpCLENBQXZDLEdBQXNFTixTQUF0RSxHQUFrRkMsQ0FBQyxDQUFDRyxXQUFGLEdBQWdCSSxRQUFoQixHQUEyQkMsTUFBM0IsQ0FBbUMsQ0FBbkMsRUFBc0MsQ0FBdEMsQ0FBMUY7QUFDQTs7QUFDRjtBQUNFLGlCQUFPUixDQUFQO0FBZEo7QUFnQkQ7QUFyQkksR0FBUDtBQXlCRCxDQTNCb0IsRUFBZCIsImZpbGUiOiIuL3Jlc291cmNlcy9qcy9tb2R1bGVzL0RhdGVzLmpzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNvbnN0IERhdGVzID0gKCgpID0+IHtcblxuICByZXR1cm4ge1xuICBcbiAgICB0b2RheShmb3JtYXQsIHNlcGFyYXRvcikge1xuICAgICAgdmFyIGYgPSBuZXcgRGF0ZSgpO1xuICAgICAgc2VwYXJhdG9yID0gc2VwYXJhdG9yID09IG51bGwgPyAnJyA6IHNlcGFyYXRvcjtcbiAgICAgIHN3aXRjaCAoIGZvcm1hdC50b1VwcGVyQ2FzZSgpICkge1xuICAgICAgICBjYXNlICdZWVlZTU1ERCc6XG4gICAgICAgICAgcmV0dXJuIGYuZ2V0RnVsbFllYXIoKSArIHNlcGFyYXRvciArIHBhZCggKCBmLmdldE1vbnRoKCkgKyAxICkgLCAyICkgKyBzZXBhcmF0b3IgKyBwYWQoIGYuZ2V0RGF0ZSgpLCAyICk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgJ1lZTU1ERCc6XG4gICAgICAgICAgcmV0dXJuIGYuZ2V0RnVsbFllYXIoKS50b1N0cmluZygpLnN1YnN0cigyLDIpICsgc2VwYXJhdG9yICsgcGFkKCAoIGYuZ2V0TW9udGgoKSArIDEgKSAsIDIgKSArIHNlcGFyYXRvciArIHBhZCggZi5nZXREYXRlKCksIDIgKTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAnRERNTVlZWVknOlxuICAgICAgICAgIHJldHVybiAgcGFkKCBmLmdldERhdGUoKSwgMiApICsgc2VwYXJhdG9yICsgcGFkKCAoIGYuZ2V0TW9udGgoKSArIDEgKSAsIDIgKSArIHNlcGFyYXRvciArIGYuZ2V0RnVsbFllYXIoKTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAnRERNTVlZJzpcbiAgICAgICAgICByZXR1cm4gIHBhZCggZi5nZXREYXRlKCksIDIgKSArIHNlcGFyYXRvciArIHBhZCggKCBmLmdldE1vbnRoKCkgKyAxICkgLCAyICkgKyBzZXBhcmF0b3IgKyBmLmdldEZ1bGxZZWFyKCkudG9TdHJpbmcoKS5zdWJzdHIoIDIsIDIgICApOyBcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICByZXR1cm4gZjtcbiAgICAgIH1cbiAgICB9XG4gICAgXG4gIH1cblxufSkoKTtcblxuIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./resources/js/modules/Dates.js\n");

/***/ }),

/***/ 10:
/*!*********************************************!*\
  !*** multi ./resources/js/modules/Dates.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/laravel_mybusiness_shopcart/resources/js/modules/Dates.js */"./resources/js/modules/Dates.js");


/***/ })

/******/ });