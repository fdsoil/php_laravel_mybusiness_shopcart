/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 17);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/modules/Times.js":
/*!***************************************!*\
  !*** ./resources/js/modules/Times.js ***!
  \***************************************/
/*! exports provided: Times */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Times\", function() { return Times; });\nvar Times = function () {\n  return {\n    chkHour00To24: function chkHour00To24(val) {\n      var arr = val.split(':');\n      if (arr[0] == '00') arr[0] = 24;\n      return arr[0] + ':' + arr[1] + ':' + arr[2];\n    },\n    currentTime: function currentTime() {\n      var dig = new Date();\n      var h = dig.getHours();\n      var m = dig.getMinutes(); //var s = dig.getSeconds();\n\n      var ap = \"AM\";\n\n      if (h > 12) {\n        ap = \"PM\";\n        h = h - 12;\n      }\n\n      if (h == 0) h = 12;\n      if (h < 9) h = \"0\" + h;\n      if (m <= 9) m = \"0\" + m; //if ( s <= 9 ) s = \"0\" + s;\n\n      return h + ':' + m + ':' + ap;\n    },\n    normalToMilitar: function normalToMilitar(hora) {\n      var meridian = hora.substr(6, 1);\n      hora = hora.replace('AM', '00');\n      hora = hora.replace('PM', '00');\n\n      if (meridian == 'P') {\n        var arr = hora.split(':');\n        arr[0] = parseInt(arr[0]) + 12;\n        if (arr[0] == 24) arr[0] = '00';\n        hora = pad(arr[0], 2) + ':' + pad(arr[1], 2) + ':' + arr[2];\n      }\n\n      return hora;\n    }\n  };\n}();//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvbW9kdWxlcy9UaW1lcy5qcz9kNDU3Il0sIm5hbWVzIjpbIlRpbWVzIiwiY2hrSG91cjAwVG8yNCIsInZhbCIsImFyciIsInNwbGl0IiwiY3VycmVudFRpbWUiLCJkaWciLCJEYXRlIiwiaCIsImdldEhvdXJzIiwibSIsImdldE1pbnV0ZXMiLCJhcCIsIm5vcm1hbFRvTWlsaXRhciIsImhvcmEiLCJtZXJpZGlhbiIsInN1YnN0ciIsInJlcGxhY2UiLCJwYXJzZUludCIsInBhZCJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFPLElBQU1BLEtBQUssR0FBSSxZQUFLO0FBRXpCLFNBQU87QUFFTEMsaUJBRksseUJBRVNDLEdBRlQsRUFFYztBQUNqQixVQUFJQyxHQUFHLEdBQUdELEdBQUcsQ0FBQ0UsS0FBSixDQUFVLEdBQVYsQ0FBVjtBQUNBLFVBQUtELEdBQUcsQ0FBQyxDQUFELENBQUgsSUFBVSxJQUFmLEVBQ0lBLEdBQUcsQ0FBQyxDQUFELENBQUgsR0FBUyxFQUFUO0FBQ0osYUFBT0EsR0FBRyxDQUFDLENBQUQsQ0FBSCxHQUFTLEdBQVQsR0FBZUEsR0FBRyxDQUFDLENBQUQsQ0FBbEIsR0FBd0IsR0FBeEIsR0FBOEJBLEdBQUcsQ0FBQyxDQUFELENBQXhDO0FBQ0QsS0FQSTtBQVNMRSxlQVRLLHlCQVNTO0FBQ1osVUFBSUMsR0FBRyxHQUFHLElBQUlDLElBQUosRUFBVjtBQUNBLFVBQUlDLENBQUMsR0FBR0YsR0FBRyxDQUFDRyxRQUFKLEVBQVI7QUFDQSxVQUFJQyxDQUFDLEdBQUdKLEdBQUcsQ0FBQ0ssVUFBSixFQUFSLENBSFksQ0FHYzs7QUFDMUIsVUFBSUMsRUFBRSxHQUFHLElBQVQ7O0FBQ0EsVUFBS0osQ0FBQyxHQUFHLEVBQVQsRUFBYztBQUNWSSxVQUFFLEdBQUcsSUFBTDtBQUNBSixTQUFDLEdBQUdBLENBQUMsR0FBRyxFQUFSO0FBQ0g7O0FBQ0QsVUFBS0EsQ0FBQyxJQUFJLENBQVYsRUFDSUEsQ0FBQyxHQUFHLEVBQUo7QUFDSixVQUFLQSxDQUFDLEdBQUcsQ0FBVCxFQUNJQSxDQUFDLEdBQUcsTUFBTUEsQ0FBVjtBQUNKLFVBQUtFLENBQUMsSUFBSSxDQUFWLEVBQ0lBLENBQUMsR0FBRyxNQUFNQSxDQUFWLENBZFEsQ0FlWjs7QUFDQSxhQUFPRixDQUFDLEdBQUcsR0FBSixHQUFVRSxDQUFWLEdBQWMsR0FBZCxHQUFvQkUsRUFBM0I7QUFDRCxLQTFCSTtBQTRCTEMsbUJBNUJLLDJCQTRCV0MsSUE1QlgsRUE0QmlCO0FBQ3BCLFVBQUlDLFFBQVEsR0FBR0QsSUFBSSxDQUFDRSxNQUFMLENBQVksQ0FBWixFQUFlLENBQWYsQ0FBZjtBQUNBRixVQUFJLEdBQUdBLElBQUksQ0FBQ0csT0FBTCxDQUFhLElBQWIsRUFBbUIsSUFBbkIsQ0FBUDtBQUNBSCxVQUFJLEdBQUdBLElBQUksQ0FBQ0csT0FBTCxDQUFhLElBQWIsRUFBbUIsSUFBbkIsQ0FBUDs7QUFDQSxVQUFLRixRQUFRLElBQUksR0FBakIsRUFBdUI7QUFDbkIsWUFBSVosR0FBRyxHQUFHVyxJQUFJLENBQUNWLEtBQUwsQ0FBVyxHQUFYLENBQVY7QUFDQUQsV0FBRyxDQUFDLENBQUQsQ0FBSCxHQUFTZSxRQUFRLENBQUVmLEdBQUcsQ0FBQyxDQUFELENBQUwsQ0FBUixHQUFxQixFQUE5QjtBQUNBLFlBQUtBLEdBQUcsQ0FBQyxDQUFELENBQUgsSUFBVSxFQUFmLEVBQ0lBLEdBQUcsQ0FBQyxDQUFELENBQUgsR0FBUyxJQUFUO0FBQ0pXLFlBQUksR0FBR0ssR0FBRyxDQUFFaEIsR0FBRyxDQUFDLENBQUQsQ0FBTCxFQUFVLENBQVYsQ0FBSCxHQUFtQixHQUFuQixHQUF5QmdCLEdBQUcsQ0FBRWhCLEdBQUcsQ0FBQyxDQUFELENBQUwsRUFBVSxDQUFWLENBQTVCLEdBQTRDLEdBQTVDLEdBQWtEQSxHQUFHLENBQUMsQ0FBRCxDQUE1RDtBQUNIOztBQUNELGFBQU9XLElBQVA7QUFDRDtBQXhDSSxHQUFQO0FBNENELENBOUNvQixFQUFkIiwiZmlsZSI6Ii4vcmVzb3VyY2VzL2pzL21vZHVsZXMvVGltZXMuanMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY29uc3QgVGltZXMgPSAoKCk9PiB7XG5cbiAgcmV0dXJuIHtcbiAgICBcbiAgICBjaGtIb3VyMDBUbzI0KHZhbCkge1xuICAgICAgdmFyIGFyciA9IHZhbC5zcGxpdCgnOicpO1xuICAgICAgaWYgKCBhcnJbMF0gPT0gJzAwJyApXG4gICAgICAgICAgYXJyWzBdID0gMjQ7XG4gICAgICByZXR1cm4gYXJyWzBdICsgJzonICsgYXJyWzFdICsgJzonICsgYXJyWzJdO1xuICAgIH0sXG4gICAgXG4gICAgY3VycmVudFRpbWUoKSB7XG4gICAgICB2YXIgZGlnID0gbmV3IERhdGUoKTtcbiAgICAgIHZhciBoID0gZGlnLmdldEhvdXJzKCk7XG4gICAgICB2YXIgbSA9IGRpZy5nZXRNaW51dGVzKCk7IC8vdmFyIHMgPSBkaWcuZ2V0U2Vjb25kcygpO1xuICAgICAgdmFyIGFwID0gXCJBTVwiO1xuICAgICAgaWYgKCBoID4gMTIgKSB7XG4gICAgICAgICAgYXAgPSBcIlBNXCI7XG4gICAgICAgICAgaCA9IGggLSAxMjtcbiAgICAgIH1cbiAgICAgIGlmICggaCA9PSAwIClcbiAgICAgICAgICBoID0gMTI7XG4gICAgICBpZiAoIGggPCA5IClcbiAgICAgICAgICBoID0gXCIwXCIgKyBoO1xuICAgICAgaWYgKCBtIDw9IDkgKVxuICAgICAgICAgIG0gPSBcIjBcIiArIG07XG4gICAgICAvL2lmICggcyA8PSA5ICkgcyA9IFwiMFwiICsgcztcbiAgICAgIHJldHVybiBoICsgJzonICsgbSArICc6JyArIGFwO1xuICAgIH0sXG4gICAgXG4gICAgbm9ybWFsVG9NaWxpdGFyKGhvcmEpIHtcbiAgICAgIHZhciBtZXJpZGlhbiA9IGhvcmEuc3Vic3RyKDYsIDEpO1xuICAgICAgaG9yYSA9IGhvcmEucmVwbGFjZSgnQU0nLCAnMDAnKTtcbiAgICAgIGhvcmEgPSBob3JhLnJlcGxhY2UoJ1BNJywgJzAwJyk7XG4gICAgICBpZiAoIG1lcmlkaWFuID09ICdQJyApIHtcbiAgICAgICAgICB2YXIgYXJyID0gaG9yYS5zcGxpdCgnOicpO1xuICAgICAgICAgIGFyclswXSA9IHBhcnNlSW50KCBhcnJbMF0gKSArIDEyO1xuICAgICAgICAgIGlmICggYXJyWzBdID09IDI0IClcbiAgICAgICAgICAgICAgYXJyWzBdID0gJzAwJztcbiAgICAgICAgICBob3JhID0gcGFkKCBhcnJbMF0sIDIgKSArICc6JyArIHBhZCggYXJyWzFdLCAyICkgKyAnOicgKyBhcnJbMl07XG4gICAgICB9XG4gICAgICByZXR1cm4gaG9yYTtcbiAgICB9XG4gIFxuICB9XG4gIFxufSkoKTtcblxuXG4iXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./resources/js/modules/Times.js\n");

/***/ }),

/***/ 17:
/*!*********************************************!*\
  !*** multi ./resources/js/modules/Times.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/laravel_mybusiness_shopcart/resources/js/modules/Times.js */"./resources/js/modules/Times.js");


/***/ })

/******/ });