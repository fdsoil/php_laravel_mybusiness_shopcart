<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReplacementsInsertIntoMovementsTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            CREATE OR REPLACE FUNCTION public.replacements_insert_into_movements()
                RETURNS trigger
                LANGUAGE 'plpgsql'
                COST 100
                VOLATILE NOT LEAKPROOF
            AS \$BODY$
            DECLARE
                v_id integer;
                v_record record;
            BEGIN
                IF (TG_OP = 'UPDATE') THEN
                    IF (NEW.received IS NOT NULL) THEN
                        INSERT INTO public.movements(
                            type_id
                            ,subject
                            ,description
                            ,observation
                            ,support_type_id
                            ,support_number
                            ,support_date
                            ,user_insert_id
                            ,user_update_id
                            ,user_edit_id
                            ,created_at
                            ,updated_at

                        ) VALUES (
                            1
                            ,NEW.subject
                            ,NEW.description
                            ,NEW.observation
                            ,1
                            ,NEW.number
                            ,NEW.received
                            ,NEW.user_insert_id
                            ,NEW.user_update_id
                            ,NEW.user_edit_id
                            ,now()::timestamp(0) without time zone
                            ,now()::timestamp(0) without time zone
                        ) RETURNING id INTO v_id;
                        FOR v_record in SELECT * FROM public.replacement_details WHERE replacement_id = NEW.id LOOP
                            --RAISE NOTICE '-- Record: % (%)', v_record.id;	
                            INSERT INTO public.movement_details(
                                movement_id
                                ,article_id
                                ,quantity
                                ,user_insert_id
                                ,user_update_id
                                ,created_at
                                ,updated_at
                            ) VALUES (
                                v_id
                                ,v_record.article_id
                                ,v_record.amount_received
                                ,v_record.user_insert_id
                                ,v_record.user_update_id
                                ,now()::timestamp(0) without time zone
                                ,now()::timestamp(0) without time zone
                            );
                        END LOOP;
                    END IF;
                END IF;
                RETURN null;
            END;
            \$BODY$;

            ALTER FUNCTION public.replacements_insert_into_movements()
            OWNER TO postgres;
        ");
        
        DB::unprepared("                
            CREATE TRIGGER replacement_insert_into_movement
                AFTER UPDATE
                ON public.replacements
                FOR EACH ROW
            EXECUTE PROCEDURE public.replacements_insert_into_movements();
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("
            DROP TRIGGER replacement_insert_into_movement ON public.replacements;                        
        ");
        
        DB::unprepared("
            DROP FUNCTION public.replacements_insert_into_movements();            
        ");
    }
}

