<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVouchersGenerateNumberTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            CREATE OR REPLACE FUNCTION public.vouchers_generate_number()
                RETURNS trigger
                LANGUAGE 'plpgsql'
                COST 100
                VOLATILE NOT LEAKPROOF
            AS \$BODY\$
            BEGIN
                  NEW.number:=lpad(cast(NEW.id as character varying),10,'0');
                  return NEW;
                END;
            \$BODY\$;

            ALTER FUNCTION public.vouchers_generate_number()
                OWNER TO postgres;        
        ");
        
        DB::unprepared("                
            CREATE TRIGGER voucher_generate_number
                BEFORE INSERT
                ON public.vouchers
                FOR EACH ROW
                EXECUTE PROCEDURE public.vouchers_generate_number();         
        ");
    }

    /**
     * Reverse the migrations.
     *     
     * @return void
     */
    public function down()
    {
        DB::unprepared("
            DROP TRIGGER voucher_generate_number ON public.vouchers;                        
        ");
        
        DB::unprepared("
            DROP FUNCTION public.vouchers_generate_number();            
        ");
    }
}
