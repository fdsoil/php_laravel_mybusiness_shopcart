<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoucherDetailsInsertIntoMovementsTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {     
        DB::unprepared("
            CREATE OR REPLACE FUNCTION public.voucher_details_insert_into_movements()
                RETURNS trigger
                LANGUAGE 'plpgsql'
                COST 100
                VOLATILE NOT LEAKPROOF
            AS \$BODY$
            DECLARE
                v_id integer;
            BEGIN
                IF (TG_OP = 'INSERT') THEN

                    SELECT id INTO v_id
                        FROM public.movements
                            WHERE close ISNULL 
                            AND support_type_id = 2 
                            AND support_number = (SELECT number
                                                      FROM public.vouchers
                                                          WHERE id = NEW.voucher_id);

                     INSERT INTO public.movement_details(
                         movement_id
                         ,article_id
                         ,quantity
                         ,user_insert_id
                         ,user_update_id
                         ,created_at
                         ,updated_at
                     ) VALUES (
                         v_id
                         ,NEW.article_id
                         ,NEW.quantity
                         ,1 --NEW.user_insert_id
                         ,1 --NEW.user_update_id
                         ,now()::timestamp(0) without time zone
                         ,now()::timestamp(0) without time zone
                     );

                END IF;
                RETURN null;
            END;
            \$BODY$;

            ALTER FUNCTION public.voucher_details_insert_into_movements()
            OWNER TO postgres;
        ");
        
        DB::unprepared("                
            CREATE TRIGGER voucher_detail_insert_into_movement
                AFTER INSERT
                ON public.voucher_details
                FOR EACH ROW
            EXECUTE PROCEDURE public.voucher_details_insert_into_movements();
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("
            DROP TRIGGER voucher_detail_insert_into_movement ON public.voucher_details;
        ");
           
        
        DB::unprepared("
            DROP FUNCTION public.voucher_details_insert_into_movements();            
        ");
    }
}


/*
INSERT INTO public.movements(
                        type_id
                        ,subject
                        ,description
                        ,observation
                        ,support_type_id
                        ,support_number
                        ,support_date
                        ,id_user_insert
                        ,id_user_update
                        ,id_user_edit
                        ,created_at
                        ,updated_at
                    ) VALUES (
                        2
                        ,NEW.subject
                        ,NEW.description
                        ,NEW.observation
                        ,2
                        ,NEW.number
                        ,NEW.date_time
                        ,1 --NEW.id_user_insert
                        ,1 --NEW.id_user_update
                        ,1 --NEW.id_user_edit
                        ,now()::timestamp(0) without time zone
                        ,now()::timestamp(0) without time zone
                    );                                  
*/
