<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoucherDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher_details', function (Blueprint $table) {
            $table->id();
            $table->integer('voucher_id');   
            $table->integer('article_id');            
            $table->float('price');
            $table->integer('quantity');
            $table->integer('status_id')->default(1);
            $table->unique(['voucher_id', 'article_id']);
            $table->foreign('voucher_id')->references('id')->on('vouchers');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voucher_details');
    }
}
