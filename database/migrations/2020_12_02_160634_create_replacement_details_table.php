<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReplacementDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('replacement_details', function (Blueprint $table) {
            $table->id();
            $table->integer('replacement_id');
            $table->integer('article_id');
            $table->integer('amount_requested')->default(1);
            $table->integer('amount_received')->default(0);
            $table->integer('user_insert_id')->default(1);
            $table->integer('user_update_id')->default(1);
            $table->unique(['replacement_id', 'article_id']);
            $table->foreign('replacement_id')->references('id')->on('replacements');      
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('replacement_details');
    }
}
