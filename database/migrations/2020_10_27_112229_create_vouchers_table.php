<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchers', function (Blueprint $table) {
            $table->id();
            $table->string('number', 10);
            $table->dateTime('date_time',0)->useCurrent();
            $table->integer('client_id');
            $table->string('subject', 40);
            $table->text('description');
            $table->text('observation')->default('S/O');
            $table->float('tax')->default(10);
            $table->integer('status_id')->default(1);
            $table->string('payment_number', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vouchers');
    }
}
