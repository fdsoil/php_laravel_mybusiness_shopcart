<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVouchersRegistFunction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            CREATE OR REPLACE FUNCTION public.vouchers_regist(
                i_client_id integer,
                i_subject character varying,
                i_description text,
                i_payment_number character varying,
                i_arr character varying[])
                RETURNS json
                LANGUAGE 'plpgsql'

                COST 100
                VOLATILE 

            AS \$BODY\$
            DECLARE  
                o_return json;
                v_id integer;
                v_number character varying;
                v_date_time character varying;
            BEGIN
                o_return := array_to_json(array['']);        
                v_id := 0;
                v_number := '';
                v_date_time := '';
        
	            INSERT INTO vouchers(client_id, subject, description, payment_number, created_at, updated_at) 
                    VALUES ( i_client_id, i_subject, i_description, i_payment_number, now()::timestamp(0) without time zone, now()::timestamp(0) without time zone)
                        RETURNING id, number, date_time::character varying 
                            INTO v_id, v_number, v_date_time;

                FOR i IN array_lower(i_arr, 1) .. array_upper(i_arr, 1) LOOP	
                    INSERT INTO voucher_details( voucher_id, article_id, price, quantity, created_at, updated_at)
                        VALUES ( v_id,
                                 i_arr[i][1]::integer,
                                 i_arr[i][2]::double precision,
                                 i_arr[i][3]::integer,
                                 now()::timestamp(0) without time zone,
                                 now()::timestamp(0) without time zone );
                END LOOP;
		
                o_return:= array_to_json(array['Compra registrada con Éxito !!', v_id::character varying , v_number, v_date_time]);     

                RETURN o_return;  
            END;
            \$BODY\$;

            ALTER FUNCTION public.vouchers_regist(integer, character varying, text, character varying , character varying[])
                OWNER TO postgres;        
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP FUNCTION public.vouchers_regist(integer, character varying, text, character varying, character varying[]);");
    }
}
