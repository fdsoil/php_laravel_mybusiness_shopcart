<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVouchersInsertIntoMovementsTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {     
        DB::unprepared("
            CREATE OR REPLACE FUNCTION public.vouchers_insert_into_movements()
                RETURNS trigger
                LANGUAGE 'plpgsql'
                COST 100
                VOLATILE NOT LEAKPROOF
            AS \$BODY$
            BEGIN
                IF (TG_OP = 'INSERT') THEN                    
                    INSERT INTO public.movements(
                        type_id
                        ,subject
                        ,description
                        ,observation
                        ,support_type_id
                        ,support_number
                        ,support_date
                        ,user_insert_id
                        ,user_update_id
                        ,user_edit_id
                        ,created_at
                        ,updated_at
                    ) VALUES (
                        2
                        ,NEW.subject
                        ,NEW.description
                        ,NEW.observation
                        ,2
                        ,NEW.number
                        ,NEW.date_time
                        ,1 --NEW.user_insert_id
                        ,1 --NEW.user_update_id
                        ,1 --NEW.user_edit_id
                        ,now()::timestamp(0) without time zone
                        ,now()::timestamp(0) without time zone
                    );                                  
                END IF;
                RETURN null;
            END;
            \$BODY$;

            ALTER FUNCTION public.vouchers_insert_into_movements()
            OWNER TO postgres;
        ");
        
        DB::unprepared("                
            CREATE TRIGGER voucher_insert_into_movement
                AFTER INSERT
                ON public.vouchers
                FOR EACH ROW
            EXECUTE PROCEDURE public.vouchers_insert_into_movements();
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("
            DROP TRIGGER voucher_insert_into_movement ON public.vouchers;                        
        ");
        
        DB::unprepared("
            DROP FUNCTION public.vouchers_insert_into_movements();            
        ");
    }
}
