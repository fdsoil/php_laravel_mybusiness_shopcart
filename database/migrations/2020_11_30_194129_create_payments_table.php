<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('payments')) {
            Schema::create('payments', function (Blueprint $table) {
                $table->id();            
                $table->string('number', 10);
                $table->dateTime('date_time',0)->useCurrent();                        
                $table->string('trx_type', 10);
                $table->string('payment_method', 10);            
                $table->string('card_number', 18);            
                $table->string('customer_id', 10);
                $table->string('account_type', 2);
                $table->string('expiration_date', 7);            
                $table->float('amount')->default(10);
                $table->boolean('success')->default(false);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('payments');
    }
}
