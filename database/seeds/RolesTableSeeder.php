<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //Role::create(["description" => "Developer"     , "pag_ini_default" => "home", "status_id" => 1 ]);
      Role::create(["description" => "Admininstrator", "pag_ini_default" => "home", "status_id" => 1 ]);
      Role::create(["description" => "User"          , "pag_ini_default" => "home", "status_id" => 1 ]);
    }

}

