<?php

use Illuminate\Database\Seeder;
use App\ArticleDetail;

class ArticleDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ArticleDetail::create([ 'article_id' => 1, 'presentation_id' => 1, 'quantity' => 1 ]);
        
        ArticleDetail::create([ 'article_id' => 2, 'presentation_id' => 2, 'quantity' => 2 ]);
        ArticleDetail::create([ 'article_id' => 2, 'presentation_id' => 3, 'quantity' => 2 ]);
        
        ArticleDetail::create([ 'article_id' => 3, 'presentation_id' => 4, 'quantity' => 3 ]);
        ArticleDetail::create([ 'article_id' => 3, 'presentation_id' => 5, 'quantity' => 3 ]);
        ArticleDetail::create([ 'article_id' => 3, 'presentation_id' => 6, 'quantity' => 3 ]);
        
        ArticleDetail::create([ 'article_id' => 4, 'presentation_id' => 7, 'quantity' => 3 ]);
        ArticleDetail::create([ 'article_id' => 4, 'presentation_id' => 8, 'quantity' => 3 ]);
        ArticleDetail::create([ 'article_id' => 4, 'presentation_id' => 9, 'quantity' => 3 ]);

        ArticleDetail::create([ 'article_id' => 5, 'presentation_id' => 10, 'quantity' => 2 ]);
        ArticleDetail::create([ 'article_id' => 5, 'presentation_id' => 11, 'quantity' => 2 ]);
        
        ArticleDetail::create([ 'article_id' => 6, 'presentation_id' => 12, 'quantity' => 1 ]);
    }
}
