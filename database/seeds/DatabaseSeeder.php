<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(MenusTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(PaymentsTableSeeder::class);
        $this->call(ReplacementSeeder::class);
        $this->call(ReplacementDetailSeeder::class);
        $this->call(VouchersTableSeeder::class);
        $this->call(VoucherDetailsTableSeeder::class);
        //$this->call(MovementSeeder::class);
        //$this->call(MovementDetailSeeder::class);
        $this->call(RoleMenusSeeder::class);
        $this->call(ArticleSeeder::class);
        $this->call(ArticleDetailSeeder::class);
    }
}
