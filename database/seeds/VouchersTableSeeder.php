<?php

use Illuminate\Database\Seeder;
use App\Voucher;

class VouchersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Voucher::create([
            'client_id'      => 1,
            'subject'        => 'V111111 - Cliente Uno',
            'description'    => 'Email: cliente@cliente.com; Tlf: 1111111111; Edo: ANZOATEGUI; Mpio: FERNANDO DE PEÑALVER; Pqa: SAN MIGUEL;', 
            'payment_number' => '0000000001'
        ]);
    }
}
