<?php

use Illuminate\Database\Seeder;
use App\Payment;

class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $payment = Payment::get();
        if ($payment->isEmpty()) {
            Payment::create([
                'trx_type'        => 'compra',
                'payment_method'  => 'TDD',
                'card_number'     => '501878200066287386',
                'customer_id'     => 'V18366876',  
                'account_type'    => 'CC',   
                'expiration_date' => '2021/11',       
                'amount'          => '990',
                'success'         => true
            ]);
        }
    }
}

