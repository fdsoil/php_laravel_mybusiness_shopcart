<?php

use Illuminate\Database\Seeder;
use App\Replacement; 

class ReplacementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        Replacement::create([
            'subject' => 'FULANO',
            'description' => 'AAAAAAA AAAAAA AAA',
            'observation' => 'N/O'
        ]);
        
    }
}
