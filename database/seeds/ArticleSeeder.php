<?php

use Illuminate\Database\Seeder;
use App\Article;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Article::create([
            'int_cod' => 'A1A1A1A1A1A1A1A',
            'name' => 'ARTICULO 1',
            'price' => 100,
            'photo' => 'articleId_1.jpeg'
        ]);
        
        Article::create([
            'int_cod' => 'B2B2B2B2B2B2B2B',
            'name' => 'ARTICULO 2',
            'price' => 200,
            'photo' => 'articleId_2.jpeg'
        ]);
        
        Article::create([
            'int_cod' => 'C3C3C3C3C3C3C3C',
            'name' => 'ARTICULO 3',
            'price' => 300,
            'photo' => 'articleId_3.jpeg'
        ]);
        
        Article::create([
            'int_cod' => 'D4D4D4D4D4D4D4D',
            'name' => 'ARTICULO 4',
            'price' => 400,
            'photo' => 'articleId_4.jpeg'
        ]);
        
        Article::create([
            'int_cod' => 'E5E5E5E5E5E5E5E',
            'name' => 'ARTICULO 5',
            'price' => 500
        ]);
        
        Article::create([
            'int_cod' => 'F6F6F6F6F6F6F6F',
            'name' => 'ARTICULO 6',
            'price' => 600
        ]);
    }
}
