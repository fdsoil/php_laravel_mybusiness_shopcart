<?php

use Illuminate\Database\Seeder;
use App\Models\RoleMenu;

class RoleMenusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RoleMenu::create(["role_id" => 1, "menu_id" =>  4 ]);
        RoleMenu::create(["role_id" => 1, "menu_id" =>  5 ]);
        RoleMenu::create(["role_id" => 1, "menu_id" =>  6 ]);
        RoleMenu::create(["role_id" => 1, "menu_id" =>  7 ]);
        RoleMenu::create(["role_id" => 1, "menu_id" =>  8 ]);
        RoleMenu::create(["role_id" => 1, "menu_id" =>  9 ]);
        RoleMenu::create(["role_id" => 1, "menu_id" =>  10]);
        RoleMenu::create(["role_id" => 1, "menu_id" =>  11]);                
        RoleMenu::create(["role_id" => 1, "menu_id" =>  12]);
        RoleMenu::create(["role_id" => 2, "menu_id" =>  13]);
        RoleMenu::create(["role_id" => 2, "menu_id" =>  14]);
    }
}

