<?php

use Illuminate\Database\Seeder;
use App\VoucherDetail;

class VoucherDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        VoucherDetail::create([            
            'voucher_id' => 1,
            'article_id' => 1,
            'price' => 300,
            'quantity' => 1            
        ]);
        
        VoucherDetail::create([            
            'voucher_id' => 1,
            'article_id' => 2,
            'price' => 150,
            'quantity' => 2            
        ]);
        
        VoucherDetail::create([            
            'voucher_id' => 1,
            'article_id' => 3,
            'price' => 100,
            'quantity' => 3            
        ]);
    }
}
