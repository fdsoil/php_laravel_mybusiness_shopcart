<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'     => 'Administrador',
            'email'    => 'admin@admin.com',
            // 'address'    => 'admin@admin.com',
            'password' => Hash::make('123qweas'),
            'remember_token'=>'fcMV8SW1HJxoYLYEYdkZ0o0KTwJ9N4WAyFIIZeFb5KWWJ6x1lonBttFXDLTa',
            'is_admin' => true,
            'email_verified_at' => now(),
        ]);
        
        User::create([
            'name'     => 'Cliente',
            'email'    => 'cliente@cliente.com',            
            'password' => Hash::make('123qweas'),
            'remember_token'=>'fcMV8SW1HJxoYLYEYdkZ0o0KTwJ9N4WAyFIIZeFb5KWWJ6x1lonBttFXDLTa',
            'is_admin' => false,
            'email_verified_at' => now(),
        ]);
    }
}
