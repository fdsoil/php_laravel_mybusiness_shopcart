<?php

use Illuminate\Database\Seeder;
use App\ReplacementDetail;
use App\Replacement; 

class ReplacementDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
        ReplacementDetail::create([
            'replacement_id' => 1,
            'article_id' => 1,
            'amount_requested' => 10
        ]);
        
        ReplacementDetail::create([
            'replacement_id' => 1,
            'article_id' => 2,
            'amount_requested' => 10
        ]);
        
        ReplacementDetail::create([
            'replacement_id' => 1,
            'article_id' => 3,
            'amount_requested' => 10
        ]);

        ReplacementDetail::find(1)->update(['amount_received' => 10]);
        ReplacementDetail::find(2)->update(['amount_received' => 5]);
        ReplacementDetail::find(3)->update(['amount_received' => 8]);

        Replacement::query()->update([
            'observation' => 'NO SE RECIBIÓ TODA LA MERCANCÍA SOLICITADA',
            'received'=> date('Y-m-d')
        ]);

    }
}

