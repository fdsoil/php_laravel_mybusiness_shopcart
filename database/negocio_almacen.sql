--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.17
-- Dumped by pg_dump version 9.6.17

-- Started on 2020-07-11 22:25:13 -04

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12393)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2361 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 3 (class 3079 OID 187957)
-- Name: dblink; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS dblink WITH SCHEMA public;


--
-- TOC entry 2362 (class 0 OID 0)
-- Dependencies: 3
-- Name: EXTENSION dblink; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION dblink IS 'connect to other PostgreSQL databases from within a database';


--
-- TOC entry 2 (class 3079 OID 188647)
-- Name: postgres_fdw; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgres_fdw WITH SCHEMA public;


--
-- TOC entry 2363 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION postgres_fdw; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgres_fdw IS 'foreign-data wrapper for remote PostgreSQL servers';


--
-- TOC entry 283 (class 1255 OID 198054)
-- Name: cierre_diario(date, date, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.cierre_diario(i_fec_desde date, i_fec_hasta date, i_id_user integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE
        o_return character;
        v_fecha date;        
BEGIN
	o_return:='Z';
        IF (SELECT public.cierre_diario_fechas_validas(i_fec_desde, i_fec_hasta)) THEN
		v_fecha = i_fec_desde;
		WHILE v_fecha <= i_fec_hasta LOOP
			IF  (SELECT public.cierre_diario_fecha_existe(v_fecha)) THEN
			        SELECT public.cierre_diario_register(v_fecha, i_id_user) INTO o_return;				
			END IF;
			SELECT ( v_fecha + interval '1 day') INTO v_fecha; 
		END LOOP;
	END IF;  
	
	RETURN o_return;
	
END;
$$;


ALTER FUNCTION public.cierre_diario(i_fec_desde date, i_fec_hasta date, i_id_user integer) OWNER TO postgres;

--
-- TOC entry 282 (class 1255 OID 198050)
-- Name: cierre_diario_fecha_existe(date); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.cierre_diario_fecha_existe(i_fecha date) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
BEGIN

        RETURN (SELECT CASE WHEN count(*)=0 THEN false ELSE true END 
		FROM public.movimiento 
			WHERE cierre_fec IS NULL AND movimiento_fec = i_fecha);

END;
$$;


ALTER FUNCTION public.cierre_diario_fecha_existe(i_fecha date) OWNER TO postgres;

--
-- TOC entry 281 (class 1255 OID 198047)
-- Name: cierre_diario_fechas_validas(date, date); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.cierre_diario_fechas_validas(i_fecha_desde date, i_fecha_hasta date) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
BEGIN
        RETURN (i_fecha_desde = (SELECT min(movimiento_fec) FROM public.movimiento WHERE cierre_fec IS NULL)) 
               AND 
               (i_fecha_hasta <= now());
END;
$$;


ALTER FUNCTION public.cierre_diario_fechas_validas(i_fecha_desde date, i_fecha_hasta date) OWNER TO postgres;

--
-- TOC entry 280 (class 1255 OID 198052)
-- Name: cierre_diario_register(date, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.cierre_diario_register(i_fecha date, i_id_user integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
BEGIN
	INSERT INTO public.cierre_diario(
		id_presentacion, 
		cantidad_entrante, 
		cantidad_saliente, 
		cantidad_reverso_entrante, 
		cantidad_reverso_saliente, 
		cierre_dia, 
		id_user_insert
	) SELECT id_presentacion, 
		cantidad_entrante, 
		cantidad_saliente,
		cantidad_reverso_entrante, 
		cantidad_reverso_saliente, 
		i_fecha, 
		i_id_user
		FROM view_cierre_pre_insert
		WHERE movimiento_fec = i_fecha;
	UPDATE public.movimiento SET cierre_fec = now()
		WHERE movimiento_fec = i_fecha;
	UPDATE public.movimiento_aux SET cierre_fec = now()
		WHERE id_movimiento IN (SELECT id FROM public.movimiento WHERE movimiento_fec = i_fecha);
	RETURN 'C';
END;
$$;


ALTER FUNCTION public.cierre_diario_register(i_fecha date, i_id_user integer) OWNER TO postgres;

--
-- TOC entry 274 (class 1255 OID 189437)
-- Name: existencia(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.existencia() RETURNS TABLE(id_presentacion integer, id_movimiento_tipo integer, cantidad_total bigint)
    LANGUAGE plpgsql
    AS $$
DECLARE
	o_return character varying;
        c_id_presentacion integer;
	c_id_movimiento_tipo integer;
	c_cantidad_total bigint;
	
	
	myCursor cursor FOR     

		SELECT A.id_presentacion, B.id_movimiento_tipo, sum(A.cantidad) AS cantidad_total
                    FROM public.movimiento_aux A 
                    INNER JOIN public.movimiento B ON A.id_movimiento = B.id
                    GROUP BY A.id_presentacion, B.id_movimiento_tipo;
BEGIN
	open myCursor;
	FETCH myCursor INTO c_id_presentacion, c_id_movimiento_tipo, c_cantidad_total;
	LOOP	
		
	FETCH myCursor INTO c_id_presentacion, c_id_movimiento_tipo, c_cantidad_total;
	EXIT WHEN NOT found;
	END LOOP;
	CLOSE myCursor;
	RETURN QUERY SELECT A.id_presentacion, B.id_movimiento_tipo, sum(A.cantidad) AS cantidad_total
                    FROM public.movimiento_aux A 
                    INNER JOIN public.movimiento B ON A.id_movimiento = B.id
                    GROUP BY A.id_presentacion, B.id_movimiento_tipo;
END;
$$;


ALTER FUNCTION public.existencia() OWNER TO postgres;

--
-- TOC entry 267 (class 1255 OID 188387)
-- Name: generar_numero(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.generar_numero() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
                   NEW.movimiento_num:= lpad(CAST ((SELECT count (id) FROM public.movimiento WHERE (EXTRACT(YEAR FROM now())) = EXTRACT(YEAR FROM movimiento_fec)
                   )+1 AS CHARACTER VARYING),7,'0') || '' ||
       lpad(CAST ((SELECT count (id) FROM public.movimiento WHERE (EXTRACT(MONTH FROM now())) = EXTRACT(MONTH FROM movimiento_fec)
                   )+1 AS CHARACTER VARYING),6,'0') || ''||
 lpad(CAST ((SELECT count (id) FROM public.movimiento WHERE (EXTRACT(YEAR FROM now())) = EXTRACT(YEAR FROM movimiento_fec) AND id_movimiento_tipo = NEW.id_movimiento_tipo 
                   )+1 AS CHARACTER VARYING),7,'0') || '' ||
       lpad(CAST ((SELECT count (id) FROM public.movimiento WHERE (EXTRACT(MONTH FROM now())) = EXTRACT(MONTH FROM movimiento_fec ) AND id_movimiento_tipo = NEW.id_movimiento_tipo
                   )+1 AS CHARACTER VARYING),6,'0') || '' || NEW.id_movimiento_tipo;
                   RETURN NEW;
               END;$$;


ALTER FUNCTION public.generar_numero() OWNER TO postgres;

--
-- TOC entry 275 (class 1255 OID 189439)
-- Name: get_film(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.get_film() RETURNS TABLE(id_presentacion integer, id_movimiento_tipo integer, cantidad_total bigint)
    LANGUAGE plpgsql
    AS $$
DECLARE 
    var_r record;
BEGIN
	FOR var_r IN(SELECT A.id_presentacion, B.id_movimiento_tipo, sum(A.cantidad) AS cantidad_total
                    FROM public.movimiento_aux A 
                    INNER JOIN public.movimiento B ON A.id_movimiento = B.id
                    GROUP BY A.id_presentacion, B.id_movimiento_tipo)  
	LOOP
	    id_presentacion :=var_r.id_presentacion;
	id_movimiento_tipo :=var_r.id_movimiento_tipo;
	cantidad_total :=var_r.cantidad_total;
        RETURN NEXT;
	END LOOP;
END; $$;


ALTER FUNCTION public.get_film() OWNER TO postgres;

--
-- TOC entry 279 (class 1255 OID 188646)
-- Name: movimiento_aux_register(integer, integer, integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.movimiento_aux_register(i_id integer, i_id_movimiento integer, i_id_presentacion integer, i_cantidad integer, i_id_user integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF (SELECT public.movimiento_aux_validate(i_id_movimiento, i_id_presentacion, i_cantidad)) THEN
		IF i_id=0 THEN
			INSERT INTO public.movimiento_aux(
				id_movimiento,
				id_presentacion,
				cantidad,
				id_user_insert,
				id_user_update)
			VALUES (
				i_id_movimiento,
				i_id_presentacion,
				i_cantidad,
				i_id_user,
				i_id_user);
			o_return:= 'C';
		ELSE
			UPDATE public.movimiento_aux SET
				id_movimiento=i_id_movimiento,
				id_presentacion=i_id_presentacion,
				cantidad=i_cantidad,
				id_user_update=i_id_user,
				date_update=('now'::text)::date,
				time_update=('now'::text)::time without time zone
			WHERE id=i_id;
			o_return:= 'A';
		END IF;
	ELSE
		o_return:= 'Z';
        END IF;
        RETURN o_return;
END;
$$;


ALTER FUNCTION public.movimiento_aux_register(i_id integer, i_id_movimiento integer, i_id_presentacion integer, i_cantidad integer, i_id_user integer) OWNER TO postgres;

--
-- TOC entry 269 (class 1255 OID 188642)
-- Name: movimiento_aux_remove(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.movimiento_aux_remove(i_id integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM public.movimiento_aux WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$$;


ALTER FUNCTION public.movimiento_aux_remove(i_id integer) OWNER TO postgres;

--
-- TOC entry 278 (class 1255 OID 189823)
-- Name: movimiento_aux_validate(integer, integer, numeric); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.movimiento_aux_validate(i_id_movimiento integer, i_id_presentacion integer, i_cantidad numeric) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE
        o_return boolean;
BEGIN
        o_return:=false;
        IF ((SELECT id_movimiento_tipo FROM public.movimiento WHERE id = i_id_movimiento) = 2) THEN
            IF ((SELECT total FROM public.view_existencia WHERE id_presentacion = i_id_presentacion)::numeric >= i_cantidad::numeric) THEN
                o_return:=true;
            END IF;
        ELSE 
            o_return:=true;
        END IF;        
        RETURN o_return;
END;
$$;


ALTER FUNCTION public.movimiento_aux_validate(i_id_movimiento integer, i_id_presentacion integer, i_cantidad numeric) OWNER TO postgres;

--
-- TOC entry 276 (class 1255 OID 189824)
-- Name: movimiento_aux_validate_(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.movimiento_aux_validate_(i_id_movimiento integer, i_id_presentacion integer, i_cantidad integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE
        o_return boolean;
BEGIN
        o_return:=false;
        IF ((SELECT id_movimiento_tipo FROM public.movimiento WHERE id = i_id_movimiento) = 2) THEN
            IF ((SELECT total FROM public.view_existencia WHERE id_presentacion = i_id_presentacion)::numeric >= i_cantidad::numeric) THEN
                o_return:=true;
            END IF;
        ELSE 
            o_return:=true;
        END IF;        
        RETURN o_return;
END;
$$;


ALTER FUNCTION public.movimiento_aux_validate_(i_id_movimiento integer, i_id_presentacion integer, i_cantidad integer) OWNER TO postgres;

--
-- TOC entry 273 (class 1255 OID 188645)
-- Name: movimiento_register(integer, integer, character varying, text, integer, character varying, date, text, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.movimiento_register(i_id integer, i_id_movimiento_tipo integer, i_sujeto character varying, i_descripcion text, i_id_soporte_tipo integer, i_soporte_num character varying, i_soporte_fec date, i_observacion text, i_id_user integer) RETURNS json
    LANGUAGE plpgsql
    AS $$
DECLARE
        v_existe boolean;
        v_id character varying;
        v_number character varying;
        v_date character varying;
        o_return json;
BEGIN
        o_return:=array_to_json(array['']);
        IF i_id=0 THEN
                --SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM public.movimiento
                --      WHERE movimiento_num=i_movimiento_num;
                --IF  v_existe='f' THEN
                        INSERT INTO public.movimiento(
                                id_movimiento_tipo,
                                sujeto,
                                descripcion,
                                id_soporte_tipo,
                                soporte_num,
                                soporte_fec,
                                observacion,
                                id_user_insert,
                                id_user_update,
                                id_user_edit)
                        VALUES (
                                i_id_movimiento_tipo,
                                i_sujeto,
                                i_descripcion,
                                i_id_soporte_tipo,
                                i_soporte_num,
                                i_soporte_fec,
                                i_observacion,
                                i_id_user,
                                i_id_user,
                                i_id_user)
                        --RETURNING id INTO v_id;    

                RETURNING id::character varying, movimiento_num, movimiento_fec::character varying INTO v_id, v_number, v_date;                        
                o_return:= array_to_json(array['C', v_id, v_number, v_date]);

                                            
                        --o_return:= array_to_json(array['C', v_id::character varying]);
                --ELSE
                --        o_return:= array_to_json(array['T']);
                --END IF;
        ELSE
                UPDATE public.movimiento SET
                        sujeto=i_sujeto,
                        descripcion=i_descripcion,
                        id_soporte_tipo=i_id_soporte_tipo,
                        soporte_num=i_soporte_num,
                        soporte_fec=i_soporte_fec,
                        observacion=i_observacion,
                        id_user_update=i_id_user,
                        id_user_edit=i_id_user,
                        date_update=('now'::text)::date,
                        time_update=('now'::text)::time without time zone
                WHERE id=i_id;
                        o_return:= array_to_json(array['A']);
        END IF;
        RETURN o_return;
END;
$$;


ALTER FUNCTION public.movimiento_register(i_id integer, i_id_movimiento_tipo integer, i_sujeto character varying, i_descripcion text, i_id_soporte_tipo integer, i_soporte_num character varying, i_soporte_fec date, i_observacion text, i_id_user integer) OWNER TO postgres;

--
-- TOC entry 268 (class 1255 OID 188640)
-- Name: movimiento_remove(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.movimiento_remove(i_id integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM public.movimiento WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$$;


ALTER FUNCTION public.movimiento_remove(i_id integer) OWNER TO postgres;

--
-- TOC entry 277 (class 1255 OID 189578)
-- Name: movimiento_reverse(integer, integer, character varying, text, integer, character varying, date, text, json[], integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.movimiento_reverse(i_id integer, i_id_movimiento_tipo integer, i_sujeto character varying, i_descripcion text, i_id_soporte_tipo integer, i_soporte_num character varying, i_soporte_fec date, i_observacion text, i_str_arr_table json[], i_id_user integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE
        v_existe boolean;
        v_id integer;
        o_return character;
BEGIN
        o_return:='C';
        IF i_id=0 THEN
		INSERT INTO public.movimiento(
			id_movimiento_tipo,
                        sujeto,
                        descripcion,
                        id_soporte_tipo,
                        soporte_num,
                        soporte_fec,
                        observacion,
                        id_user_insert,
                        id_user_update,
                        id_user_edit)
                VALUES (
                        i_id_movimiento_tipo,
                        i_sujeto,
                        i_descripcion,
                        i_id_soporte_tipo,
                        i_soporte_num,
                        i_soporte_fec,
                        i_observacion,
                        i_id_user,
                        i_id_user,
                        i_id_user)
                RETURNING id INTO v_id;                        
                o_return:= 'C';
        ELSE
                UPDATE public.movimiento SET
                        sujeto=i_sujeto,
                        descripcion=i_descripcion,
                        id_soporte_tipo=i_id_soporte_tipo,
                        soporte_num=i_soporte_num,
                        soporte_fec=i_soporte_fec,
                        observacion=i_observacion,
                        id_user_update=i_id_user,
                        id_user_edit=i_id_user,
                        date_update=('now'::text)::date,
                        time_update=('now'::text)::time without time zone
                WHERE id=i_id;
                        o_return:= 'A';
        END IF;
        IF  o_return='C' THEN --OR o_return='A'
                --DELETE FROM public.movimiento_aux WHERE id_movimiento = v_id; 
		FOR i IN array_lower(i_str_arr_table, 1) .. array_upper(i_str_arr_table, 1) LOOP	
                       SELECT public.movimiento_aux_register( 
                               (i_str_arr_table[i]->>'id')::integer, 
                               v_id, 
                               (i_str_arr_table[i]->>'id_presentacion')::integer, 
                               (i_str_arr_table[i]->>'cantidad')::integer, 
                               i_id_user
                       ) INTO o_return;				
		END LOOP;		
	END IF;	
        RETURN o_return;
END;
$$;


ALTER FUNCTION public.movimiento_reverse(i_id integer, i_id_movimiento_tipo integer, i_sujeto character varying, i_descripcion text, i_id_soporte_tipo integer, i_soporte_num character varying, i_soporte_fec date, i_observacion text, i_str_arr_table json[], i_id_user integer) OWNER TO postgres;

--
-- TOC entry 271 (class 1255 OID 188751)
-- Name: view_presentacion_despliegue(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.view_presentacion_despliegue(i_id integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE
        o_return character varying;
        v_arr json[];
        v_uni_med character varying;
        v_arr_union character varying[];
        v_n integer;
BEGIN
         o_return := '';
         SELECT (ARRAY(select json_array_elements(empaque_json::json))), medida_unidad  INTO v_arr, v_uni_med FROM view_presentacion WHERE id=i_id;
         v_arr_union = ARRAY[' DE ', ' CON ']; 
         v_n = 1;        
         FOR i IN array_lower(v_arr, 1) .. array_upper(v_arr, 1) LOOP                  
                  IF  i = 1 THEN
                      o_return := v_arr[i]->'packing' || v_arr_union[v_n] || (v_arr[i]->'quantity'::text) || ' ' || v_uni_med;
                  ELSE
                      o_return := (v_arr[i]->'packing') || v_arr_union[v_n] || (v_arr[i]->'quantity'::text) || ' ' || o_return;
                  END IF;
                  v_n = v_n + 1;
                  IF v_n = 3 THEN
                      v_n = 1;
                  END IF;
         END LOOP;
         
        RETURN replace(o_return, '"', '');
END;
$$;


ALTER FUNCTION public.view_presentacion_despliegue(i_id integer) OWNER TO postgres;

--
-- TOC entry 284 (class 1255 OID 198056)
-- Name: view_total_antes_cierre(integer, date); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.view_total_antes_cierre(i_id_presentacion integer, i_cierre_dia date) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
DECLARE
        o_return numeric;
        
BEGIN
       o_return =0;
       SELECT 
        sum(cantidad_entrante - cantidad_reverso_entrante - (cantidad_saliente - cantidad_reverso_saliente))
       
       INTO o_return
       FROM public.cierre_diario 
       WHERE id_presentacion = i_id_presentacion AND cierre_dia < i_cierre_dia
       GROUP BY id_presentacion;         
       RETURN COALESCE(o_return,0);
END;
$$;


ALTER FUNCTION public.view_total_antes_cierre(i_id_presentacion integer, i_cierre_dia date) OWNER TO postgres;

--
-- TOC entry 1812 (class 1417 OID 188668)
-- Name: localhost_negocio_producto; Type: SERVER; Schema: -; Owner: postgres
--

CREATE SERVER localhost_negocio_producto FOREIGN DATA WRAPPER postgres_fdw OPTIONS (
    dbname 'negocio_producto',
    host 'localhost',
    port '5432'
);


ALTER SERVER localhost_negocio_producto OWNER TO postgres;

--
-- TOC entry 2364 (class 0 OID 0)
-- Dependencies: 1812
-- Name: USER MAPPING postgres SERVER localhost_negocio_producto; Type: USER MAPPING; Schema: -; Owner: postgres
--

CREATE USER MAPPING FOR postgres SERVER localhost_negocio_producto OPTIONS (
    password 'postgres',
    "user" 'postgres'
);


--
-- TOC entry 1811 (class 1417 OID 188660)
-- Name: localhost_negocio_seguridad; Type: SERVER; Schema: -; Owner: postgres
--

CREATE SERVER localhost_negocio_seguridad FOREIGN DATA WRAPPER postgres_fdw OPTIONS (
    dbname 'negocio_seguridad',
    host 'localhost',
    port '5432'
);


ALTER SERVER localhost_negocio_seguridad OWNER TO postgres;

--
-- TOC entry 2365 (class 0 OID 0)
-- Dependencies: 1811
-- Name: USER MAPPING postgres SERVER localhost_negocio_seguridad; Type: USER MAPPING; Schema: -; Owner: postgres
--

CREATE USER MAPPING FOR postgres SERVER localhost_negocio_seguridad OPTIONS (
    password 'postgres',
    "user" 'postgres'
);


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 202 (class 1259 OID 189392)
-- Name: cierre_anual; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cierre_anual (
    id integer NOT NULL,
    id_presentacion integer NOT NULL,
    cantidad_entrante integer DEFAULT 0 NOT NULL,
    cantidad_saliente integer DEFAULT 0 NOT NULL,
    cantidad_reverso_entrante integer DEFAULT 0 NOT NULL,
    cantidad_reverso_saliente integer DEFAULT 0 NOT NULL,
    cierre_ano integer NOT NULL,
    id_user_insert integer DEFAULT 1 NOT NULL,
    date_insert date DEFAULT ('now'::text)::date NOT NULL,
    time_insert time(0) without time zone DEFAULT ('now'::text)::time without time zone NOT NULL
);


ALTER TABLE public.cierre_anual OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 189390)
-- Name: cierre_anual_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cierre_anual_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cierre_anual_id_seq OWNER TO postgres;

--
-- TOC entry 2366 (class 0 OID 0)
-- Dependencies: 201
-- Name: cierre_anual_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cierre_anual_id_seq OWNED BY public.cierre_anual.id;


--
-- TOC entry 198 (class 1259 OID 189362)
-- Name: cierre_diario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cierre_diario (
    id integer NOT NULL,
    id_presentacion integer NOT NULL,
    cantidad_entrante integer DEFAULT 0 NOT NULL,
    cantidad_saliente integer DEFAULT 0 NOT NULL,
    cantidad_reverso_entrante integer DEFAULT 0 NOT NULL,
    cantidad_reverso_saliente integer DEFAULT 0 NOT NULL,
    cierre_dia date NOT NULL,
    id_user_insert integer DEFAULT 1 NOT NULL,
    date_insert date DEFAULT ('now'::text)::date NOT NULL,
    time_insert time(0) without time zone DEFAULT ('now'::text)::time without time zone NOT NULL
);


ALTER TABLE public.cierre_diario OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 189360)
-- Name: cierre_diario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cierre_diario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cierre_diario_id_seq OWNER TO postgres;

--
-- TOC entry 2367 (class 0 OID 0)
-- Dependencies: 197
-- Name: cierre_diario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cierre_diario_id_seq OWNED BY public.cierre_diario.id;


--
-- TOC entry 200 (class 1259 OID 189377)
-- Name: cierre_mensual; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cierre_mensual (
    id integer NOT NULL,
    id_presentacion integer NOT NULL,
    cantidad_entrante integer DEFAULT 0 NOT NULL,
    cantidad_saliente integer DEFAULT 0 NOT NULL,
    cantidad_reverso_entrante integer DEFAULT 0 NOT NULL,
    cantidad_reverso_saliente integer DEFAULT 0 NOT NULL,
    cierre_mes integer NOT NULL,
    id_user_insert integer DEFAULT 1 NOT NULL,
    date_insert date DEFAULT ('now'::text)::date NOT NULL,
    time_insert time(0) without time zone DEFAULT ('now'::text)::time without time zone NOT NULL
);


ALTER TABLE public.cierre_mensual OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 189375)
-- Name: cierre_mensual_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cierre_mensual_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cierre_mensual_id_seq OWNER TO postgres;

--
-- TOC entry 2368 (class 0 OID 0)
-- Dependencies: 199
-- Name: cierre_mensual_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cierre_mensual_id_seq OWNED BY public.cierre_mensual.id;


--
-- TOC entry 193 (class 1259 OID 188601)
-- Name: movimiento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.movimiento (
    id integer NOT NULL,
    id_movimiento_tipo integer NOT NULL,
    movimiento_num character varying(27) NOT NULL,
    movimiento_fec date DEFAULT ('now'::text)::date NOT NULL,
    sujeto character varying(40) NOT NULL,
    descripcion text NOT NULL,
    id_soporte_tipo integer NOT NULL,
    soporte_num character varying(27) NOT NULL,
    soporte_fec date,
    observacion text NOT NULL,
    cierre_fec date,
    id_user_insert integer DEFAULT 1 NOT NULL,
    id_user_update integer DEFAULT 1 NOT NULL,
    id_user_edit integer DEFAULT 1 NOT NULL,
    editing integer DEFAULT 1 NOT NULL,
    date_insert date DEFAULT ('now'::text)::date NOT NULL,
    date_update date DEFAULT ('now'::text)::date NOT NULL,
    time_insert time(0) without time zone DEFAULT ('now'::text)::time without time zone NOT NULL,
    time_update time(0) without time zone DEFAULT ('now'::text)::time without time zone NOT NULL
);


ALTER TABLE public.movimiento OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 188580)
-- Name: movimiento_aux; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.movimiento_aux (
    id integer NOT NULL,
    id_movimiento integer NOT NULL,
    id_presentacion integer NOT NULL,
    cantidad integer DEFAULT 0 NOT NULL,
    cierre_fec date,
    id_user_insert integer NOT NULL,
    id_user_update integer NOT NULL,
    date_insert date DEFAULT ('now'::text)::date NOT NULL,
    date_update date DEFAULT ('now'::text)::date NOT NULL,
    time_insert time(0) without time zone DEFAULT ('now'::text)::time without time zone NOT NULL,
    time_update time(0) without time zone DEFAULT ('now'::text)::time without time zone NOT NULL
);


ALTER TABLE public.movimiento_aux OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 188578)
-- Name: movimiento_aux_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.movimiento_aux_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.movimiento_aux_id_seq OWNER TO postgres;

--
-- TOC entry 2369 (class 0 OID 0)
-- Dependencies: 190
-- Name: movimiento_aux_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.movimiento_aux_id_seq OWNED BY public.movimiento_aux.id;


--
-- TOC entry 192 (class 1259 OID 188599)
-- Name: movimiento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.movimiento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.movimiento_id_seq OWNER TO postgres;

--
-- TOC entry 2370 (class 0 OID 0)
-- Dependencies: 192
-- Name: movimiento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.movimiento_id_seq OWNED BY public.movimiento.id;


--
-- TOC entry 188 (class 1259 OID 188529)
-- Name: movimiento_tipo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.movimiento_tipo (
    id integer NOT NULL,
    nombre character varying
);


ALTER TABLE public.movimiento_tipo OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 188662)
-- Name: seguridad_usuario; Type: FOREIGN TABLE; Schema: public; Owner: postgres
--

CREATE FOREIGN TABLE public.seguridad_usuario (
    id integer,
    usuario character varying,
    correo character varying,
    cedula character varying,
    nombre character varying,
    apellido character varying
)
SERVER localhost_negocio_seguridad
OPTIONS (
    schema_name 'seguridad',
    table_name 'usuario'
);


ALTER FOREIGN TABLE public.seguridad_usuario OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 188537)
-- Name: soporte_tipo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.soporte_tipo (
    id integer NOT NULL,
    nombre character varying,
    id_movimiento_tipo integer
);


ALTER TABLE public.soporte_tipo OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 188705)
-- Name: view_categoria; Type: FOREIGN TABLE; Schema: public; Owner: postgres
--

CREATE FOREIGN TABLE public.view_categoria (
    id integer,
    nombre character varying,
    familia text,
    padre_id integer,
    nivel integer,
    final boolean
)
SERVER localhost_negocio_producto
OPTIONS (
    schema_name 'public',
    table_name 'view_categoria'
);


ALTER FOREIGN TABLE public.view_categoria OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 198030)
-- Name: view_cierre_mov_fec_presentacion; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.view_cierre_mov_fec_presentacion AS
 SELECT DISTINCT b.movimiento_fec,
    a.id_presentacion
   FROM (public.movimiento_aux a
     LEFT JOIN public.movimiento b ON ((a.id_movimiento = b.id)))
  WHERE ((a.cierre_fec IS NULL) AND (b.cierre_fec IS NULL))
  ORDER BY b.movimiento_fec, a.id_presentacion;


ALTER TABLE public.view_cierre_mov_fec_presentacion OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 198019)
-- Name: view_cierre_pre_insert_aux; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.view_cierre_pre_insert_aux AS
 SELECT b.movimiento_fec,
    a.id_presentacion,
    sum(a.cantidad) AS sum,
    b.id_movimiento_tipo
   FROM (public.movimiento_aux a
     JOIN public.movimiento b ON ((a.id_movimiento = b.id)))
  WHERE ((a.cierre_fec IS NULL) AND (b.cierre_fec IS NULL))
  GROUP BY b.movimiento_fec, a.id_presentacion, b.id_movimiento_tipo
  ORDER BY b.movimiento_fec, a.id_presentacion, b.id_movimiento_tipo;


ALTER TABLE public.view_cierre_pre_insert_aux OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 198035)
-- Name: view_cierre_pre_insert; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.view_cierre_pre_insert AS
 SELECT a.movimiento_fec,
    a.id_presentacion,
        CASE
            WHEN (sum(b.sum) IS NULL) THEN ((0)::bigint)::numeric
            ELSE sum(b.sum)
        END AS cantidad_entrante,
        CASE
            WHEN (sum(c.sum) IS NULL) THEN ((0)::bigint)::numeric
            ELSE sum(c.sum)
        END AS cantidad_saliente,
        CASE
            WHEN (sum(d.sum) IS NULL) THEN ((0)::bigint)::numeric
            ELSE sum(d.sum)
        END AS cantidad_reverso_entrante,
        CASE
            WHEN (sum(e.sum) IS NULL) THEN ((0)::bigint)::numeric
            ELSE sum(e.sum)
        END AS cantidad_reverso_saliente
   FROM ((((public.view_cierre_mov_fec_presentacion a
     LEFT JOIN public.view_cierre_pre_insert_aux b ON (((a.id_presentacion = b.id_presentacion) AND (b.id_movimiento_tipo = 1) AND (a.movimiento_fec = b.movimiento_fec))))
     LEFT JOIN public.view_cierre_pre_insert_aux c ON (((a.id_presentacion = c.id_presentacion) AND (c.id_movimiento_tipo = 2) AND (a.movimiento_fec = c.movimiento_fec))))
     LEFT JOIN public.view_cierre_pre_insert_aux d ON (((a.id_presentacion = d.id_presentacion) AND (d.id_movimiento_tipo = 3) AND (a.movimiento_fec = d.movimiento_fec))))
     LEFT JOIN public.view_cierre_pre_insert_aux e ON (((a.id_presentacion = e.id_presentacion) AND (e.id_movimiento_tipo = 4) AND (a.movimiento_fec = e.movimiento_fec))))
  GROUP BY a.movimiento_fec, a.id_presentacion
  ORDER BY a.movimiento_fec, a.id_presentacion;


ALTER TABLE public.view_cierre_pre_insert OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 189431)
-- Name: view_presentacion; Type: FOREIGN TABLE; Schema: public; Owner: postgres
--

CREATE FOREIGN TABLE public.view_presentacion (
    id integer,
    categoria text,
    producto character varying,
    marca character varying,
    empaque_json jsonb,
    medida_unidad character varying,
    precio_costo double precision,
    precio_venta double precision,
    bar_cod character varying,
    int_cod character varying
)
SERVER localhost_negocio_producto
OPTIONS (
    schema_name 'public',
    table_name 'view_presentacion'
);


ALTER FOREIGN TABLE public.view_presentacion OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 189812)
-- Name: view_existencia_cierre_diario; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.view_existencia_cierre_diario AS
 SELECT b.categoria,
    b.producto,
    b.marca,
    a.id_presentacion,
    ( SELECT public.view_presentacion_despliegue(a.id_presentacion) AS view_presentacion_despliegue) AS presentacion,
        CASE
            WHEN (a.cantidad_entrante IS NULL) THEN 0
            ELSE a.cantidad_entrante
        END AS entradas,
        CASE
            WHEN (a.cantidad_saliente IS NULL) THEN 0
            ELSE a.cantidad_saliente
        END AS salidas,
        CASE
            WHEN (a.cantidad_reverso_entrante IS NULL) THEN 0
            ELSE a.cantidad_reverso_entrante
        END AS reverso_entradas,
        CASE
            WHEN (a.cantidad_reverso_saliente IS NULL) THEN 0
            ELSE a.cantidad_reverso_saliente
        END AS reverso_salidas,
    ((COALESCE(a.cantidad_entrante, 0) - COALESCE(a.cantidad_reverso_entrante, 0)) - (COALESCE(a.cantidad_saliente, 0) - COALESCE(a.cantidad_reverso_saliente, 0))) AS total
   FROM (public.cierre_diario a
     JOIN public.view_presentacion b ON ((a.id_presentacion = b.id)))
  WHERE true;


ALTER TABLE public.view_existencia_cierre_diario OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 189802)
-- Name: view_presentacion_cantidad_entrada; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.view_presentacion_cantidad_entrada AS
 SELECT a.id_presentacion,
    sum(a.cantidad) AS cantidad
   FROM (public.movimiento_aux a
     JOIN public.movimiento b ON (((a.id_movimiento = b.id) AND (b.cierre_fec IS NULL))))
  WHERE (b.id_movimiento_tipo = 1)
  GROUP BY a.id_presentacion;


ALTER TABLE public.view_presentacion_cantidad_entrada OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 189797)
-- Name: view_presentacion_cantidad_reverso_entrada; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.view_presentacion_cantidad_reverso_entrada AS
 SELECT a.id_presentacion,
    sum(a.cantidad) AS cantidad
   FROM (public.movimiento_aux a
     JOIN public.movimiento b ON (((a.id_movimiento = b.id) AND (b.cierre_fec IS NULL))))
  WHERE (b.id_movimiento_tipo = 3)
  GROUP BY a.id_presentacion;


ALTER TABLE public.view_presentacion_cantidad_reverso_entrada OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 189792)
-- Name: view_presentacion_cantidad_reverso_salida; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.view_presentacion_cantidad_reverso_salida AS
 SELECT a.id_presentacion,
    sum(a.cantidad) AS cantidad
   FROM (public.movimiento_aux a
     JOIN public.movimiento b ON (((a.id_movimiento = b.id) AND (b.cierre_fec IS NULL))))
  WHERE (b.id_movimiento_tipo = 4)
  GROUP BY a.id_presentacion;


ALTER TABLE public.view_presentacion_cantidad_reverso_salida OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 189787)
-- Name: view_presentacion_cantidad_salida; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.view_presentacion_cantidad_salida AS
 SELECT a.id_presentacion,
    sum(a.cantidad) AS cantidad
   FROM (public.movimiento_aux a
     JOIN public.movimiento b ON (((a.id_movimiento = b.id) AND (b.cierre_fec IS NULL))))
  WHERE (b.id_movimiento_tipo = 2)
  GROUP BY a.id_presentacion;


ALTER TABLE public.view_presentacion_cantidad_salida OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 189807)
-- Name: view_existencia_movimiento; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.view_existencia_movimiento AS
 SELECT DISTINCT b.categoria,
    b.producto,
    b.marca,
    a.id_presentacion,
    ( SELECT public.view_presentacion_despliegue(a.id_presentacion) AS view_presentacion_despliegue) AS presentacion,
        CASE
            WHEN (e.cantidad IS NULL) THEN (0)::bigint
            ELSE e.cantidad
        END AS entradas,
        CASE
            WHEN (s.cantidad IS NULL) THEN (0)::bigint
            ELSE s.cantidad
        END AS salidas,
        CASE
            WHEN (re.cantidad IS NULL) THEN (0)::bigint
            ELSE re.cantidad
        END AS reverso_entradas,
        CASE
            WHEN (rs.cantidad IS NULL) THEN (0)::bigint
            ELSE rs.cantidad
        END AS reverso_salidas,
    ((COALESCE(e.cantidad, (0)::bigint) - COALESCE(re.cantidad, (0)::bigint)) - (COALESCE(s.cantidad, (0)::bigint) - COALESCE(rs.cantidad, (0)::bigint))) AS total
   FROM (((((public.movimiento_aux a
     JOIN public.view_presentacion b ON ((a.id_presentacion = b.id)))
     LEFT JOIN ( SELECT view_presentacion_cantidad_entrada.id_presentacion,
            view_presentacion_cantidad_entrada.cantidad
           FROM public.view_presentacion_cantidad_entrada) e ON ((a.id_presentacion = e.id_presentacion)))
     LEFT JOIN ( SELECT view_presentacion_cantidad_salida.id_presentacion,
            view_presentacion_cantidad_salida.cantidad
           FROM public.view_presentacion_cantidad_salida) s ON ((a.id_presentacion = s.id_presentacion)))
     LEFT JOIN ( SELECT view_presentacion_cantidad_reverso_entrada.id_presentacion,
            view_presentacion_cantidad_reverso_entrada.cantidad
           FROM public.view_presentacion_cantidad_reverso_entrada) re ON ((a.id_presentacion = re.id_presentacion)))
     LEFT JOIN ( SELECT view_presentacion_cantidad_reverso_salida.id_presentacion,
            view_presentacion_cantidad_reverso_salida.cantidad
           FROM public.view_presentacion_cantidad_reverso_salida) rs ON ((a.id_presentacion = rs.id_presentacion)))
  WHERE (a.cierre_fec IS NULL);


ALTER TABLE public.view_existencia_movimiento OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 189817)
-- Name: view_existencia; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.view_existencia AS
 SELECT alias.categoria,
    alias.producto,
    alias.marca,
    alias.id_presentacion,
    alias.presentacion,
    sum(alias.entradas) AS entradas,
    sum(alias.salidas) AS salidas,
    sum(alias.reverso_entradas) AS reverso_entradas,
    sum(alias.reverso_salidas) AS reverso_salidas,
    sum(alias.total) AS total
   FROM ( SELECT view_existencia_movimiento.categoria,
            view_existencia_movimiento.producto,
            view_existencia_movimiento.marca,
            view_existencia_movimiento.id_presentacion,
            view_existencia_movimiento.presentacion,
            view_existencia_movimiento.entradas,
            view_existencia_movimiento.salidas,
            view_existencia_movimiento.reverso_entradas,
            view_existencia_movimiento.reverso_salidas,
            view_existencia_movimiento.total
           FROM public.view_existencia_movimiento
        UNION ALL
         SELECT view_existencia_cierre_diario.categoria,
            view_existencia_cierre_diario.producto,
            view_existencia_cierre_diario.marca,
            view_existencia_cierre_diario.id_presentacion,
            view_existencia_cierre_diario.presentacion,
            view_existencia_cierre_diario.entradas,
            view_existencia_cierre_diario.salidas,
            view_existencia_cierre_diario.reverso_entradas,
            view_existencia_cierre_diario.reverso_salidas,
            view_existencia_cierre_diario.total
           FROM public.view_existencia_cierre_diario) alias
  GROUP BY alias.categoria, alias.producto, alias.marca, alias.id_presentacion, alias.presentacion;


ALTER TABLE public.view_existencia OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 188708)
-- Name: view_producto; Type: FOREIGN TABLE; Schema: public; Owner: postgres
--

CREATE FOREIGN TABLE public.view_producto (
    id integer,
    categoria text,
    producto character varying,
    marca character varying
)
SERVER localhost_negocio_producto
OPTIONS (
    schema_name 'public',
    table_name 'view_producto'
);


ALTER FOREIGN TABLE public.view_producto OWNER TO postgres;

--
-- TOC entry 2197 (class 2604 OID 189395)
-- Name: cierre_anual id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cierre_anual ALTER COLUMN id SET DEFAULT nextval('public.cierre_anual_id_seq'::regclass);


--
-- TOC entry 2181 (class 2604 OID 189365)
-- Name: cierre_diario id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cierre_diario ALTER COLUMN id SET DEFAULT nextval('public.cierre_diario_id_seq'::regclass);


--
-- TOC entry 2189 (class 2604 OID 189380)
-- Name: cierre_mensual id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cierre_mensual ALTER COLUMN id SET DEFAULT nextval('public.cierre_mensual_id_seq'::regclass);


--
-- TOC entry 2172 (class 2604 OID 188604)
-- Name: movimiento id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movimiento ALTER COLUMN id SET DEFAULT nextval('public.movimiento_id_seq'::regclass);


--
-- TOC entry 2165 (class 2604 OID 188583)
-- Name: movimiento_aux id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movimiento_aux ALTER COLUMN id SET DEFAULT nextval('public.movimiento_aux_id_seq'::regclass);


--
-- TOC entry 2222 (class 2606 OID 189404)
-- Name: cierre_anual cierre_anual_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cierre_anual
    ADD CONSTRAINT cierre_anual_pkey PRIMARY KEY (id);


--
-- TOC entry 2218 (class 2606 OID 189374)
-- Name: cierre_diario cierre_diario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cierre_diario
    ADD CONSTRAINT cierre_diario_pkey PRIMARY KEY (id);


--
-- TOC entry 2220 (class 2606 OID 189389)
-- Name: cierre_mensual cierre_mensual_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cierre_mensual
    ADD CONSTRAINT cierre_mensual_pkey PRIMARY KEY (id);


--
-- TOC entry 2214 (class 2606 OID 188619)
-- Name: movimiento movements_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movimiento
    ADD CONSTRAINT movements_pkey PRIMARY KEY (id);


--
-- TOC entry 2210 (class 2606 OID 188590)
-- Name: movimiento_aux movemovimiento_aux_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movimiento_aux
    ADD CONSTRAINT movemovimiento_aux_pkey PRIMARY KEY (id);


--
-- TOC entry 2212 (class 2606 OID 189826)
-- Name: movimiento_aux movimiento_aux_id_movimiento_id_presentacion_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movimiento_aux
    ADD CONSTRAINT movimiento_aux_id_movimiento_id_presentacion_key UNIQUE (id_movimiento, id_presentacion);


--
-- TOC entry 2216 (class 2606 OID 188756)
-- Name: movimiento movimiento_movimiento_num_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movimiento
    ADD CONSTRAINT movimiento_movimiento_num_key UNIQUE (movimiento_num);


--
-- TOC entry 2206 (class 2606 OID 188536)
-- Name: movimiento_tipo movimiento_tipo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movimiento_tipo
    ADD CONSTRAINT movimiento_tipo_pkey PRIMARY KEY (id);


--
-- TOC entry 2208 (class 2606 OID 188544)
-- Name: soporte_tipo soporte_tipo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.soporte_tipo
    ADD CONSTRAINT soporte_tipo_pkey PRIMARY KEY (id);


--
-- TOC entry 2226 (class 2620 OID 188632)
-- Name: movimiento generar_numero; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER generar_numero BEFORE INSERT ON public.movimiento FOR EACH ROW EXECUTE PROCEDURE public.generar_numero();


--
-- TOC entry 2223 (class 2606 OID 188634)
-- Name: movimiento_aux movimiento_aux_id_movimiento_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movimiento_aux
    ADD CONSTRAINT movimiento_aux_id_movimiento_fkey FOREIGN KEY (id_movimiento) REFERENCES public.movimiento(id);


--
-- TOC entry 2224 (class 2606 OID 188622)
-- Name: movimiento movimiento_id_movimiento_tipo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movimiento
    ADD CONSTRAINT movimiento_id_movimiento_tipo_fkey FOREIGN KEY (id_movimiento_tipo) REFERENCES public.movimiento_tipo(id);


--
-- TOC entry 2225 (class 2606 OID 188627)
-- Name: movimiento movimiento_id_soporte_tipo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movimiento
    ADD CONSTRAINT movimiento_id_soporte_tipo_fkey FOREIGN KEY (id_soporte_tipo) REFERENCES public.soporte_tipo(id);


-- Completed on 2020-07-11 22:25:14 -04

--
-- PostgreSQL database dump complete
--

