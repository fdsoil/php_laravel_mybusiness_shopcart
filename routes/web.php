<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Exception routes
Route::get('exception/index', 'ExceptionController@index');

Route::get('/welcome', function () {
    return view('welcome');
});

Auth::routes(['verify' => true, 'register' => false]);
Route::get('/', function () { return redirect('login'); });
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth', 'check_user_is_client']], function () {
    Route::get('/shopcart', 'ShopCartController@index')->name('shopcart.index');    
    Route::get('/shopcart/list/{id}', 'ShopCartController@list')->name('shopcart.list');        
    Route::get('/shopcart/show/{id}', 'ShopCartController@show')->name('shopcart.show');    
    Route::get('/shopcart/pdf/{id}', 'ShopCartController@pdf')->name('shopcart.pdf');
    Route::get('/payment', 'PaymentController@pay');
    Route::post('/voucher', 'VoucherController@register');
    Route::get('/article/presentation/{ids}', 'ArticleController@getByPresentation');
});

  

Route::group(['middleware' => ['auth', 'check_user_is_admin']], function () {

    Route::get('/register',  'UsersController@create')->name('register');
    Route::post('/register', 'UsersController@register');
    Route::resource('user', 'UsersController');  
    Route::resource('client', 'ClientController')->parameters([
        'client' => 'email'
    ]);    

    Route::prefix('menu')->group(function () {
        Route::get('/', 'Admin\MenuController@index')->name('menu');
        Route::get('/create', 'Admin\MenuController@create')->name('menu/create');
        Route::get('/edit/{id}', 'Admin\MenuController@edit')->name('menu/edit/id');
        Route::get('/get/{id}', 'Admin\MenuController@get');
        Route::get('/recursive/', 'Admin\MenuController@recursive');
        Route::get('/children/{id}', 'Admin\MenuController@children');
        Route::post('/store', 'Admin\MenuController@store');
        Route::put('/', 'Admin\MenuController@update');
        Route::delete('/{id}', 'Admin\MenuController@destroy');    
    });
    
    Route::prefix('role')->group(function () {
        Route::get('/', 'Admin\RoleController@index')->name('role');
       //Route::get('/', 'Admin\RoleController@get');    
    });

    Route::prefix('voucher')->group(function () {
        Route::get('/', 'VoucherController@index')->name('voucher.index');
        Route::get('/{id}', 'VoucherController@show')->name('voucher.show');
    });
        
    Route::prefix('replacement')->group(function () {
        Route::get('/', 'ReplacementController@index')->name('replacement.index');
        Route::get('/create', 'ReplacementController@create')->name('replacement.create');
        Route::get('/show/{replacement}', 'ReplacementController@show')->name('replacement.show');
        Route::get('/edit/{replacement}', 'ReplacementController@edit')->name('replacement.edit');    
        Route::post('/', 'ReplacementController@store')->name('replacement.store');
        Route::put('/{replacement}', 'ReplacementController@update')->name('replacement.update');
        Route::post('/detail', 'ReplacementDetailController@store')->name('replacement.detail.store');
        Route::put('/detail/{replacementDetail}', 'ReplacementDetailController@update')->name('replacement.detail.update');
        Route::delete('/detail/{replacementDetail}', 'ReplacementDetailController@destroy')->name('replacement.detail.destroy');
    });
    
    Route::prefix('movement')->group(function () {
      Route::get('/', 'MovementController@index')->name('movement.index');
      Route::get('/show/{movement}', 'MovementController@show')->name('movement.show');    
    });
    
    Route::prefix('stock')->group(function () {
      Route::get('/', 'StockController@index')->name('stock.index');    
    });
    
    Route::prefix('article')->group(function () {
      Route::get('/', 'ArticleController@index')->name('article.index');
      Route::get('/get', 'ArticleController@get')->name('article.get');      
      Route::get('/search/{value}', 'ArticleController@search')->name('article.search');
      Route::post('/', 'ArticleController@store')->name('article.store');
      Route::put('/{article}', 'ArticleController@update')->name('article.update');
      Route::post('/photo', 'ArticleController@photo')->name('article.photo');
      Route::delete('/{article}', 'ArticleController@destroy')->name('article.destroy');
      Route::put('/restore/{article}', 'ArticleController@restore')->name('article.restore');
    });

    Route::prefix('article-detail')->group(function () {
      Route::get('/{articleId}', 'ArticleDetailController@index')->name('article-detail.index');
      Route::post('/', 'ArticleDetailController@store')->name('article-detail.store');
      Route::delete('/{articleDetail}', 'ArticleDetailController@destroy')->name('article-detail.destroy');
    });
     
});








