window.Arr = (function() {

  return {
  
    in: function (needle, haystack, argStrict) {
      var key = '';
      var strict = !!argStrict;
      if (strict) {
        for (key in haystack) {
          if (haystack[key] === needle) {
            return true;
          }
        }
      } else {
        for (key in haystack) {
          if (haystack[key] == needle) { 
            return true;
          }
        }
      }
      return false;
    }
  
  }  

})();

