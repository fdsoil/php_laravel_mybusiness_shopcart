window.Number = (() => {

  return {
  
    formato_float_3d(fld, milSep, decSep, e) {
      var tipo = e.keyCode;
      if ( tipo == 8 ) {  // 3 8,37,39,46
        return true;
      }
      var sep = 0;
      var key = '';
      var i = 0;
      var j = 0;
      var len = 0;
      var len2 = 0;
      var strCheck = '0123456789';
      var aux = '';
      var aux2 = '';
      var whichCode = (window.Event) ? e.which : e.keyCode;
      //if (whichCode == 13) return true; // Enter
      key = String.fromCharCode(whichCode); // Get key value from key code
      if ( strCheck.indexOf(key) == -1 )
        return false; // Not a valid key
      len = fld.value.length;
      for ( i = 0; i < len; i++ )
        if ( ( fld.value.charAt(i) != '0' ) && ( fld.value.charAt(i) != decSep ) )
          break;
        aux = '';
        for ( ; i < len; i++ )
          if ( strCheck.indexOf( fld.value.charAt(i) ) != -1 )
            aux += fld.value.charAt(i);
        aux += key;
        len = aux.length;
        if ( len == 0 )
          fld.value = '';
        if ( len == 1 )
          fld.value = '0'+ decSep + '00' + aux;
        if ( len == 2 )
          fld.value = '0'+ decSep + '0' + aux;
        if ( len == 3 )
          fld.value = '0'+ decSep + aux;
        if ( len > 3 ) {
          aux2 = '';
          for ( j = 0, i = len - 4; i >= 0; i-- ) {
            if ( j == 3 ) {
              aux2 += milSep;
              j = 0;
            }
            aux2 += aux.charAt(i);
            j++;
          }
          fld.value = '';
          len2 = aux2.length;
          for ( i = len2 - 1; i >= 0; i-- )
            fld.value += aux2.charAt(i);
          fld.value += decSep + aux.substr(len - 3, len);
        }
        return false;
      },

      formato_float(fld, milSep, decSep, e) {
        var tipo=e.keyCode;
        if ( tipo == 8 ) { // 3 8,37,39,46
          return true;
        }
        var sep = 0;
        var key = '';
        var i = 0;
        var j = 0;
        var len = 0;
        var len2 = 0;
        var strCheck = '0123456789';
        var aux = '';
        var aux2 = '';
        var whichCode = (window.Event) ? e.which : e.keyCode;
        //if (whichCode == 13) return true; // Enter
        key = String.fromCharCode(whichCode); // Get key value from key code
        if ( strCheck.indexOf(key) == -1 )
          return false; // Not a valid key
        len = fld.value.length;
        for ( i = 0; i < len; i++ )
          if ( ( fld.value.charAt(i) != '0' ) && ( fld.value.charAt(i) != decSep ) )
            break;
        aux = '';
        for ( ; i < len; i++ )
          if ( strCheck.indexOf( fld.value.charAt(i) ) != -1 )
            aux += fld.value.charAt(i);
        aux += key;
        len = aux.length;
        if ( len == 0 )
          fld.value = '';
        if ( len == 1 )
          fld.value = '0'+ decSep + '0' + aux;
        if ( len == 2 )
          fld.value = '0'+ decSep + aux;
        if ( len > 2 ) {
          aux2 = '';
          for ( j = 0, i = len - 3; i >= 0; i-- ) {
            if ( j == 3 ) {
              aux2 += milSep;
              j = 0;
            }
            aux2 += aux.charAt(i);
            j++;
          }
          fld.value = '';
          len2 = aux2.length;
          for ( i = len2 - 1; i >= 0; i-- )
            fld.value += aux2.charAt(i);
          fld.value += decSep + aux.substr(len - 2, len);
        }
        return false;
      },

      formato_numeric(fld, milSep, decSep, e) {
        var tipo=e.keyCode;
        if ( tipo == 8 )  // 3 8,37,39,46
          return true;
        var key = '';
        var i = 0;
        var j = 0;
        var len = 0;
        var len2 = 0;
        var strCheck = '0123456789';
        var aux = '';
        var aux2 = '';       
        var whichCode = (window.Event) ? e.which : e.keyCode;
        //if (whichCode == 13) return true; // Enter
        key = String.fromCharCode(whichCode); // Get key value from key code
        if ( strCheck.indexOf(key) == -1 )
          return false; // Not a valid key
        len = fld.value.length;
        for ( i = 0; i < len; i++ )
          if ( ( fld.value.charAt(i) != '0' ) )
            break;
        aux = '';
        for ( ; i < len; i++ )
          if ( strCheck.indexOf( fld.value.charAt(i) ) != -1 )
            aux += fld.value.charAt(i);
        aux += key;
        len = aux.length;
        aux2 = '';
        for ( j = 0, i = len - 1; i >= 0; i-- ) {
          if ( j == 3 ) {
            aux2 += milSep;
            j = 0;
          }
          aux2 += aux.charAt(i);
          j++;
        }
        fld.value = '';
        len2 = aux2.length;
        for ( i = len2 - 1; i >= 0; i-- )
          fld.value += aux2.charAt(i);
        fld.value += aux.substr(len - 0, len);
        return false;
      },
      
      putFloatFormat: function (numVal, sepMil,sepDec,cantDec) {
        let numStr = numVal.toFixed(cantDec).toString();
        let regExp = /\./;
        let array = [];       
        if (regExp.test(numStr))
          array = numStr.split('.');
        else {
          array[0] = numStr;
          array[1] = '0';
        }
        regExp=/(\d+)(\d{3})/;
	    while (regExp.test(array[0]))           
          array[0] = array[0].replace(regExp, '$1' + sepMil + '$2');
	    return array[0] + ( ( cantDec != 0 ) ? sepDec + array[1] : '' );
      },

      delFormat: function(val1) {
	    let val2='';
	    for ( let i=0; i < (val1.length); i++ ){
	      if (val1.charAt(i)!='.'){
            val2=val2+val1.charAt(i);
		  }
	    }
	    return val2.replace(',', '.');
      }
  
  }

})();


