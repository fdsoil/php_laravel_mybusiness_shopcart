window.Modal = ( ()=> {

  

  return {

    Confirm: (() => { //https://stackoverflow.com/questions/9334636/how-to-create-a-dialog-with-yes-and-no-options
      let div01;
      if ( typeof div01 === 'undefined') {
        div01 = document.createElement('div');      
      }
      return {

        chk: (ask = 'Quiere ejecutar esta acción?', title = 'Confirmar', val, func ) => {

          div01.id = 'id01';
          div01.className = "modal";
          const div02 = document.createElement('div');
          div02.className = "modal-content container";
          const h1 = document.createElement('h1');
          const head = document.createTextNode(title);
          h1.appendChild(head);
          div02.appendChild(h1);
          const p = document.createElement('p');
          const question = document.createTextNode(ask);
          p.appendChild(question);
          div02.appendChild(p);
          const div03 = document.createElement('div');
          div03.className = "clearfix";
          const btn1 = document.createElement('button');
          btn1.type = "button";
          btn1.setAttribute("onclick", `Modal.Confirm.cancel(${func}, ${val})`);
          btn1.className = "cancelbtn";
          const cancel = document.createTextNode("Cancelar");
          btn1.appendChild(cancel);
          div03.appendChild(btn1);
          const btn2 = document.createElement('button'); 
          btn2.type = "button";
          btn2.setAttribute("onclick", `Modal.Confirm.acept(${func}, ${val})`);
          btn2.className = "deletebtn";
          const acept = document.createTextNode("Aceptar");
          btn2.appendChild(acept);
          div03.appendChild(btn2);
          div02.appendChild(div03);
          div01.appendChild(div02);
          document.getElementsByTagName('body')[0].appendChild(div01);
          div01.style.display='block';
          return; 

        },

        cancel(callback, val) {
          document.getElementsByTagName('body')[0].removeChild(div01);
          return callback(false, val);
          //return;
        },

        acept(callback, val) {
          document.getElementsByTagName('body')[0].removeChild(div01);
          return callback(true, val);
          //return;
        }

      }
    })() 

  }

})();






