window.Constraints = ( () => {

  const addSetAttributeInGroup = (aObj,  attribute, property) => {
    for ( var i = 0; i < aObj.length; i++ )
      aObj[i].setAttribute( attribute, property );
  };

  const addEventListenerInGroup = (aObj, aEvent, oFun) => {
    for ( var i = 0; i < aObj.length; i++ )
      for ( var j = 0; j < aEvent.length; j++ )
        aObj[i].addEventListener( aEvent[j], oFun);
  };
  
    return {
      acceptOnlyRegExp( e, strRegExp ) {
          var tecla = ( document.all ) ? e.keyCode : e.which;
          if ( tecla == 8 )
            return true;
          let patron = strRegExp;
          let te;
          te = String.fromCharCode( tecla );
          return patron.test( te );
      },     
      
      set(obj) {
      const aEvents = [ "onkeypress" ];
      const fNoBlankSpaceLeft = " && ((this.value.length==0 && ((document.all)?event.keyCode:event.which)==32)?false:true);"; 

      /* Accepts Only Letters. */
      const fLetter = "return Constraints.acceptOnlyRegExp( event, /[a-zA-Z]/ );";
      addSetAttributeInGroup( obj.querySelectorAll( "[data-constraints=letter]" ), aEvents, fLetter );
      /* Accepts Only Numbers. */
      const fNumber = "return Constraints.acceptOnlyRegExp( event, /[0-9]/ );";
      addSetAttributeInGroup( obj.querySelectorAll( "[data-constraints=number]" ), aEvents, fNumber );
      /* Accepts Only Numbers and Letters. */
      const fCode = "return Constraints.acceptOnlyRegExp( event, /[0-9a-zA-Z]/ );";
      addSetAttributeInGroup( obj.querySelectorAll( "[data-constraints=code]" ), aEvents, fCode );
      /* Accepts Letters, Blanks, Points and Commas. */
      const fFullName = "return Constraints.acceptOnlyRegExp( event, /[a-zñA-ZÑ\\s.,]/ )" + fNoBlankSpaceLeft;
      addSetAttributeInGroup( obj.querySelectorAll( "[data-constraints=fullname]" ), aEvents, fFullName );
      /* Accepts Letters, Blanks, Points, Commas and Parentheses. */
      const fTextArea = "return Constraints.acceptOnlyRegExp( event, /[0-9a-zñA-ZÑ\\s.,()]/ )" + fNoBlankSpaceLeft;
      addSetAttributeInGroup( obj.querySelectorAll( "[data-constraints=textarea]" ), aEvents, fTextArea );
      /* Accepts Letters, Blanks, Points, Commas, Parentheses and Ampersand. */
      const fFirmName = "return Constraints.acceptOnlyRegExp( event, /[0-9a-zñA-ZÑ\\s.,()&]/ )" + fNoBlankSpaceLeft ;
      addSetAttributeInGroup( obj.querySelectorAll( "[data-constraints=firmname]" ), aEvents, fFirmName);
      /* Accepts Numbers, Letters, Points And Underscore. */
      const fUserName = "return Constraints.acceptOnlyRegExp( event, /[0-9a-zA-Z._]/ );";
      addSetAttributeInGroup( obj.querySelectorAll( "[data-constraints=username]"), aEvents, fUserName );
      /* Accepts Numbers, Letters, Points, Underscore, Guión And Arroba. */
      const fEmail = "return Constraints.acceptOnlyRegExp( event, /[0-9a-zA-Z.@_-]/ );";
      addSetAttributeInGroup( obj.querySelectorAll( "[data-constraints=email]" ), aEvents, fEmail);
      /* Accepts Numbers, 2 Points, Aa, Pp and Mm. */
      const fTime = "return Constraints.acceptOnlyRegExp( event, /[0-9AaMmPp:]/ );";
      addSetAttributeInGroup( obj.querySelectorAll( "[data-constraints=time]" ), aEvents, fTime);
      /* Accepts Numbers and guión ("-"). */
      const fDate = "return Constraints.acceptOnlyRegExp( event, /[0-9-]/ );";
      addSetAttributeInGroup( obj.querySelectorAll( "[data-constraints=date]" ), aEvents, fDate);
    }
    }
    
    })();
