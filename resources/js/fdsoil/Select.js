window.Select = (function () {
  let select;
  const empty = function () {
    while(select.options.length > 0)
      select.options[select.options.length-1] = null;    
  }
  const createOption = function (value, text) {
    let option = document.createElement("option");
    option.value = value;
    option.text = text;
    select.options.add(option);
  }
  const createGroupOption = function (aJson, oSetUp) {    
    let options = [];
    oSetUp = oSetUp ? oSetUp : {id: 0, text: 1, label: 2 };
    for (let i = 0; i < aJson.length; i++){
      options[i] = document.createElement("option");	
      options[i].value = aJson[i][oSetUp.id];
      options[i].text = aJson[i][oSetUp.text];
      if (aJson[i][oSetUp.label] != undefined)
          options[i].label = aJson[i][oSetUp.label];        
      select.appendChild(options[i]);
    }
  }
  return {
    fill: function (eSel, aJson, value, text, oSetUp) {
      select = eSel;
      empty();
      if (value!=null && text!=null) 
          createOption(value, text);
      createGroupOption(aJson, oSetUp); 
    }
  }
})();
