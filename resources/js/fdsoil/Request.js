window.Request = function ( oContainer, typeReqs='txt', url = false, b64 = false ) {

    var objsSelect = oContainer.getElementsByTagName("select");
    var objsTextArea = oContainer.getElementsByTagName("textarea");
    var objsInput = oContainer.getElementsByTagName("input");
    var objsRadioCheck = [];
    var k=0;

    if ( typeReqs === 'txt' || typeReqs === 'json' ) //( inArray( typeReqs, ['txt', 'json'] ) )
        var resp = '';
    else if (typeReqs === 'formdata')
        var data = new FormData();

    this.reqsAux = function(obj)
    {
        var valor = () => { return !url ? obj.value : encodeURIComponent(obj.value); }

        if ( typeReqs === 'txt' )
            return ( b64 ) 
                ? b64EncodeUnicode( obj.name ) + '=' + b64EncodeUnicode( valor() ) + '&' 
                : obj.name + '=' + valor() + '&';
        else if ( typeReqs === 'json' )
            return ( b64 ) 
                ? '"' + b64EncodeUnicode( obj.name ) + '" : "' + b64EncodeUnicode( valor() ) + '", '
                : '"' + obj.name + '" : "' + valor() + '", ';
        else if ( typeReqs === 'formdata' )
            return ( b64 )
                ? data.append( b64EncodeUnicode( obj.name ), b64EncodeUnicode( valor() ) )
                : data.append( obj.name, valor() );
    }

    for ( var i = 0; i < objsSelect.length; i++ )
        if ( objsSelect[i].disabled == false )
            resp += reqsAux( objsSelect[i] );            

    for( var i = 0; i < objsTextArea.length; i++ )
        if (objsTextArea[i].disabled == false)
            resp += reqsAux( objsTextArea[i] ); 

    for ( var i=0; i < objsInput.length; i++ )
        if ( objsInput[i].getAttribute('type') != 'button' && objsInput[i].getAttribute('type') != 'submit' 
        && objsInput[i].getAttribute('type') != 'radio' && objsInput[i].getAttribute('type') != 'checkbox' 
        && objsInput[i].disabled == false )
            resp += reqsAux( objsInput[i] );
        else if ( objsInput[i].getAttribute('type') == 'radio' || objsInput[i].getAttribute('type') == 'checkbox' )
            objsRadioCheck[k++] = objsInput[i];

    if ( objsRadioCheck.length>0 ) {
        var key=0;
        var keyAux = 0;
        var strRadioCheckName = objsRadioCheck[0].name;
        var mObjsRadioCheck = [];
        mObjsRadioCheck[key] = [];
        mObjsRadioCheck[key][keyAux++] = objsRadioCheck[0];
        for ( var i = 1; i < objsRadioCheck.length; i++ )
            if ( strRadioCheckName == objsRadioCheck[i].name ) {
                while (strRadioCheckName == objsRadioCheck[i].name ) {
                    mObjsRadioCheck[key][keyAux++] = objsRadioCheck[i++];
                    if ( i == objsRadioCheck.length )
                        break;
                }
                if ( i == objsRadioCheck.length )
                    break;
                else {
                    strRadioCheckName = objsRadioCheck[i--].name;
                    keyAux = 0;
                    mObjsRadioCheck[++key] = [];
                }
            } else {
                strRadioCheckName = objsRadioCheck[i--].name;
                keyAux = 0;
                mObjsRadioCheck[++key] = [];
            }
            for ( var i = 0; i < mObjsRadioCheck.length; i++ )
                for ( var j = 0; j < mObjsRadioCheck[i].length; j++ )
                    if ( mObjsRadioCheck[i][j].checked )
                        resp += reqsAux( mObjsRadioCheck[i][j], b64 );
    }

    if ( typeReqs === 'txt' )
        return resp.substring( 0, resp.length-1 );
    else if ( typeReqs === 'json' )
        return '{ ' + resp.substring( 0, resp.length-2 ) + ' }';
    else if ( typeReqs === 'formdata' )
        return data;

};
