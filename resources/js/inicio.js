import {Dom} from './modules/Dom.js';
import {Data} from './modules/Data.js';

(() => {

    Dom.ready(() => {
      const obj = document.getElementById( "contenido" );
      inicializarObjs();
      Data.format( obj );
      Constraints.set( obj );
      Data.Validate.set( obj );
    });
    
})();

window.inicializarObjs = () => {
  Data.Init.objs( document.getElementById( "contenido" ) );
  Data.Init.objsSelectMultiple( document.getElementById( "objList" ), document.getElementById( "objSelected" ) );
};   

window.valEnvio = () => {
  if ( Data.Validate.objs( document.getElementById( "contenido" ) ) ) {
    alert( "Exito: Formulario validado correctamente..." );
  }
}
