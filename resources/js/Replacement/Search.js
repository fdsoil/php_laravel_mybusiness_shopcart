Replacement.Detail.Search = (()=> {

  const element = {
    articleId: document.getElementById("article_id"),
    article: document.getElementById("article"),
    idCancelArticle: document.getElementById("id_cancel_article"),
    idTabArticle:   document.getElementById("id_tab_art")
  };
   
  return {

    get() {
      const ele = {
          search: document.getElementById('product_search')
      }
      let arg = ele.search.value;
      if (arg.length > 0) {
        Service.Article.getSearch(arg).then(response => {
          const allowed = ['name','descript', 'int_cod'];          
          let oTbl = document.getElementById("id_tab_art");
          let oTBody = oTbl.getElementsByTagName('tbody');
          let arr = response; 
          let arrLen = arr.length; 
          let oTr, oTd, row, col, oImg; 

          oTBody[0].querySelectorAll("tr").forEach(function(e){e.remove()})
          for (row = 0; row <= arrLen; row++) { 
            oTr = document.createElement("tr"); 
            for (col in arr[row]) {
                if (col=='id')
                    oTr.id=arr[row][col];
                if (col=='photo') {                    
                    oImg = document.createElement("img");
                    oImg.src = `${process.env.MIX_APP_URL}storage/article/${arr[row][col]}`;
                    oImg.className = "imagen";
                    oTd = document.createElement("td");
                    oTd.width = "10%"; 
                    oTd.appendChild(oImg);
                    oTr.appendChild(oTd);
                }
                if (Arr.in(col, allowed)) {
                    oTd = document.createElement("td");
                    oTd.textContent=arr[row][col];
                    oTr.appendChild(oTd);
                }
            }
            //oTr.addEventListener( "click", (row) => Replacement.Detail.Search.selectItem(row));      
            oTr.setAttribute("onclick","Replacement.Detail.Search.selectItem("+row+")");
            oTr.className=((row% 2) == 0)?'losnone':'lospare';   
            oTBody[0].appendChild(oTr); 
          } 
          oTbl.appendChild(oTBody[0]);	   
        });
      }

    },

    selectItem: function (row) {
      let obj = element.idTabArticle.querySelectorAll("tbody tr")[row];
      element.articleId.value = obj.id;
      element.article.value = obj.cells[1].textContent 
                                       + ', ' + obj.cells[2].textContent 
                                       + ', ' + obj.cells[3].textContent;            
      Replacement.Detail.Search.ModWin.hide('article');
      element.idTabArticle.getElementsByTagName("tbody")[0].innerHTML = "";
    },
 
   
    
    ModWin: (() => {
      
      element.article.addEventListener( "click", () => Replacement.Detail.Search.ModWin.show('article') );      
      element.idCancelArticle.addEventListener( "click", () => Replacement.Detail.Search.ModWin.hide('article') );      
      
      return{
        show(strName) {
          (() => {
          //  init();
          //  let uMed = element.medidaUnidad.[element.medidaUnidad.selectedIndex].text;
          //  let oCan = element.labelQuantity;
          //  oCan.textContent = 'Cantidad de ' + uMed;
          })();
          show("div_deshacer_fondo_opaco",'clip',250);
          show("div_deshacer_modal_"+ strName,'clip',500);
        },
        hide(strName) {
          //(() => {
          //  init();
          //})(); 
          hide("div_deshacer_fondo_opaco",'clip',250);
          hide("div_deshacer_modal_" + strName,'clip',500);
        }
      }
    })()
  }
})();

