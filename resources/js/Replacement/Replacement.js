window.Replacement = (() => {

  const element = {
    id: document.getElementById('id'),
    subject: document.getElementById('subject'),
    description: document.getElementById('description'),
    observation: document.getElementById('observation'),
    received: document.getElementById('received')
  };

  return {    
    
    insert() {      
            
      const data = new FormData();
      data.append('id', element.id.value);
      data.append('subject', element.subject.value);
      data.append('description', element.description.value);
      data.append('observation', element.observation.value);
      data.append('received', element.received.value);       
      Service.Replacement.insert(data).then(response => {
        response.status == 201
          ? Notification.success(response.data["msg"])
            : Notification.error(`Error# ${response.status}; Desc: ${response.statusText}`);
      }).catch(error => {        
        Notification.error(error);
      });
      
    },
    
    update(id) {      
            
      const data = new FormData();
      data.append('_method', 'PUT');
      data.append('id', element.id.value);
      data.append('subject', element.subject.value);
      data.append('description', element.description.value);
      data.append('observation', element.observation.value);
      data.append('received', element.received.value);       
      Service.Replacement.update(data, id).then(response => {        
        response.status == 200
          ? Notification.success(response.data["msg"])
            : Notification.error(`Error# ${response.status}; Desc: ${response.statusText}`);
        //} else if (response[0] == 'H') 
	    //  document.getElementById('popup_ok').setAttribute('onClick', 'window.location.assign("../producto/")');          
      }).catch(error => {        
        Notification.error(error);
      });
      
    }

  }

})();
