Replacement.Detail = (() => {

  const element = {
    articlePanel: document.getElementById("id_panel_article"),
    replacementId: document.getElementById("id"),
    detailId: document.getElementById("detail_id"),
    articleId: document.getElementById("article_id"),
    article: document.getElementById("article"),
    amountRequested: document.getElementById("amount_requested"),
    amountReceived: document.getElementById("amount_received")
  };  

  const initObjsPanel = () => { 
    /*initObjs(element.articlePanel);*/
      element.detailId.value = '';
      element.articleId.value = '';
      element.article.value = '';
      element.amountRequested.value = 0;
      element.amountReceived.value = 0; 
  };
  
  const insert = () => {
    const data = new FormData();
    //data.append('detail_id', element.detailId.value);      
    data.append('replacement_id', element.replacementId.value);
    data.append('article_id', element.articleId.value);
    data.append('amount_requested', element.amountRequested.value);
    data.append('amount_received', element.amountReceived.value);       
    Service.Replacement.detailInsert(data).then(response => {
      Replacement.Detail.onOffPanel();        
      Notification.success(response["msg"]);  
      tableFill(response["replacementDetails"]);           
    }).catch(error => {        
      Notification.error(error);
    });
  };
  
  const update = id => {
    const data = new FormData();
    data.append('_method', 'PUT');
    data.append('id', element.detailId.value);      
    data.append('replacement_id', element.replacementId.value);
    data.append('article_id', element.articleId.value);
    data.append('amount_requested', element.amountRequested.value);
    data.append('amount_received', element.amountReceived.value);       
    Service.Replacement.detailUpdate(data, id).then(response => {
      Replacement.Detail.onOffPanel();       
      Notification.success(response["msg"]);
      tableFill(response["replacementDetails"]);     
    }).catch(error => {        
      Notification.error(error);
    });
  };

  const tableFill = (data) => {
    const config = {
      limit: 1,
      id: "id",
      hiddenData: ["article_id"],
      img: {
        name: "photo",
        path: `${process.env.MIX_APP_URL}storage/article/`
      },
      fields: [
        {name: "int_cod", show: true, align: "left"},
        {name: "name", show: true, align: "left"},
        {name: "descript", show: true, align: "left"},
        {name: "amount_requested", show: true, align: "right"},
        {name: "amount_received", show: true, align: "right"}
      ],      
      actions: [{ 
        style  : "cursor:pointer",
        src    : "../../img/edit.svg",
        title  : "Editar datos..." ,
        func   : Replacement.Detail.edit,
        param  : "this" 
      }, {
        style  : "cursor:pointer",
        src    : "../../img/delete.svg",
        title  : "Eliminar datos..." ,
        func   : Replacement.Detail.remove,
        param  : "id"
      }]
    }

    Table.fill('id_tab_article', data, config);

  };

  return {
  
    get(productId) {
      /*Service.Product.article(productId).then(response => {
	    tableFill(response);
      });*/
    },

    onOffPanel() {
      toggle("id_panel_article", "blind",500);
      changeTheObDeniedWritingImg("id_img_article");
      initObjsPanel();
      //element.articlePanel.value=0;
    },    
    
    edit(obj) {    
      if(element.articlePanel.style.display == "none")
        Replacement.Detail.onOffPanel();
      let hiddendata = JSON.parse(obj.parentNode.parentNode.getAttribute("hiddendata"));  
      element.detailId.value = obj.parentNode.parentNode.id;    
      element.articleId.value = JSON.stringify(hiddendata[0]);
      element.article.value = obj.parentNode.parentNode.cells[0].textContent + ', '
                                 + obj.parentNode.parentNode.cells[1].textContent + ', '
                                 + obj.parentNode.parentNode.cells[2].textContent + ', '
                                 + obj.parentNode.parentNode.cells[3].textContent;
      element.amountRequested.value = obj.parentNode.parentNode.cells[4].textContent;
      element.amountReceived.value = obj.parentNode.parentNode.cells[5].textContent;
    },

    remove(val) {

      Notification.confirm(() => {
        let data = new FormData();
        data.append('_method', 'DELETE');
        data.append('id', val);
        Service.Replacement.detailDelete(data, val).then(response => {
          if (element.articlePanel.style.display == "")
            Replacement.Detail.onOffPanel();
          tableFill(response["replacementDetails"]);     
        }).catch(error => {        
          Notification.error(error);
        });
      });

    },
    
    valEnvio() {
      /*if (validateObjs(element.articlePanel))
        regist();*/
      if (true) {
        //console.log(element.detailId.value);
        (element.detailId.value=='') ? insert() : update(element.detailId.value);
      }
        
    }

  }

})();



