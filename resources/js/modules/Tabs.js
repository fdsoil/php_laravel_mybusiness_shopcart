export const Tabs = (()=> {

  const elements = {
    back   : document.getElementById('id_back'),
    forward: document.getElementById('id_forward'),
    save   : document.getElementById('id_save')
  };
  
  let nTab,
      tTab;      

  const buttonAddEventListener = () => {    
    for (let i=0; i<tTab; i++) {
      document.getElementById(`Tab${i}`).addEventListener("click", () => {
        Tabs.tab(i,tTab);
        buttonSaveOnOff(i);
      });
    }    
    elements.back.addEventListener("click", () => {
      backTab();
      backTabButtonSaveOnOff();
    });    
    elements.forward.addEventListener("click", () => {
      forwardTab();
      forwardButtonSaveOnOff();
    });    
  };
 
  const forwardTab = () => {
    for (let i=0; i<tTab-1; i++) {	
      if (document.all(`div${i}`).style.display != "none") {
        if (document.getElementById(`div${i+1}`).getAttribute('label') == "f") {
          do {                                
            i++;
          } while (document.getElementById(`div${i+1}`).getAttribute('label') == "f");
        }
        Tabs.tab(i+1,tTab);
        return;
      }
    }
  };

  const backTab = () => {
    for (let i=1; i<=tTab-1; i++) {
      if (document.all(`div${i}`).style.display != "none") {
        if (document.getElementById(`div${i-1}`).getAttribute('label') == "f") {
          do {
            i--;
          } while (document.getElementById(`div${i-1}`).getAttribute('label') == "f");
        }
        Tabs.tab(i-1,tTab);
        return;	
      }
    }
  };

  const backTabButtonSaveOnOff = () => {
    for (let i=tTab-1; i >= 0; i--)
      if (document.all(`div${i}`).style.display!="none")
        elements.save.style.display = i==0 ? '' : 'none';
  };

  const forwardButtonSaveOnOff = () => {
    for (let i=1; i<=tTab-1; i++)
      if (document.all(`div${i}`).style.display!="none")       
        elements.save.style.display = i==0 ? '' : 'none';
  };

  const buttonSaveOnOff = () => {
      elements.save.style.display = nTab==0 ? '' : 'none';      
  };

  return {
    tab(n, t) {
      nTab = n;
      tTab = t;
      buttonAddEventListener();
      var tabEndLabel = 't';
      var nTabLast_T = 0;
      for (let i=0; i<tTab; i++) {
        document.getElementById(`chk${i}`).style.visibility = 'hidden';
        if (nTab==i) {
          typeof show === 'undefined' ? document.all(`div${i}`).style.display="": show(`div${i}`,250);          
          document.getElementById(`chk${i}`).style.visibility = 'visible';      
          document.getElementById(`Tab${i}`).style.background="#A9F5F2";
        } else {
          typeof hide === 'undefined' ? document.all(`div${i}`).style.display="none": hide(`div${i}`,250);                                        
          document.getElementById(`chk${i}`).style.visibility = 'hidden';
          document.getElementById(`Tab${i}`).style.background="#D8D8D8";
        }
        tabEndLabel = document.getElementById(`div${i}`).getAttribute('label');
        if (document.getElementById(`div${i}`).getAttribute('label')=='t')
          nTabLast_T=i;
      }	
      if (nTab == 0 ) {
        elements.back.className='disableButton';
        elements.forward.className='enableButton';
      } else if ( nTab == tTab-1 || (tabEndLabel == 'f' && nTab == nTabLast_T )) {
        elements.back.className = 'enableButton';
        elements.forward.className = 'disableButton';
      } else {
        elements.back.className = 'enableButton';
        elements.forward.className = 'enableButton';
      }
    }
  
  }
  
})();
