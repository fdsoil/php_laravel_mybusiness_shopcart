export const Table = (() => {

  let eTable;

  const setTable = idTable => {
    eTable = document.getElementById(idTable);
  }

  const deleteAllRows = limit => {
    for(let i = eTable.rows.length; i > limit;i--)
      eTable.deleteRow(i -1);    
  }

  const fill = (object, config) => {

    const buildTdActions = params => {
      const eImg = document.createElement('img');
      const param = (params.param === 'id') ? eTr.id : ((params.param === 'this') ? eImg : null);
      eImg.setAttribute('style', params.style);
      eImg.setAttribute('src', params.src);
      eImg.setAttribute('title', params.title);
      eImg.addEventListener("click", ()=> params.func(param));
      return eImg;
    } 

    const eTr = document.createElement('tr');

    if (typeof config.id !== "undefined")
      eTr.id = object[config.id];

    if (typeof config.hiddenData !== "undefined") {
      let hiddendata = [];
      config.hiddenData.forEach(element => hiddendata.push(object[element]));
      eTr.setAttribute("hiddendata", "["+hiddendata+"]");
    }
    
    if (typeof config.img !== "undefined") {      
      let eImg = document.createElement('img');
      eImg.src = `${config.img.path}${object[config.img.name]}`;
      eImg.className = "imagen";
      let eTd = document.createElement("td");
      eTd.width = "10%"; 
      eTd.appendChild(eImg);
      eTr.appendChild(eTd);
    }

    for (const field of config.fields) {
      if (field.show) {
        const eTd = document.createElement('td');
        eTd.setAttribute("align", field.align);
        const node = document.createTextNode(object[field.name]);
        eTd.appendChild(node);      
        eTr.appendChild(eTd);
      }
    }

    const tdActions = document.createElement('td');
    tdActions.align = 'center';
    
    for (let i = 0; i < config.actions.length; i++) {
      let params = buildTdActions(config.actions[i]);      
      tdActions.appendChild(params);      
    }

    eTr.appendChild(tdActions);

    return eTr; 
  }

  return {

    fill(idTable, objects, config) {
      setTable(idTable);
      deleteAllRows(config.limit);
      if (objects.length !== 0) {        
        objects.forEach(
          object => eTable.tBodies[0].appendChild(fill(object, config))
        );
      }
    }

  };

})();
