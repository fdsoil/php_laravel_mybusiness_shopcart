export const Http = (() => {

  const send = async config => {

    let eWait = WaitElement.show();

    try {

      let response = await axios(config);
      WaitElement.hidden(eWait);
      return response;

    } catch (error) {

      WaitElement.hidden(eWait);
      throw new Error(error);

    }

  }

  return {

    request(config) {
      return send(config);
    }

  }

})();

