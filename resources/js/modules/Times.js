export const Times = (()=> {

  return {
    
    chkHour00To24(val) {
      var arr = val.split(':');
      if ( arr[0] == '00' )
          arr[0] = 24;
      return arr[0] + ':' + arr[1] + ':' + arr[2];
    },
    
    currentTime() {
      var dig = new Date();
      var h = dig.getHours();
      var m = dig.getMinutes(); //var s = dig.getSeconds();
      var ap = "AM";
      if ( h > 12 ) {
          ap = "PM";
          h = h - 12;
      }
      if ( h == 0 )
          h = 12;
      if ( h < 9 )
          h = "0" + h;
      if ( m <= 9 )
          m = "0" + m;
      //if ( s <= 9 ) s = "0" + s;
      return h + ':' + m + ':' + ap;
    },
    
    normalToMilitar(hora) {
      var meridian = hora.substr(6, 1);
      hora = hora.replace('AM', '00');
      hora = hora.replace('PM', '00');
      if ( meridian == 'P' ) {
          var arr = hora.split(':');
          arr[0] = parseInt( arr[0] ) + 12;
          if ( arr[0] == 24 )
              arr[0] = '00';
          hora = pad( arr[0], 2 ) + ':' + pad( arr[1], 2 ) + ':' + arr[2];
      }
      return hora;
    }
  
  }
  
})();


