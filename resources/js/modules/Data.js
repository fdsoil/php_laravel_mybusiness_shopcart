import {Dates} from './Dates.js';
import {Times} from './Times.js';

export const Data = ( () => {

  const addSetAttributeInGroup = (aObj,  attribute, property) => {
    for ( var i = 0; i < aObj.length; i++ )
      aObj[i].setAttribute( attribute, property );
  };

  const addEventListenerInGroup = (aObj, aEvent, oFun) => {
    for ( var i = 0; i < aObj.length; i++ )
      for ( var j = 0; j < aEvent.length; j++ )
        aObj[i].addEventListener( aEvent[j], oFun);
  };

  return {  
  
    format(obj) {
      let aEvents = [ "keyup" ];
      addEventListenerInGroup (
        obj.querySelectorAll("[data-format=uppercase]"), aEvents, function(event) {
            this.value = this.value.toUpperCase();
        }
      );
      addSetAttributeInGroup(obj.querySelectorAll("[data-format=uppercase]"), "style" , "text-transform: uppercase");

      addEventListenerInGroup (
        obj.querySelectorAll("[data-format=lowercase]"), aEvents, function(event){
            this.value = this.value.toLowerCase();
        }
      );
      addSetAttributeInGroup(obj.querySelectorAll("[data-format=lowercase]"), "style" , "text-transform: lowercase");

      aEvents = [ "onkeypress" ];
    
      const fInt = "return Number.formato_numeric(this, '.', ',', event);";
      addSetAttributeInGroup(obj.querySelectorAll("[data-format=integer]"), aEvents, fInt);
      addSetAttributeInGroup(obj.querySelectorAll("[data-format=integer]"), [ "style" ], "text-align:right");

      const fFloat = "return Number.formato_float(this, '.', ',', event);";
      addSetAttributeInGroup(obj.querySelectorAll("[data-format=float]"), aEvents, fFloat);
      addSetAttributeInGroup(obj.querySelectorAll("[data-format=float]"), [ "style" ], "text-align:right");

      const fFloat3d = "return Number.formato_float_3d(this, '.', ',', event);";
      addSetAttributeInGroup(obj.querySelectorAll("[data-format=float_3d]"), aEvents, fFloat3d);
      addSetAttributeInGroup(obj.querySelectorAll("[data-format=float_3d]"), [ "style" ], "text-align:right");
    },
 
    Init: (() => {

      return {
  
        objs(obj) {
          const objsInput = obj.getElementsByTagName( "input" );
          let objsRadioCheck = [];
          let k = 0;
          for ( let i = 0; i < objsInput.length; i++ )
            if ( objsInput[i].getAttribute("type") != "button" 
            && objsInput[i].getAttribute("type") != "submit"
            && objsInput[i].getAttribute("type") != "radio"
            && objsInput[i].getAttribute("type") != "checkbox"
            && objsInput[i].getAttribute( "data-validation" ) != null)
              Data.Init.obj( objsInput[i] );
            else if ( objsInput[i].getAttribute( "type" ) == "radio" || objsInput[i].getAttribute( "type" ) == "checkbox" )
              objsRadioCheck[k++] = objsInput[i];
            if ( objsRadioCheck.length > 0 ) {
              let key = 0;
              let keyAux = 0;
              let strRadioCheckName = objsRadioCheck[0].name;
              let mObjsRadioCheck = [];
              mObjsRadioCheck[key] = [];
              mObjsRadioCheck[key][keyAux++] = objsRadioCheck[0];
              for ( let i = 1; i < objsRadioCheck.length; i++ )
                if ( strRadioCheckName == objsRadioCheck[i].name ) {
                  while ( strRadioCheckName == objsRadioCheck[i].name ) {
                    mObjsRadioCheck[key][keyAux++] = objsRadioCheck[i++];
                    if ( i == objsRadioCheck.length)
                      break;
                  }
                  if ( i == objsRadioCheck.length )
                    break;
                  else {
                    strRadioCheckName = objsRadioCheck[i--].name;
                    keyAux = 0;
                    mObjsRadioCheck[++key] = [];
                  }
                }
              for ( let i = 0; i < mObjsRadioCheck.length; i++ )
                Data.Init.objsRadioCheck( mObjsRadioCheck[i] );
            }
            let objsSelect = obj.getElementsByTagName( "select" );
            for ( let i = 0; i < objsSelect.length; i++ )
              Data.Init.obj( objsSelect[i] );
            let objsTextArea = obj.getElementsByTagName( "textarea" );
            for ( let i = 0; i < objsTextArea.length; i++ )
              Data.Init.obj( objsTextArea[i] );
        },
    
        obj(obj) {
          if ( (obj.getAttribute( "data-validation" )== "required" ) 
          || (obj.getAttribute( "data-validation" )=="length")
          || (obj.getAttribute( "data-validation" )=="email")
          || (obj.getAttribute( "data-validation" )=="url")
          || (obj.getAttribute( "data-validation" )=="textarea") )
            Data.Init.ializeObj( obj, '' );
          else if ( obj.getAttribute( "data-validation" ) == "integer" )
            Data.Init.ializeObj( obj, '0' );
          else if ( obj.getAttribute( "data-validation" ) == "float" )
            Data.Init.ializeObj( obj, '0,00' );
          else if ( obj.getAttribute( "data-validation" ) == "float_3d" )
            Data.Init.ializeObj( obj, '0,000' );
          else if ( ( obj.getAttribute( "data-validation" ) == "select" ) || ( obj.getAttribute( "data-validation" ) == "select-small" ) )
            Data.Init.ializeObj( obj, 'null' );
          else if ( obj.getAttribute( "data-validation" ) == "time" )
            Data.Init.ializeObj( obj, Times.currentTime() );
          else if ( obj.getAttribute( "data-validation" ) == "date" )
            Data.Init.ializeObj( obj, Dates.today( 'YYYYMMDD', '-' ) );
          //*else if (obj.getAttribute( "data-validation" ) == 'table' ) */
        },
    
        objsRadioCheck(arrayObj) {
          if ( arrayObj[0].getAttribute( "data-validation" ) == "radio" 
          || arrayObj[0].getAttribute( "data-validation" ) == "checkbox") {
            for ( var j = 0; j < arrayObj.length; j++ )
              arrayObj[j].checked = false;
            Data.Init.ializeObj(arrayObj[--j]);
          }
        },
    
        objsSelectMultiple(objSelect, objSelected) {
          for ( let i = 0; i < objSelected.length; i++ )
            objSelected[i].selected = true;
          SelectMultiple.move( 'left', objSelect, objSelected );
          Data.Init.ializeObj( objSelected, 'null' );
        },

        ializeObj(obj, value) {
          if ( value != null )
            obj.value = value;
          const objP = obj.parentNode.getElementsByTagName( "span" );
          if ( objP.length == 1 )
            obj.parentNode.removeChild(obj.parentNode.lastChild);
          obj.setAttribute("class","init");
        }
    
      }
  
    })(),
    
    Validate: ( ()=> {   

      const moreThan = (obj) => {
  
        if ( obj.getAttribute( "data-validation" ) == "integer"
        || obj.getAttribute( "data-validation" ) == "float"
        || obj.getAttribute( "data-validation" )=="float_3d" )
            return ( ( obj.value.replace(/\./g, '').replace(',', '.') * 1 ) >= ( obj.getAttribute( "data-validation-min" ) * 1 ) );
        else if ( obj.getAttribute( "data-validation" ) == "date" )
            return ( ( obj.value.replace(/\-/g, '') * 1 ) >= ( obj.getAttribute( "data-validation-min" ).replace(/\-/g, '') * 1 ) );
        else if ( obj.getAttribute( "data-validation" ) == "time")
            return ( ( Times.chkHour00To24( Times.normalToMilitar( obj.value ) ).replace(/\:/g, '') * 1 )
                >= ( Times.chkHour00To24( Times.normalToMilitar( obj.getAttribute( "data-validation-min" ) ) ).replace(/\:/g, '') * 1 ) );
       
      };
  
      const lessThan = (obj) => {

        if ( obj.getAttribute( "data-validation" ) == "integer"
        || obj.getAttribute( "data-validation" ) == "float"
        || obj.getAttribute( "data-validation" ) == "float_3d" )
            return ( ( obj.value.replace(/\./g, '').replace(',', '.') * 1 ) <= ( obj.getAttribute( "data-validation-max" ) * 1 ) );
        else if ( obj.getAttribute( "data-validation" ) == "date" )
            return ( ( obj.value.replace(/\-/g, '') * 1 ) <= ( obj.getAttribute( "data-validation-max" ).replace(/\-/g, '') * 1 ) );
        else if ( obj.getAttribute( "data-validation" ) == "time" )
            return ( ( Times.chkHour00To24( Times.normalToMilitar(obj.value) ).replace(/\:/g, '') * 1 )
                <= ( Times.chkHour00To24( Times.normalToMilitar( obj.getAttribute( "data-validation-max" ) ) ).replace(/\:/g, '') * 1 ) );

      };
  
      const moreAndLessThan = (obj) => {
  
        var arr = obj.getAttribute( "data-validation-min-max" ).split(obj.getAttribute( "data-validation" ) == "date" ? '/' : '-' );
        if ( obj.getAttribute( "data-validation" ) == "integer"
        || obj.getAttribute( "data-validation" ) == "float"
        || obj.getAttribute( "data-validation" ) == "float_3d" )
            return ( ( obj.value.replace(/\./g, '').replace(',', '.') * 1 )
            >= ( arr[0] * 1 )
            && ( obj.value.replace(/\./g, '').replace(',', '.') * 1 )
            <= ( arr[1] * 1 ) );
        else if ( obj.getAttribute( "data-validation" ) == "date" )
            return ( ( obj.value.replace(/\-/g, '') * 1 )
            >= ( arr[0].replace(/\-/g, '') * 1 )
            && ( obj.value.replace(/\-/g, '') * 1 )
            <= ( arr[1].replace(/\-/g, '') * 1 ) );
        else if ( obj.getAttribute( "data-validation" ) == "time" )
            return ( ( Times.chkHour00To24( Times.normalToMilitar(obj.value) ).replace(/\:/g, '') * 1 )
            >= ( Times.chkHour00To24( Times.normalToMilitar( arr[0] ) ).replace(/\:/g, '') * 1 )
            && ( Times.chkHour00To24( Times.normalToMilitar( obj.value) ).replace(/\:/g, '') * 1 )
            <= ( Times.chkHour00To24( Times.normalToMilitar( arr[1] ) ).replace(/\:/g, '') * 1 ) );
        
      };

      const length = (obj) => {

        if ( obj.getAttribute( "data-validation-length" ) != null )
          ( obj.value.length != obj.getAttribute( "data-validation-length" ) )
          ? Data.Validate.displayErrorMsg( obj, ' Deben ser ' + obj.getAttribute( "data-validation-length" ) + ' caracteres ')
          : Data.Validate.removeErrorMsg(obj);
        else if ( obj.getAttribute( "data-validation-min" ) != null )
          ( obj.value == '' || obj.value.length < obj.getAttribute( "data-validation-min" ) )
          ? Data.Validate.displayErrorMsg( obj, ' Debe ser mayor o igual a ' + obj.getAttribute( "data-validation-min" ) + ' caracteres ')
          : Data.Validate.removeErrorMsg(obj);
        else if ( obj.getAttribute( "data-validation-max" ) != null )
          ( obj.value == '' || obj.value.length > obj.getAttribute( "data-validation-max" ) )
          ? Data.Validate.displayErrorMsg( obj, ' Debe ser menor o igual a ' + obj.getAttribute( "data-validation-max" ) + ' caracteres ')
          : Data.Validate.removeErrorMsg(obj);
        else if ( obj.getAttribute( "data-validation-min-max" ) != null ) {
          var arr = obj.getAttribute( "data-validation-min-max" ).split('-');
          ( obj.value == '' || ( obj.value.length < arr[0] || obj.value.length > arr[1] ) )
          ? Data.Validate.displayErrorMsg( obj, ' Debe ser entre '
            + obj.getAttribute( "data-validation-min-max" ).replace('-', ' y ') + ' caracteres ')
          : Data.Validate.removeErrorMsg(obj);
        }
  
      };
  
      const integer = (obj) => {

        const validate = (obj) => {
          var x = obj.value.replace(/\./g, '');
          var y = parseInt(x);
          if ( isNaN(y) )
            return false;
          return x == y && x.toString() == y.toString();
        }
  
        if ( obj.getAttribute( "data-validation-min" ) != null )
          ( validate(obj) == false )
            ? Data.Validate.displayErrorMsg( obj, ' Indique un valor numérico' )
            : !( moreThan(obj) )
              ? Data.Validate.displayErrorMsg( obj, ' Indique número mayor o igual a ' + obj.getAttribute( "data-validation-min" ) )
              : Data.Validate.removeErrorMsg(obj);
        else if ( obj.getAttribute( "data-validation-max" ) != null )
          ( validate(obj) == false )
            ? Data.Validate.displayErrorMsg( obj, ' Indique un valor numérico' )
            : !( lessThan(obj) )
              ? Data.Validate.displayErrorMsg( obj, ' Indique número menor o igual a ' + obj.getAttribute( "data-validation-max" ) )
              : Data.Validate.removeErrorMsg(obj);
        else if ( obj.getAttribute( "data-validation-min-max" ) != null )
          ( validate(obj) == false)
            ? Data.Validate.displayErrorMsg( obj, ' Indique un valor numérico' )
            : !( moreAndLessThan(obj) )
              ?Data.Validate.displayErrorMsg( obj,' Indique número entre '+obj.getAttribute( "data-validation-min-max" ).replace('-', ' y '))
              : Data.Validate.removeErrorMsg(obj);
        else
          (validate(obj)==false)?Data.Validate.displayErrorMsg(obj, ' Indique valor numérico' ):Data.Validate.removeErrorMsg(obj);
  
      };
  
      const float = (obj) => {
  
        const validate = (obj) => {
          return ( isNaN( parseFloat( obj.value.replace(/\./g, '').replace(',', '.') ) ) ) ? false : true;
        }
  
        if ( obj.getAttribute( "data-validation-min" ) != null )
          ( validate(obj) == false )
            ? Data.Validate.displayErrorMsg( obj, ' Indique un valor numérico' )
            : !( moreThan(obj) )
              ? Data.Validate.displayErrorMsg( obj, ' Indique número mayor o igual a ' + obj.getAttribute( "data-validation-min" ) )
              : Data.Validate.removeErrorMsg(obj);
        else if ( obj.getAttribute( "data-validation-max" ) != null )
          ( validate(obj) == false )
            ? Data.Validate.displayErrorMsg( obj, ' Indique un valor numérico' )
            : !( lessThan(obj) )
              ? Data.Validate.displayErrorMsg( obj, ' Indique número menor o igual a ' + obj.getAttribute( "data-validation-max" ) )
              : Data.Validate.removeErrorMsg(obj);
        else if ( obj.getAttribute( "data-validation-min-max" ) != null )
          ( validate(obj) == false )
            ? Data.Validate.displayErrorMsg( obj, ' Indique un valor numérico' )
            : !( moreAndLessThan(obj) )
              ? Data.Validate.displayErrorMsg(obj,' Indique número entre '+obj.getAttribute( "data-validation-min-max" ).replace('-', ' y '))
              : Data.Validate.removeErrorMsg(obj);
        else
          ( validate(obj) == false )
            ? Data.Validate.displayErrorMsg( obj, ' Indique valor numérico' )
            : Data.Validate.removeErrorMsg(obj);  

      };

      const date = (obj) => {

        if ( obj.getAttribute( "data-validation-min" ) != null )
          ( valDate(obj) == false )
            ? Data.Validate.displayErrorMsg( obj, ' Formato de Fecha Invalida' )
            : !( moreThan(obj) )
              ? Data.Validate.displayErrorMsg( obj, ' Indique fecha mayor o igual a ' + obj.getAttribute( "data-validation-min" ) )
              : Data.Validate.removeErrorMsg(obj);
        else if ( obj.getAttribute( "data-validation-max" ) != null )
          ( valDate(obj) == false )
            ? Data.Validate.displayErrorMsg( obj, ' Formato de Fecha Invalida' )
            : ! ( lessThan(obj) )
              ? Data.Validate.displayErrorMsg( obj, ' Indique fecha menor o igual a ' + obj.getAttribute( "data-validation-max" ) )
              : Data.Validate.removeErrorMsg(obj);
        else if ( obj.getAttribute( "data-validation-min-max" ) != null )
          ( valDate(obj) == false )
            ? Data.Validate.displayErrorMsg( obj, ' Formato de Fecha Invalida' )
            : !( moreAndLessThan(obj) )
              ? Data.Validate.displayErrorMsg( obj, ' Indique fecha entre ' + obj.getAttribute( "data-validation-min-max" ).replace('/', ' y '))
              : Data.Validate.removeErrorMsg(obj);
        else
          ( valDate(obj) ==false )
            ? Data.Validate.displayErrorMsg(obj, ' Formato de Fecha Invalida' )
            : Data.Validate.removeErrorMsg(obj);

      };
  
      const time = (obj) => {

        if ( obj.getAttribute( "data-validation-min" ) != null )
          ( valTime(obj) == false )
            ? Data.Validate.displayErrorMsg( obj, ' Formato de Hora Invalido' )
            : !( moreThan(obj) )
              ? Data.Validate.displayErrorMsg( obj, ' Indique Hora mayor o igual a ' + obj.getAttribute( "data-validation-min" ) )
              : Data.Validate.removeErrorMsg(obj);
        else if ( obj.getAttribute( "data-validation-max" ) != null )
          ( valTime(obj) == false )
            ? Data.Validate.displayErrorMsg( obj, ' Formato de Hora Invalido' )
            : !( lessThan(obj) )
              ? Data.Validate.displayErrorMsg( obj, ' Indique Hora menor o igual a ' + obj.getAttribute( "data-validation-max" ) )
              : Data.Validate.removeErrorMsg(obj);
        else if ( obj.getAttribute( "data-validation-min-max" ) != null )
          ( valTime(obj) == false )
            ? Data.Validate.displayErrorMsg( obj, ' Formato de Hora Invalido' )
            : !( moreAndLessThan(obj) )
              ? Data.Validate.displayErrorMsg( obj, ' Indique Hora entre ' + obj.getAttribute( "data-validation-min-max" ).replace('/', ' y '))
              : Data.Validate.removeErrorMsg(obj);
        else
          ( valTime(obj) == false )
            ? Data.Validate.displayErrorMsg( obj, ' Formato de Hora Invalido' )
            : Data.Validate.removeErrorMsg(obj);

      };
  
      const valEmail = (strValue) => {
        var emailFilter=/^[^@\s]+@[^@\.\s]+(\.[^@\.\s]+)+$/;
        return emailFilter.test(strValue);
      };

      const valURL = (strValue) => {
        var urlFilter = /^(https?|ftp):\/\/((((\w|-|\.|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])(\w|-|\.|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])(\w|-|\.|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/(((\w|-|\.|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/((\w|-|\.|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|\[|\]|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#(((\w|-|\.|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i; 
        return urlFilter.test(strValue);
      };
  
      const valDate = (obj) => {
        var dateFilter1 = /^([0-9]{4})-([0-9]{2})-([0-9]{2})$/;
        var dateFilter2 = /^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;
        return ( dateFilter1.test(obj.value) || dateFilter2.test(obj.value) );
      };

      const valTime = (str) => {
        var hora = str.value
        if (hora.length != 8)
          return false;
        var a = hora.charAt(0) //<=2
        var b = hora.charAt(1) //<4
        var c = hora.charAt(2) //:
        var d = hora.charAt(3) //<=5
        var e = hora.charAt(5) //:
        var f = hora.charAt(6) //<=5
        if ((a == 2 && b > 3) || (a > 2))//El valor que introdujo en la Hora no corresponde, introduzca un digito entre 00 y 23
          return false;
        if (d > 5) //El valor que introdujo en los minutos no corresponde, introduzca un digito entre 00 y 59
          return false;
        if (f > 5) //El valor que introdujo en los segundos no corresponde
          return false;
        if (c != ':' || e != ':') //Introduzca el caracter ':' para separar la hora, los minutos y los segundos
          return false;
        return true;
      };

      const valTable = (obj, minRow) => {
        return (obj.tBodies[0].rows.length < parseInt(minRow))?false:true;
      };  

      return {

        set(obj) {
          const aEventsInput = [ "keyup", "blur", "focus" ];
          const aEventsSelect = [ "change", "blur"];
          const aEventsSelectMultiple = [ "dblclick", "blur", "focus" ];
          const aEventsRadio = [ "click" ];
          const oFun = function() { Data.Validate.obj(this); };
          const oFunRadio = function() { Data.Validate.objRadioCheck(this.name); };
          addEventListenerInGroup(obj.querySelectorAll("input[data-validation=required]"), aEventsInput, oFun);
          addEventListenerInGroup(obj.querySelectorAll("input[data-validation=length]"), aEventsInput, oFun);
          addEventListenerInGroup(obj.querySelectorAll("input[data-validation=email]"), aEventsInput, oFun);
          addEventListenerInGroup(obj.querySelectorAll("input[data-validation=float]"), aEventsInput, oFun);
          addEventListenerInGroup(obj.querySelectorAll("input[data-validation=float_3d]"), aEventsInput, oFun);
          addEventListenerInGroup(obj.querySelectorAll("input[data-validation=integer]"), aEventsInput, oFun);
          addEventListenerInGroup(obj.querySelectorAll("input[data-validation=date]"), aEventsInput, oFun);
          addEventListenerInGroup(obj.querySelectorAll("input[data-validation=time]"), aEventsInput, oFun);
          addEventListenerInGroup(obj.querySelectorAll("input[data-validation=radio]"), aEventsRadio, oFunRadio);
          addEventListenerInGroup(obj.querySelectorAll("textarea[data-validation=textarea]"), aEventsInput, oFun);
          addEventListenerInGroup(obj.querySelectorAll("select[data-validation=select]"), aEventsSelect, oFun);
          addEventListenerInGroup(obj.querySelectorAll("select[data-validation=select-small]"), aEventsSelect, oFun);
          addEventListenerInGroup(obj.querySelectorAll("select[data-validation=select-multiple]"), aEventsSelectMultiple, oFun);
        },
    
        objs(obj) {
    
          var objsSelect = obj.getElementsByTagName( "select" );
          for ( var i = 0; i < objsSelect.length; i++ )
            Data.Validate.obj( objsSelect[i] );
          var objsTextArea = obj.getElementsByTagName( "textarea" );
          for ( var i = 0; i < objsTextArea.length; i++ )
            Data.Validate.obj( objsTextArea[i], true );
          var objsInput = obj.getElementsByTagName( "input" );
          var objsRadioCheck = [];
          var k = 0;
          for ( var i = 0; i < objsInput.length;i++)
            if ( objsInput[i].getAttribute( "type" ) != "button"
            && objsInput[i].getAttribute( "type" ) != "submit"
            && objsInput[i].getAttribute( "type" ) != "radio"
            && objsInput[i].getAttribute( "type" ) != "checkbox"
            && objsInput[i].getAttribute( "data-validation" ) != null )
              Data.Validate.obj( objsInput[i], true );
            else if ( objsInput[i].getAttribute( "type" ) == "radio" )
              objsRadioCheck[k++] = objsInput[i];
          if ( objsRadioCheck.length > 0 ) {
            var key = 0;
            var keyAux = 0;
            var strRadioCheckName = objsRadioCheck[0].name;
            var mObjsRadioCheck = [];
            mObjsRadioCheck[key] = [];
            mObjsRadioCheck[key][keyAux++] = objsRadioCheck[0];
            for ( var i = 1; i < objsRadioCheck.length; i++ )
              if ( strRadioCheckName == objsRadioCheck[i].name ) {
                while ( strRadioCheckName == objsRadioCheck[i].name ) {
                  mObjsRadioCheck[key][keyAux++] = objsRadioCheck[i++];
                  if ( i == objsRadioCheck.length )
                    break;
                }
                if ( i == objsRadioCheck.length )
                  break;
                else {
                  strRadioCheckName = objsRadioCheck[i--].name;
                  keyAux = 0;
                  mObjsRadioCheck[++key] = [];
                }
              }
            for ( var i = 0; i < mObjsRadioCheck.length; i++ )
              Data.Validate.objsRadioCheck( mObjsRadioCheck[i] );
          }
          return ( obj.getElementsByClassName( "error" ).length == 0 );
      
        },

        obj(obj , inGroup = false) {
    
          obj.value = inGroup
            ? obj.value.replace(/([\ \t]+(?=[\ \t])|^\s+|\s+$)/g, '')
            : obj.value.replace(/([\ \t]+(?=[\ \t])|^\s+)/g, '');
          if ( obj.getAttribute( "data-validation" ) == "required" )
            ( obj.value == '' ) ? Data.Validate.displayErrorMsg( obj, ' Campo Requerido' ) : Data.Validate.removeErrorMsg(obj);
          else if ( obj.getAttribute( "data-validation" ) == "length" )      
            length(obj);        
          else if ( obj.getAttribute( "data-validation" ) == "integer" )
            integer(obj);
          else if ( obj.getAttribute( "data-validation" ) == "float" || obj.getAttribute( "data-validation" ) == "float_3d" )
            float(obj);
          else if ( obj.getAttribute( "data-validation" ) == "date" )
            date(obj);
          else if ( obj.getAttribute( "data-validation" ) == "time" )
            time(obj);
          else if ( obj.getAttribute( "data-validation" ) == "email" )
            ( valEmail(obj.value) == false ) 
              ? Data.Validate.displayErrorMsg(obj, 'Formato de Correo Invalido' )
              : Data.Validate.removeErrorMsg(obj);
          else if ( obj.getAttribute( "data-validation" ) == "url" )
            ( valURL(obj.value) == false )
              ? Data.Validate.displayErrorMsg( obj, 'Formato de URL Invalido' )
              : Data.Validate.removeErrorMsg(obj);
          else if ( obj.getAttribute( "data-validation" ) == "select" )
            ( obj.selectedIndex == 0 )
              ? Data.Validate.displayErrorMsg( obj, 'Seleccione un Item' )
              : Data.Validate.removeErrorMsg(obj);
          else if ( obj.getAttribute( "data-validation" ) == "select-small" )
            ( obj.selectedIndex==0 )
              ? Data.Validate.displayErrorMsg( obj, 'Selec...' )
              : Data.Validate.removeErrorMsg(obj);
          else if ( obj.getAttribute( "data-validation" ) == "select-multiple" )
            ( obj.length==0 )
              ? Data.Validate.displayErrorMsg( obj, 'Seleccionar Item(s)' )
              : Data.Validate.removeErrorMsg(obj);
          else if ( obj.getAttribute( "data-validation" ) == "textarea" )
            ( obj.value == '')
              ? Data.Validate.displayErrorMsg(obj, 'Texto Requerido' )
              : Data.Validate.removeErrorMsg(obj);
      
        },
    
        objRadioCheck(strName) {
          var objElementsByName = document.getElementsByName(strName);
          Data.Validate.objsRadioCheck(objElementsByName);
        },  

        objTable(obj) {

          if ( obj.getAttribute( "data-validation" ) == "table" ) {
            if ( valTable( obj, obj.getAttribute( "data-validation-min" ) ) == false ) {
              Data.Validate.displayErrorMsgTable( obj, 'Debe agregar mínimo ' + obj.getAttribute( "data-validation-min" ) + ' Item(s)' );
              return false;
            } else {
              Data.Validate.removeErrorMsgTable(obj);
              return true;
            }
          }
      
        },

        objsRadioCheck(arrayObj) {
          var nSelected = 0;
          if ( arrayObj[0].getAttribute( "data-validation" ) == "radio" 
	    || arrayObj[0].getAttribute( "data-validation" ) == "checkbox" ) {
            var dataValidation = arrayObj[0].getAttribute( "data-validation" );
            var strUn = ( arrayObj[0].getAttribute( "data-validation" ) == "checkbox" ) ? 'algún(os)' : 'un';
            var strS = ( arrayObj[0].getAttribute( "data-validation" ) == "checkbox") ? '(s)' : '';
          } else
            return null;
          for ( var j = 0; j < arrayObj.length; j++ )
            if ( arrayObj[j].getAttribute( "data-validation" ) != dataValidation )
              return null;
            else if ( arrayObj[j].checked )
              nSelected = 1;
            ( nSelected == 0 )
              ? Data.Validate.displayErrorMsg( arrayObj[--j], 'Seleccione ' + strUn + ' Item' + strS )
              : Data.Validate.removeErrorMsg( arrayObj[--j] );
        },

        displayErrorMsg(obj, msg) {
          if ( obj.parentNode.getElementsByTagName( "span" ).length == 0 ) {
            obj.setAttribute("class", "error" );
            var objDisplayErrorMsg = document.createElement( "span" );
            objDisplayErrorMsg.innerHTML = msg;
            objDisplayErrorMsg.setAttribute("class","help-block form-error");
            obj.parentNode.appendChild(objDisplayErrorMsg);
          } else {
            var oSpan = obj.parentNode.getElementsByTagName( "span" );
            oSpan[0].innerHTML = msg;
          }
        },

        removeErrorMsg(obj) {
          var objP = obj.parentNode.getElementsByTagName( "span" );
          if ( objP.length == 1 )
            obj.parentNode.removeChild(obj.parentNode.lastChild);
          obj.setAttribute("class","valid");
        },

        displayErrorMsgTable(obj, msg) {
          if ( obj.parentNode.getElementsByTagName( "span" ).length == 0 ) {
            var objDisplayErrorMsg = document.createElement("span");
            objDisplayErrorMsg.innerHTML = msg;
            objDisplayErrorMsg.setAttribute("class","help-block form-error");
            obj.parentNode.appendChild(objDisplayErrorMsg);
          }
        },

        removeErrorMsgTable(obj) {
          var objP = obj.parentNode.getElementsByTagName( "span" );
          if ( objP.length == 1 )
            obj.parentNode.removeChild(obj.parentNode.lastChild);
        }    

      }
  
    })()
  
  }
  
})();
