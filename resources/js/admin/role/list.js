// register the grid component
Vue.component('grid', {
    template: '#grid-template',
    props: {
        data: Array,
        columns: Array,
        filterKey: String
    },
    data: function () {
        var sortOrders = {}
        this.columns.forEach(function (key) {
            sortOrders[key] = 1
        })
        return {
          sortKey: '',
          sortOrders: sortOrders
        }
    },
    computed: {
        filteredData: function () {
            var sortKey = this.sortKey
            var filterKey = this.filterKey && this.filterKey.toLowerCase()
            var order = this.sortOrders[sortKey] || 1
            var data = this.data
            if (filterKey) {
                data = data.filter(function (row) {
                    return Object.keys(row).some(function (key) {
                        return String(row[key]).toLowerCase().indexOf(filterKey) > -1
                    })
                })
            }
            if (sortKey) {
                data = data.slice().sort(function (a, b) {
                    a = a[sortKey]
                    b = b[sortKey]
                    return (a === b ? 0 : a > b ? 1 : -1) * order
                })
            }
            return data
        }
    },
    filters: {
        capitalize: function (str) {
            return str.charAt(0).toUpperCase() + str.slice(1)
        }
    },
    methods: {
        sortBy: function (key) {
            this.sortKey = key
            this.sortOrders[key] = this.sortOrders[key] * -1
        },
        editMenu: function (role) {
            //alert(menu.description);
            updateModel.roleOption.id = role.id;
            updateModel.roleOption.description = role.description;
            $('#edit').modal('show');
            //document.getElementById("edit").style="display:;";
        },
        deleteMenu: function (id) {
            let url = `${process.env.MIX_APP_URL}menu/${id}`;
            axios.delete(url).then(response => {
               gridViewModel.getMenus();
               alert('Eliminado correctamente');
               //toastr.success('Eliminado correctamente');
            });
        },
    }
})

window.gridViewModel = new Vue({
    el: '#demo',
    data: {
        searchQuery: '',
        gridColumns: ['id', 'description', 'pag_ini_default', 'status_id'],
        gridData: [],
        startRow: 0,
        rowsPerPage: 10,
    },
    created: function() {
        this.getMenus();
    },
    methods: {
        movePages: function (amount) {
            var newStartRow = this.startRow + amount * this.rowsPerPage;
            if (newStartRow >= 0 && newStartRow < this.gridData.length) {
                this.startRow = newStartRow;
            }
        },
        getMenus: function (id, step = true ) {
            /*let urlUsers = `https://mybusinessshopcart.gob.ve/role/get`;            
            axios.get(urlUsers).then(response => {                
                this.gridData = response.data;
            });*/
	    this.gridData = gridData;
        },
        createMenu: function () {
            //alert(menu.description);
            updateModel.roleOption.id = '';
            updateModel.roleOption.description = '';
            $('#create').modal('show');
            //document.getElementById("edit").style="display:;";
        }
    },
    filters: {
        orderByBusinessRules: function (data) {
            return data.slice().sort(function (a, b) {
                return a.power - b.power;
            });
        }
    },
    computed: {
        limitGridData: function () {
            return this.gridData.slice(this.startRow, this.startRow + this.rowsPerPage);
        }
    }
});










