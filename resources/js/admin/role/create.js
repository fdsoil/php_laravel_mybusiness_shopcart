window.Roles = (function() {
    let children = [], jsons = [], trs = [], tds = [];
    let tBody = document.createElement("tbody");
    tds[0] = [], tds[1] = [];
    const Do = (function() {
        let nodeTitle = function (tNodo, num) {
            return document.createTextNode("\u00A0\u00A0\u00A0\u00A0".repeat(num) + tNodo);            
        }
        let elementInputCheckBox = function (id) {
            const oInputCheckBox = document.createElement("input");
            oInputCheckBox.type = "checkbox";
            oInputCheckBox.id = id;
            return oInputCheckBox;
        }
        let nodeEmpty = function () {
            return document.createTextNode("");
        }
        return {
            tdTitle: function (tNodo, num) {
                const oTd = document.createElement("td");
                oTd.appendChild( nodeTitle(tNodo, num) );
                return oTd;
            },
            tdInputCheckBox: function (id) {
                const oTd = document.createElement("td");
                oTd.appendChild( elementInputCheckBox(id) );
                return oTd;
            },
            tdEmpty: function () {
                const oTd = document.createElement("td");
                oTd.appendChild( nodeEmpty() );
                return oTd;
            }
        }
    })();
    const childrenGet = function (jsons, paren) {
        return jsons.filter(json => json.padre_id == paren);
    }
    const process = function (num) {
         for (let i in children[num]) {
             trs[num]  = document.createElement("tr");
             tds[0][num] = Do.tdTitle(children[num][i].title, num);
             children[num+1] = childrenGet(jsons, children[num][i].id);
             tds[1][num] = (children[num+1].length)
                 ? Do.tdEmpty() 
                 : Do.tdInputCheckBox(children[num][i].id);
             trs[num].appendChild(tds[0][num]);
             trs[num].appendChild(tds[1][num]);
             tBody.appendChild(trs[num]);
             if (children[num+1])
                 process(num+1);
         }
    }
    return {
        make: function (arr) {
            jsons = arr;
            children[0] = childrenGet(jsons, 0);
            if (children[0])
                process(0);
            return tBody;
        }
    }
})();

(function () {
  let urlUsers = `${process.env.MIX_APP_URL}menu/recursive`;            
      axios.get(urlUsers).then(response => {                
	  document.getElementById("rolesTable").appendChild( Roles.make(response.data) );
  });
})();


