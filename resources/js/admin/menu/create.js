new Vue({
    el: "#main",
    created: function() {
        this.stepFrontward(0);
    },                
    data: {
        menus: [],
        selVal : 0,
        selTexs : [],
        nivel: 0,
        newMenu:'',
        newParent:0,
        newPath: "",
        newSort: ""
    },
    methods: {
        stepFrontward: function (id, step = true ) {
            let urlUsers = `${process.env.MIX_APP_URL}menu/children/${id}`;
            let showSelected = function () {
                  let menu = (this.menus.find(element => element.id === this.selVal));
                  this.selTexs.push({ 
                      nivel: this.nivel,
                      title: menu.title,
                      id   : menu.parent_id 
                  });
            }.bind(this);
            axios.get(urlUsers).then(response => {
                if (step) {
                    if (this.selVal) {                    
                        showSelected();
                        this.nivel++;
                        this.newParent = this.selVal;
                    }
                } else {
                    this.nivel--;
                    this.newParent = response.data[0].parent_id;
                }
                this.menus = [ {id:0 , title:'Seleccione...'} ].concat(response.data);
                this.selVal = 0;

            });
        },
        stepBackward: function (id) {
            this.selTexs.pop();
            this.stepFrontward(id, false);
        },
        fastBackward: function (id) {
            this.nivel = 0;
            this.selTexs = [];
            this.stepFrontward(0);
        },
        createMenu: function () {
            let url = `${process.env.MIX_APP_URL}menu/store`;
            axios.post(url, {
                parent_id: this.newParent,
                title  : this.newMenu,
                path: this.newPath,
                sort: this.newSort
            }).then(response => {
               gridViewModel.getMenus();
               this.newMenu = '';
               this.errors = [];
               $('#create').modal('hide');
               Notification.success('Nueva tarea creada con éxito');
            }).catch(error => {
               this.errors = error.response.data;
            });

        }
    }    
});




