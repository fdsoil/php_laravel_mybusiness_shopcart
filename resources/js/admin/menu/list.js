// register the grid component
Vue.component('grid', {
    template: '#grid-template',
    props: {
        data: Array,
        columns: Array,
        filterKey: String
    },
    data: function () {
        var sortOrders = {}
        this.columns.forEach(function (key) {
            sortOrders[key] = 1
        })
        return {
          sortKey: '',
          sortOrders: sortOrders
        }
    },
    computed: {
        filteredData: function () {
            var sortKey = this.sortKey
            var filterKey = this.filterKey && this.filterKey.toLowerCase()
            var order = this.sortOrders[sortKey] || 1
            var data = this.data
            if (filterKey) {
                data = data.filter(function (row) {
                    return Object.keys(row).some(function (key) {
                        return String(row[key]).toLowerCase().indexOf(filterKey) > -1
                    })
                })
            }
            if (sortKey) {
                data = data.slice().sort(function (a, b) {
                    a = a[sortKey]
                    b = b[sortKey]
                    return (a === b ? 0 : a > b ? 1 : -1) * order
                })
            }
            return data
        }
    },
    filters: {
        capitalize: function (str) {
            return str.charAt(0).toUpperCase() + str.slice(1)
        }
    },
    methods: {
        sortBy: function (key) {
            this.sortKey = key
            this.sortOrders[key] = this.sortOrders[key] * -1
        },
        editMenu: function (menu) {
            updateModel.menuOption.id = menu.id;
            updateModel.menuOption.familia = menu.familia;
            updateModel.menuOption.path = menu.path;
            updateModel.menuOption.sort = menu.sort;
            updateModel.menuOption.parent_id = menu.padre_id;
            $('#edit').modal('show');
        },
        deleteMenu: function (id) {
        
          Notification.confirm(() => {
            let url = `${process.env.MIX_APP_URL}menu/${id}`;
            axios.delete(url).then(response => {
               gridViewModel.getMenus();
               Notification.success('Eliminado correctamente');               
            });
          });            
            
        },
    }
})

window.gridViewModel = new Vue({
    el: '#demo',
    data: {
        searchQuery: '',
        gridColumns: ['id', 'familia', 'path', 'sort'],
        gridData: [],
        startRow: 0,
        rowsPerPage: 5,
    },
    created: function() {
        this.getMenus();
    },
    methods: {
        movePages: function (amount) {
            var newStartRow = this.startRow + amount * this.rowsPerPage;
            if (newStartRow >= 0 && newStartRow < this.gridData.length) {
                this.startRow = newStartRow;
            }
        },
        getMenus: function (id, step = true ) {
            let urlUsers = `${process.env.MIX_APP_URL}menu/recursive`;            
            axios.get(urlUsers).then(response => {                
                this.gridData = response.data;
            });
        }
    },
    filters: {
        orderByBusinessRules: function (data) {
            return data.slice().sort(function (a, b) {
                return a.power - b.power;
            });
        }
    },
    computed: {
        limitGridData: function () {
            return this.gridData.slice(this.startRow, this.startRow + this.rowsPerPage);
        }
    }
});










