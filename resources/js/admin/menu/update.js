window.updateModel = new Vue({
    el: "#edit", 
    computed: {
        parents: function () {
            let arr = this.menuOption.familia.split(' / ');
            this.nivel = arr.length -1;
            this.title = arr.pop();
            this.menuOption.title = this.title;
            return arr;
        }
    },            
    data: {
        menuOption: {'id': '', 'familia': '', 'path': '', 'sort': '', 'title': ''},
        title: '',
        nivel: 0,
        id: null,
    },
    methods: {
        updateMenu: function () {
            let url = `${process.env.MIX_APP_URL}menu`;
            axios.put(url, this.menuOption).then(response => {
               gridViewModel.getMenus();
               this.menuOption= {'id': '', 'familia': '', 'path': '', 'sort': '', 'title': ''};
               this.errors = [];
               $('#edit').modal('hide');
               Notification.success('Opción actualizada con éxito');               
            }).catch(error => {
               this.errors = error.response.data;
            });
        }
    }    
});



