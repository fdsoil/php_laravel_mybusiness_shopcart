export const Article = (() => {

  return {
  
    async get() {      
      let response = await Http.request({
        method: 'get',
        url: `${process.env.MIX_APP_URL}article/get`
      });      
      return response.data;
    },
    
    async getSearch(value) {      
      let response = await Http.request({
        method: 'get',
        url: `${process.env.MIX_APP_URL}article/search/${value}`
      });      
      return response.data;
    },
    
    async getStock(ids) {      
      let response = await Http.request({
        method: 'get',
        url: `${process.env.MIX_APP_URL}article/presentation/${ids}`
      });      
      return response.data;
    },
    
    async insert(data) {
      let response = await Http.request({
          method: 'post',
          url: `${process.env.MIX_APP_URL}article`,
          data: data
      });      
      return response;
      //return response.data;
    },
  
    async update(data, id) {
      let response = await Http.request({
          method: 'post',
          url: `${process.env.MIX_APP_URL}article/${id}`,
          data: data
      });      
      return response;
    },
    async delete(data, id) {
      let response = await Http.request({
        method: 'post',
        url: `${process.env.MIX_APP_URL}article/${id}`,
        data: data
      });      
      return response.data;
    },
    async restore(data, id) {
      let response = await Http.request({
        method: 'post',
        url: `${process.env.MIX_APP_URL}article/restore/${id}`,
        data: data
      });      
      return response.data;
    },
    async upload(data) {
      let response = await Http.request({
          headers: { 'content-type':'multipart/form-data' },
          method: 'post',
          url: `${process.env.MIX_APP_URL}article/photo`,
          data: data
      });      
      return response;
    },    

    Detail: (() => {

      return {

        async get(articleId) {      
          let response = await Http.request({
            method: 'get',
            url: `${process.env.MIX_APP_URL}article-detail/${articleId}`
          });      
          return response.data;
        },

        async insert(data) {
          let response = await Http.request({
            method: 'post',
            url: `${process.env.MIX_APP_URL}article-detail`,
            data: data
          });      
          return response.data;
        },

        async delete(data, id) {
          let response = await Http.request({
            method: 'post',
            url: `${process.env.MIX_APP_URL}article-detail/${id}`,
            data: data
          });      
          return response.data;
        }

      }
    })()

  }

})();
