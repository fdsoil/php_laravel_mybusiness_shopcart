export const Client = (function () {

  return {
  
    async data(email) {      
      let response = await Http.request({
          method: 'get',
          url: `${process.env.MIX_API_URL_CLIENT}email/${email}`
      });      
      return response.data;
    },

    async fullData(email) {
      let response1 = await Service.Client.data(email);
      let response3;
      if (response1) {
        let ids = [
          response1.state,
          response1.municipality,
          response1.parish,
          response1.zone_type,
          response1.route_type,
          response1.domicile_type].toString();
        let response2 = await Service.Common.geoLocation(ids);
        response3 = Object.assign(response1, response2);
      }
      return response3;
    }
    

  }

})();
