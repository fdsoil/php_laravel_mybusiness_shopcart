export const Replacement = (function () {

  return {  
  
    async insert(data) {
      let response = await Http.request({
          method: 'post',
          url: `${process.env.MIX_APP_URL}replacement`,
          data: data
      });      
      return response;
      //return response.data;
    },
    
    async update(data, id) {
      let response = await Http.request({
          method: 'post',
          url: `${process.env.MIX_APP_URL}replacement/${id}`,
          data: data
      });      
      return response;
      //return response.data;
    },
    
    async detailInsert(data) {
      let response = await Http.request({
          method: 'post',
          url: `${process.env.MIX_APP_URL}replacement/detail`,
          data: data
      });      
      return response.data;
    },
    
    async detailUpdate(data, id) {
      let response = await Http.request({
          method: 'post',
          url: `${process.env.MIX_APP_URL}replacement/detail/${id}`,
          data: data
      });      
      return response.data;
    },

    async detailDelete(data, id) {
      let response = await Http.request({
          method: 'post',
          url: `${process.env.MIX_APP_URL}replacement/detail/${id}`,
          data: data
      });      
      return response.data;
    }    

  }

})();
