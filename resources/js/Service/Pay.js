export const Pay = (function () {

  return {
       
    async make(data) {      
      let response = await Http.request({
          method: 'post',
          url: `${process.env.MIX_APP_URL}voucher`,
          data: data
      });      
      return response;
    }

  }

})();
