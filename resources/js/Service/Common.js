export const Common = (function () {

  return {

    async state() {
      let response = await Http.request({
        method: 'get',
        url: `${process.env.MIX_API_URL_COMMON}geo-location/state`
      });
      return response.data;
    },

    async municipality(stateId) {
      let response = await Http.request({
        method: 'get',
        url: `${process.env.MIX_API_URL_COMMON}geo-location/municipality/${stateId}`
      });
      return response.data;
    },

    async parish(municipalityId) {
      let response = await Http.request({
        method: 'get',
	    url: `${process.env.MIX_API_URL_COMMON}geo-location/parish/${municipalityId}`
      });
      return response.data;
    },

    async city(municipalityId) {
      let response = await Http.request({
        method: 'get',
        url: `${process.env.MIX_API_URL_COMMON}geo-location/city/${municipalityId}`
      });
      return response.data;
    },

    async zoneType() {
      let response = await Http.request({
        method: 'get',
        url: `${process.env.MIX_API_URL_COMMON}geo-location/zone-type`
      });
      return response.data;
    },

    async routeType() {
      let response = await Http.request({
        method: 'get',
        url: `${process.env.MIX_API_URL_COMMON}geo-location/route-type`
      });
      return response.data;
    },

    async domicileType() {
      let response = await Http.request({
        method: 'get',
        url: `${process.env.MIX_API_URL_COMMON}geo-location/domicile-type`
      });
      return response.data;
    },

    async geoLocation(ids) {
      let response = await Http.request({
	    method: 'get',
	    url: `${process.env.MIX_API_URL_COMMON}geo-location/${ids}`
      });
      return response.data;
    }    

  }

})();
