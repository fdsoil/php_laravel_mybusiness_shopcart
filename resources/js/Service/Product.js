export const Product = (function () {

  return {   

    async category() {
      let response = await Http.request({ 
          method: 'get', 
          url: `${process.env.MIX_API_URL_PRODUCT}category/get/list`
      });
      return response.data;
    },
    
    async categoryStock(availables) {
      let response = await Http.request({ 
          method: 'get', 
          url: `${process.env.MIX_API_URL_PRODUCT}category/get/stock/${availables}`
      });
      return response.data;
    },

    async productStock(categoryId, availables) { 
      let response = await Http.request({
          method: 'get',
          url: `${process.env.MIX_API_URL_PRODUCT}product/get/stock/${categoryId}/${availables}`
      });      
      return response.data;
    },

    async presentation(productId) {      
      let response = await Http.request({
          method: 'get',
          url: `${process.env.MIX_API_URL_PRODUCT}presentation/product/${productId}`
      });      
      return response.data;
    },

    async presentationStock(productId, availables) {      
      let response = await Http.request({
          method: 'get',
          url: `${process.env.MIX_API_URL_PRODUCT}presentation/stock/${productId}/${availables}`
      });      
      return response.data;
    },

    async presentationSearch(arg) {      
      let response = await Http.request({
          method: 'get',
          url: `${process.env.MIX_API_URL_PRODUCT}presentation/search/${arg}`
      });      
      return response.data;
    },
    
    async presentationIds(ids) {      
      let response = await Http.request({
          method: 'get',
          url: `${process.env.MIX_API_URL_PRODUCT}presentation/ids/${ids}`
      });      
      return response.data;
    }

  }

})();
