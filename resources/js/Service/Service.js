import {Article} from './Article.js';
import {Client} from './Client.js';
import {Common} from './Common.js';
import {Pay} from './Pay.js';
import {Product} from './Product.js';
import {Replacement} from './Replacement.js';

export const Service = (function () {

  return {
    Article,
    Client,
    Common,
    Pay,
    Product,
    Replacement
  }

})();
