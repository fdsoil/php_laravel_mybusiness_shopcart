import {Dom} from './modules/Dom.js';
import {Data} from './modules/Data.js';
import {Dates} from './modules/Dates.js';
import {FormatNumber} from './modules/FormatNumber.js';
import {Http} from './modules/Http.js';
import {Notification} from './modules/Notification.js';
import {Table} from './modules/Table.js';
import {Tabs} from './modules/Tabs.js';
import {Times} from './modules/Times.js';
import {WaitElement} from './modules/WaitElement.js';

import {Service} from './Service/Service.js';

window.Dom = Dom;
window.Data = Data;
window.Dates = Dates;
window.FormatNumber = FormatNumber;
window.Http = Http;
window.Notification = Notification;
window.Table = Table;
window.Tabs = Tabs;
window.Times = Times;
window.WaitElement = WaitElement;

window.Service = Service;
