export default {
  data:{
    article: {
      id: '',
      photo:''
    }
  },
  setArticleId (id){
    this.data.article.id = id;
  },
  setArticlePhoto (photo){
    this.data.article.photo = photo;
  }
}
