@extends('layouts.app')
@section('content')
  <style scoped>
    .imagen {
      width:100%;
      box-shadow: 0 0 10px gray;
    }
  </style>
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 d-flex flex-column  bg-white shadow">
                <div class="w-100 justify-content-between align-items-center p-3">
                    <div class="card-header"><h2 class="mb-0">Lista de existencia de artículos</h2></div>                  
                </div>
                <table id="tablaRows" class="table table-striped table-bordered compact" style="display:none;">
                    <thead>
                        <tr class="table-success text-center" >
			  <th style="color:black;" width="25%">Img / Cód</th>
                            <th style="color:black;" width="25%">Nombre / Descripción</th> 
                            <th style="color:black;" width="10%">Entradas</th>
                            <th style="color:black;" width="10%">Salidas</th>       
                            <th style="color:black;" width="10%">Reverso (E)</th>
                            <th style="color:black;" width="10%">Reverso (S)</th>
                            <th style="color:black;" width="10%">Existencias</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($stocks as $stock)
                        <tr>
			    <td width="10%">
                              <img class="imagen" src="{{ asset('/storage/article') .'/'. $stock['photo'] }}">
                              <span style="font-size:10px"> {{ $stock['int_cod'] }} </span>
                            </td>
			    <td align="left" width="40%" >
			      <span style="font-size:16px">{{ $stock['name'] }}</span>
                              <p style="font-size:12px">{{ $stock['descript'] }}</p>
                            </td>
                            <td align="center" valign="middle" width="10%">{{ $stock['inputs'] }}</td>
                            <td align="center" valign="middle" width="10%">{{ $stock['outputs'] }}</td>
                            <td align="center" valign="middle" width="10%">{{ $stock['reverse_inputs'] }}</td>
                            <td align="center" valign="middle" width="10%">{{ $stock['reverse_outputs'] }}</td>
                            <td align="center" valign="middle" width="10%">{{ $stock['total'] }}</td>                            
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                     <div class="text-center" style="margin:1%;">
                     <a type="buttom" class="btn btn-success" href="{{ route('home') }}">Regresar</a>
                  </div>
                </div>
                  
            </div>
        </div>
    </div>
@endsection
@section('after_scripts')
<script>    
    $(document).ready(function () {
        $('#tablaRows').DataTable({
            "bJQueryUI":true,
            "bSort":true,            
            "bPaginate":true,
            "sPaginationType":"full_numbers",
            "iDisplayLength": 5,
            "language": {
                "sSearch": "Buscar",
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Disculpe, nada encontrado",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtrado desde _MAX_ registros totales)",               
                "oPaginate": {
                     "sFirst": "Primera",
                     "sPrevious": "Anterior",
                     "sNext": "Siguiente",
                     "sLast": "Última"
                }
            }                  
        });
        $('#tablaRows').show();
    });
</script>
@endsection
