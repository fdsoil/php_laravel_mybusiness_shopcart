<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>cart</title>
  </head>
  <body>
    <div id="app">
     <tabdemo-component></tabdemo-component>
    </div>
    <script src="js/appVue.js"></script>    
    <script>
      const app = new Vue({
        el: '#app',
    });
    </script>
  </body>
</html>
