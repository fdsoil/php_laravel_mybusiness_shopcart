@extends('layouts.app')
@section('content')


<style>
  * {box-sizing: border-box}

  /* Set height of body and the document to 100% */
  body, html {
    height: 100%;
    margin: 0;
    font-family: Arial;
  }

  /* Style tab links */
  .tablink {
    background-color: #202426;   
    color: black;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    font-size: 17px;
    width: 25%;

    position: -webkit-sticky;
    position: sticky;
    top: 0;
    z-index: 1001;
  }

  .tablink:hover {
    background-color: #6c733d;
  }

  /* Style the tab content (and add height:100% for full page content) */
  .tabcontent {
    color: black;
    display: none;
    padding: 100px 20px;
    height: 100%;
  }

  #Home {background-color: #FFFFFF;}
  #News {background-color: #FFFFFF;}
  #Contact {background-color: #FFFFFF;}
</style>

<div id="app">
  <shopcart-component :availables="availables"></shopcart-component>
</div>

@endsection

@section('after_scripts')
  <script src="js/appVue.js"></script>
  <script>

    window.stocks = {!! $stocks !!};

    window.presentationIds = () => {
      let groupIds = [];
      stocks.forEach(stock => {
        groupIds.push(JSON.parse(stock["presentation_ids"]));
      });  
      let duplicateIds = groupIds.flat();
      let uniqueIds = duplicateIds.filter(
        (item, index) => duplicateIds.indexOf( item ) === index
      );
      return uniqueIds.sort( (a, b) => a - b );
    }

    window.availables = presentationIds(); 

    const app = new Vue({
      el: '#app',
      data: {
        availables: availables
      }
    });

    function openPage(pageName,elmnt,color) {
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }
      tablinks = document.getElementsByClassName("tablink");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].style.backgroundColor = "";
      }
      document.getElementById(pageName).style.display = "block";
      elmnt.style.backgroundColor = color;
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

    //document.getElementById("a1").textContent=localStorage.getItem('ced_rif') + ' -> ';
    //document.getElementById("a2").textContent=localStorage.getItem('business_name') + ' -> ';
    //document.getElementById("a3").textContent=localStorage.getItem('email');      
    //document.getElementById("ced_rif").textContent=localStorage.getItem('ced_rif');
    //document.getElementById("business_name").textContent=localStorage.getItem('business_name');
    //localStorage.getItem('sTruck')
  </script> 
@endsection

