@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 d-flex flex-column align-items-center bg-white shadow">
                <div class="d-flex w-100 justify-content-between align-items-center p-3">
                    <h1 class="mb-0">Mis compras</h1>                    
                </div>
                <table class="table table-striped">
                    <thead>
                        <tr class="table-info text-center">
                            <th scope="col">Número</th>
                            <th scope="col">Fecha / Hora</th>
                            <th scope="col">Cliente</th>                            
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($vouchers as $voucher)
                        <tr>                            
                            <td>{{ $voucher['number'] }}</td>
                            <td>{{ $voucher['date_time'] }}</td>
                            <td>{{ $voucher['business_name'] }}</td>                            
                            <td class="d-flex justify-content-around">
                                <a href="{{ route('shopcart.show', $voucher['id']) }}" class="btn btn-success font-weight-bold">Ver</a>                    
                                <a href="{{ route('shopcart.pdf', $voucher['id']) }}" class="btn btn-success font-weight-bold">Pdf</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                {!! $vouchers->links() !!}
                
                            <div class="form-group row justify-content-center">                    
                    <a href="{{ route('home') }}" class="btn btn-success font-weight-bold">Salir</a>  
               </div> 

            </div>

        </div>
    </div>
@endsection
