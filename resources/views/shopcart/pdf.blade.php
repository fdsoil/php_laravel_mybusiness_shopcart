<div>
  <h3>Mi Compra</h3><br/>              
  <table border="0">
    <thead>
      <tr>
        <th>Nùmero</th>    
        <th>Fecha/Hora</th>
        <th>Cliente</th>                        
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><strong>{{ $voucher->number }}</strong></td>
        <td><strong>{{ $voucher->date_time }}</strong></td>
        <td><strong>{{ $voucher->business_name }}</strong></td>                                                       
      </tr>                        
    </tbody>
  </table>                    
  <br/>
  <table border="1">
    <thead>
      <tr class="table-info text-center">                            
        <th scope="col">Artículo</th>
        <th scope="col">Descripción</th>
        <th scope="col">Precio</th>
        <th scope="col">Cantidad</th>
        <th scope="col">SubTotal</th>                           
      </tr>
    </thead>
    <tbody>
      @php
        $total = 0;
      @endphp
      @foreach ($voucherDetails as $voucherDetail)
      <tr>
        <td align="left">{{ $voucherDetail['name'] }}</td>
        <td align="left">{{ $voucherDetail['descript'] }}</td>
        <td align="right">{{ $voucherDetail['price'] }}</td>
        <td align="right">{{ $voucherDetail['quantity'] }}</td>
        <td align="right">{{ $voucherDetail['price']*$voucherDetail['quantity'] }}</td>
      </tr>
      @php
        $total += $voucherDetail['price']*$voucherDetail['quantity'];
      @endphp
      @endforeach
    </tbody>
    @php                    
      $tax = ($total *10)/100;
      $totGen = $total + $tax;
    @endphp                    
    <tfoot>
      <tr>                            
        <th align="right" colspan="4">SubTotal</th>  
        <td align="right"><strong>{{ $total }}</strong></td>                          
      </tr>
      <tr>                            
        <th align="right" colspan="4">10% Iva</th>  
        <td align="right" scope="col"><strong>{{ $tax }}</strong></td>                          
      </tr>
      <tr>                            
        <th align="right" scope="col" colspan="4">Total</th>  
        <td align="right" scope="col"><strong>{{ $totGen }}</strong></td>                          
      </tr>
    </tfoot>
  </table>                              
</div>
