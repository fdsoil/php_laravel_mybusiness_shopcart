@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-12 bg-white shadow p-3">
                <h3 class="text-center">Mi Compra</h3><br/>              
                <div class="form-group row">
                  <div class="col-sm-4">
                    <label for="email">Número</label><br/>
                    <span><strong>{{ $voucher->number }}</strong></span>
                  </div>
                  <div class="col-sm-4">
                    <label for="email">Fecha/Hora </label><br/>
                    <span><strong>{{ $voucher->date_time }}</strong></span>
                  </div>
                  <div class="col-sm-4">
                    <label for="email">Cliente </label><br/>
                    <span><strong>{{ $voucher->business_name }}</strong></span>
                  </div>
                </div>
                <br/> 
                <table class="table table-striped">
                    <thead>
                        <tr class="table-success text-center">                            
                            <th scope="col">Artículo</th>
                            <th scope="col">Descripción</th>
                            <th scope="col">Precio</th>
                            <th scope="col">Cantidad</th>  
                            <th scope="col">SubTotal</th>                          
                        </tr>
                    </thead>
                    <tbody>                    
                        @php
                            $total = 0;
                        @endphp                    
                        @foreach ($voucherDetails as $voucherDetail)
                        <tr>
                            <td align="left">{{ $voucherDetail['name'] }}</td>
                            <td align="left">{{ $voucherDetail['descript'] }}</td>
                            <td align="right">{{ $voucherDetail['price'] }}</td>
                            <td align="right">{{ $voucherDetail['quantity'] }}</td>      
                            <td align="right">{{ $voucherDetail['price']*$voucherDetail['quantity'] }}</td>                                                                             
                        </tr>
                        @php
                            $total += $voucherDetail['price']*$voucherDetail['quantity'];
                        @endphp                        
                        @endforeach
                    </tbody>
                    @php                    
                        $tax = ($total *10)/100;
                        $totGen = $total + $tax;
                    @endphp                    
                    <tfoot>
                        <tr>                            
                            <th class="table-info text-right" scope="col" colspan="4">SubTotal</th>  
                            <td class="table-success text-right" scope="col"><strong>{{ $total }}</strong></td>                          
                        </tr>
                        <tr>                            
                            <th class="table-info text-right" scope="col" colspan="4">10% Iva</th>  
                            <td class="table-success text-right" scope="col"><strong>{{ $tax }}</strong></td>                          
                        </tr>
                        <tr>                            
                            <th class="table-info text-right" scope="col" colspan="4">Total</th>  
                            <td class="table-success text-right" scope="col"><strong>{{ $totGen }}</strong></td>                          
                        </tr>
                    </tfoot>
                </table>                
                <div class="form-group row justify-content-center">                    
                    <a href="{{ route('shopcart.list', ['id' => $voucher->client_id ]) }}" class="btn btn-success font-weight-bold">Regresar</a>
                    &nbsp;&nbsp;&nbsp;
                    <a href="{{ route('shopcart.pdf', $voucher->id) }}" class="btn btn-success font-weight-bold">Imprimir</a>
               </div>               
            </div>
        </div>
    </div>
@endsection
@section('after_scripts')
@endsection

