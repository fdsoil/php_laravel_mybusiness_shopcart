@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 d-flex flex-column align-items-center bg-white shadow">
                <div class="d-flex w-100 justify-content-between align-items-center p-3">
                    <h1 class="mb-0">Consultar Ventas</h1>                    
                </div>
                <table class="table table-striped">
                    <thead>
                        <tr class="table-success text-center">
                            <th scope="col">Nùmero</th>
                            <th scope="col">Fecha / Hora</th>
                            <th scope="col">Cliente</th>                            
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($vouchers as $voucher)
                        <tr>                            
                            <td>{{ $voucher['number'] }}</td>
                            <td>{{ $voucher['date_time'] }}</td>
                            <td>{{ $voucher['business_name'] }}</td>                            
                            <td class="d-flex justify-content-around">
                                <a href="{{ route('voucher.show', $voucher['id']) }}" class="btn btn-primary font-weight-bold">Ver</a>                                                       
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                {!! $vouchers->links() !!}

            </div>
        </div>
    </div>
@endsection
