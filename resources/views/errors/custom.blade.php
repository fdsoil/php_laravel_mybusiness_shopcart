@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 d-flex flex-column  bg-white shadow">
                <div class="w-100 justify-content-between align-items-center p-3">
                    <div class="card-header"><h2 class="mb-0">Exception details: <b>{{ $exception->getMessage() }}</b></h2></div>                  
                </div>
                     <div class="text-center" style="margin:1%;">
                     <a type="buttom" class="btn btn-success" href="{{ route('home') }}">Regresar</a>
                  </div>
                </div>
                  
            </div>
        </div>
    </div>
@endsection

