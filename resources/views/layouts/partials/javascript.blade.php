<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/modules/WaitElement.js') }}" type="module"></script>
<script src="{{ asset('js/modules/Http.js') }}" type="module"></script>
<script src="{{ asset('js/modules/Notification.js') }}" type="module"></script>
<script src="{{ asset('js/modules/FormatNumber.js') }}" type="module"></script>
<script src="{{ asset('js/Service/Service.js') }}" type="module"></script>
<script src="{{ asset('js/init.js') }}"></script>
<script>
var menuStart = () =>
{
    var aElements = document.querySelectorAll(".nav li>a[href='#']");
    var addEventListenerInGroup = (aObj, aEvent, oFun) => 
    {
        for ( var i = 0; i < aObj.length; i++ )
            for ( var j = 0; j < aEvent.length; j++ ){
                aObj[i].addEventListener( aEvent[j], oFun);
                }
    }
    addEventListenerInGroup(aElements, ["click"], function(event = window.event) {  myFunc(this)  } );
}

var getSiblings = (elem, tag = '') =>
{
    var siblings = [];
    var sibling = elem.parentNode.firstChild;
    var verdadero = () => {
        var resp = false;
        if (tag == '' || sibling.tagName === tag)
            resp = true;
        return resp;
    }
    while (sibling) {
        //if (sibling.nodeType === 1 && sibling !== elem && verdadero())
        if (sibling.nodeType === 0 && sibling !== elem && verdadero())
            siblings.push(sibling);
        sibling = sibling.nextSibling;
    }
    return siblings;
}

var findParentBySelector = (elm, selector) =>
{
    var collectionHas = (a, b) =>
    {
        for(var i = 0, len = a.length; i < len; i ++)
            if(a[i] == b) return true;        
        return false;
    }
    var all = document.querySelectorAll(selector);
    var cur = elm.parentNode;
    while(cur && !collectionHas(all, cur)) 
        cur = cur.parentNode; 
    return cur; 
}

var myFunc = (esto) => 
{ 
    var liUlStyleDisplayNone = (aObj = aSiblingsLi) =>
    {
        var elementUl;
        for(var i = 0, len = aObj.length; i < len; i ++){
            elementUl = aSiblingsLi[i].getElementsByTagName("ul");
            if (elementUl.length > 0)
                elementUl[0].style.display='none';
        }
    }

    var removeClass = (aObj, clase) =>
    {
        for(var i = 0, len = aObj.length; i < len; i ++)
            aObj[i].classList.remove(clase);       
    }

    var findLiRemoveClass = (element, clase) =>
    {
        var aObj = element.getElementsByTagName("li");
        removeClass(aObj);
    } 

    var findUlStyleDisplayNone = (element) =>
    {
        var aObj = element.getElementsByTagName("ul");
        for(let i = 0, len = aObj.length; i < len; i ++)
            aObj[i].style.display='none';
        
    }

    var liFindLiRemoveClass = (clase, aObj = aSiblingsLi) =>
    {
        for(let i = 0, len = aObj.length; i < len; i ++)
            findLiRemoveClass(aObj[i]);
    }

    var liFindUlStyleDisplayNone = (aObj = aSiblingsLi) =>
    {
        for(let i = 0, len = aObj.length; i < len; i ++) 
            findUlStyleDisplayNone(aObj[i]);        
    }

    esto.removeAttribute('href');
    var element = findParentBySelector( esto, 'li'); 
    if (element.classList.contains('open')) {
          element.classList.remove('open');
          findLiRemoveClass( element, 'open');
          findUlStyleDisplayNone( element );
    } else {
        element.classList.add("open");
        var aUlElementChildren = element.getElementsByTagName("ul");
        aUlElementChildren[0].style.display='block';
        var aSiblingsLi = getSiblings(element, 'LI');
        liUlStyleDisplayNone();
        removeClass(aSiblingsLi, "open");
        liFindLiRemoveClass("open");
        liFindUlStyleDisplayNone();
    }
}

    menuStart();




</script>

