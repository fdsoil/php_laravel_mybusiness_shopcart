<footer class="text-muted jumbotron" style="background-color:#202426;">
  <div class="row justify-content-center">
    <div class="col-6 col-md-2 mb-md-0 d-flex justify-content-center grow">
      <a href="#">
        <img src="../../img/facebook.png" style="margin:auto">                
      </a>
    </div>
    <div class="col-6 col-md-2 d-flex justify-content-center grow">
      <a href="#">
        <img src="../../img/gplus.png" style="margin:auto">                
      </a>
    </div>
    <div class="col-6 col-md-2 mb-md-0 d-flex justify-content-center grow">
      <a href="#">
        <img src="../../img/twitter.png" style="margin:auto">                
      </a>
    </div>
    <div class="col-6 col-md-2 d-flex justify-content-center grow">
      <a href="#">
        <img src="../../img/youtube.png" style="margin:auto">                
      </a>
    </div>
  </div>
  
    <p class="float-left">
      <a href="#">
          <img src="../../img/phone.svg" width="3%" style="margin:auto">  
          Contactenos
      </a>
    </p>
    <p class="float-right">
      <a href="#">
          <img src="../../img/return.svg" width="15%" style="margin:auto">  
          Volver a arriba
      </a>
    </p>
  
    
</footer>
