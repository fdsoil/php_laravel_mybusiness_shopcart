@if(Session::has('info'))
<div class="row justify-content-center">
  <div class="alert alert-info text-center alert-dismissible fade show shadow h4 py-3 col-md-10">
    <button type="button" class="close" data-dismiss="alert">
      &times;   
    </button>
    {{ Session::get('info') }}
  </div>
</div>
@elseif(Session::has('success'))
<div class="row justify-content-center">
  <div class="alert alert-success text-center alert-dismissible fade show shadow h4 py-3 col-md-10">
    <button type="button" class="close" data-dismiss="alert">
      &times;   
    </button>
    {{ Session::get('success') }}
  </div>
</div>
@elseif(Session::has('error'))
<div class="row justify-content-center">
  <div class="alert alert-danger text-center alert-dismissible fade show shadow h4 py-3 col-md-10">
    <button type="button" class="close" data-dismiss="alert">
      &times;   
    </button>
    {{ Session::get('error') }}
  </div>
</div>
@endif
