<style>
.sticky{
    margin-top: -33px;
    margin-bottom: 0px;
    margin-right: 50px;
    margin-left: 0%;
    background:  #9da65d;
    
    width:100%;
    position: -webkit-sticky;
    position: sticky;
    top: 0;
    z-index: 1000;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;    
  padding: 6px 8px;
  z-index: 1;
}

.dropdown:hover .dropdown-content {  
  display: block;
}

.nav li a{
    background-color:#9da65d;
}

.nav li a:hover {
    background-color:#8c8c88;
    font-weight: bold;    
}
</style>



<nav class="navbar navbar-expand-md navbar-light shadow-sm sticky">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            Inicio
            {{--config('app.name', 'Laravel') --}}
        </a>
        <button class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="{{ __('Habilitar navegación') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of Navbar --><!--ul class="navbar-nav ml-auto"-->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @guest
                    <!--li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Registrarse') }}</a>
                        </li>
                    @endif-->
                @else
                    {{--@if (Auth::user()->is_admin==1)--}}
                        @each('layouts.partials.menu', session('menu'), 'menu', 'layouts.partials.menus-none')
                    {{--@endif--}}
                    
                        <li class="nav-item dropdown">
                        <a id="navbarDropdown"
                           class="nav-link dropdown-toggle"
                           href="#"
                           role="button"
                           data-toggle="dropdown"
                           aria-haspopup="true"
                           aria-expanded="false"
                           v-pre>
                            {{ Auth::user()->name }}
                        </a>

                        <div class="dropdown-content" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Salir') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
