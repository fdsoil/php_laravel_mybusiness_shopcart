<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  @include('layouts.partials.head')
</head>
<body style="background-color:#202426;">
  @include('layouts.partials.header')
  @include('layouts.partials.navbar')    
  @include('layouts.partials.message')
  <div style="background-color:#6c733d;">
  <main class="py-4">
    @yield('content')
  </main>
  </div>
  @include('layouts.partials.footer')
  @include('layouts.partials.javascript')
  @yield('after_scripts')    
</body>
</html>
