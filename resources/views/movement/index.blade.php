@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 d-flex flex-column  bg-white shadow">
                <div class="w-100 justify-content-between align-items-center p-3">
                    <div class="card-header"><h2 class="mb-0">Lista de Movimientos</h2></div>                  
                </div>
                <table id="tablaRows" class="table table-striped table-bordered compact" style="display:none">
                    <thead>
                        <tr class="table-success text-center">
                            <th>Número</th>
                            <th>Tipo</th>
                            <th>Fecha / Hora</th>       
                            <th>Sujeto</th>
                            <th>Descripción</th>
                            <th>Cerrado</th>                                              
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($movements as $movement)
                        <tr>
                            <td>{{ $movement['number'] }}</td>
                            <td align="center">{{ $movement['type_id'] ==1 ? 'ENTRADA' : 'SALIDA' }}</td>
                            <td>{{ $movement['date_time'] }}</td>
                            <td>{{ $movement['subject'] }}</td>
                            <td>{{ $movement['description'] }}</td>
                            <td>{{ $movement['close'] }}</td>
                            <td  class="justify-content-around">
                              <a href="{{ route('movement.show', $movement['id']) }}" class="btn btn-success font-weight-bold">Ver</a>                       
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                     <div class="text-center" style="margin:1%;">
                     <a type="buttom" class="btn btn-success" href="{{ route('home') }}">Regresar</a>
                  </div>
                </div>
                  
            </div>
        </div>
    </div>
@endsection
@section('after_scripts')
<script>    
    $(document).ready(function () {
        $('#tablaRows').DataTable({
            "bJQueryUI":true,
            "bSort":true,            
            "bPaginate":true,
            "sPaginationType":"full_numbers",
            "iDisplayLength": 5,
            "language": {
                "sSearch": "Buscar",
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Disculpe, nada encontrado",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtrado desde _MAX_ registros totales)",               
                "oPaginate": {
                     "sFirst": "Primera",
                     "sPrevious": "Anterior",
                     "sNext": "Siguiente",
                     "sLast": "Última"
                }
            }                  
        });
        $('#tablaRows').show();
    });
</script>
@endsection
