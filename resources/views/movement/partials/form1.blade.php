<style scoped>
  .imagen {
    width:100%;
    box-shadow: 0 0 10px gray;
  }
</style>
<div class="form-group row">
  <div class="col-sm-12">
    <div  align="center">    
      <div style="display:{{ isset($readonly) ? 'none' : '' }}">
        <table style="cursor:pointer;" onclick="Replacement.Detail.onOffPanel();">
          <tr>
            <td>
              <div class="subtitulo_datos">Agregar Productos</div>
            </td>
            <td>
              <div id="id_img_presentacion" title="Mostrar Panel" class="img_show"></div>
            </td>
          </tr>
        </table>
      </div><br/>
      <div id="id_panel_presentacion" style="display: none;">
        <input id="detail_id" name="detail_id" value="0" type="hidden">
        <table class="table table-striped table-bordered compact" style="width: 75%">
          <tr>
            <th colspan="2" class="table-info text-center">Datos de Presentación</th>
          </tr>            
          <tr>
            <td colspan="2"><label>Producto</label>
              <input type="hidden" 
                     name="presentation_id" 
                     id="presentation_id">
              <textarea id="presentation"
                        name="presentation"
                        type="text"
                        autocomplete="off"
                        style="width: 98%;"
                        data-format="uppercase"
                        data-constraints=""
                        data-validation="required"
                        readonly
                        placeholder='SELECCIONE...'></textarea>
            </td>
          </tr>
          <tr>
            <td><label>Cantidad Solicitada</label>
              <input id="amount_requested"
                     name="amount_requested"
                     type="text"
                     autocomplete="off"
                     style="width: 98%;">
            </td>
            <td><label>Cantidad Recibida</label>
              <input id="amount_received"
                     name="amount_received"
                     type="text"
                     autocomplete="off"
                     style="width: 98%;">
            </td>
          </tr>
        </table>
        <div style="margin-bottom:2%">
          <button id="addReg" type="button" class="btn btn-primary" onClick="Replacement.Detail.valEnvio();">Agregar</button>
        </div>
      </div>     
      <table id="id_tab_presentacion" class="table table-striped table-bordered compact" width="100%">
        <thead class="table-success text-center">
          <tr>
            <th>Imagen</th>
            <th>Código</th>
            <th>Artículo</th>
            <th>Descripción</th> 
            <th>Cantidad</th>
            @if(!isset($readonly))        
              <th>Acción(es)</th>
            @endif
          </tr>
        </thead>
        <tbody>
        @if (isset($details))
          @foreach ($details as $detail)
          <tr id="{{ $detail['id'] }}" hiddendata="[ {{ $detail['article_id'] }} ]">
            <td width="10%"><img class="imagen" src="{{ asset('/storage/article') .'/'. $detail['photo'] }}"</td>
            <td align="center">{{ $detail['int_cod'] }}</td>
            <td align="left">{{ $detail['name'] }}</td>
            <td align="left">{{ $detail['descript'] }}</td>
            <td align="right">{{ $detail['quantity'] }}</td>
            @if(!isset($readonly))        
              <td>
                <img style="cursor:pointer"
                     src="../../img/edit.svg"
                     title="Editar datos..."
                     onclick="Replacement.Detail.edit(this);//valBtnClose();">
                <img style="cursor:pointer"
                     src="../../img/delete.svg"
                     title="Eliminar/Borrar datos..."
                     onclick="Replacement.Detail.remove({{ $detail['id'] }});//valBtnClose();">
              </td>
            @endif     
          </tr>                                           
          @endforeach
        @endif
        </tbody>
      </table>
    </div>
  </div>
</div>


