<input id="id"
           name="id"
           type="hidden" 
           value="{{ isset($movement) && $movement->id ? $movement->id : old('id') }}"> 

<div class="form-group row" align="left">
  <div class="col-sm-6">
    <label for="subject">Sujeto</label>
    <input id="subject"
           name="subject"
           type="text" 
           autocomplete="off"
           style="width: 100%;"
           value="{{ isset($movement) && $movement->subject ? $movement->subject : old('subject') }}" 
           {{ isset($readonly) ? "readonly" : null }}> 
    @error('subject')
      <span class="invalid-feedback" role="alert"> isset($movement)
        <strong>{{ $message }}</strong>
      </span>
    @enderror
  </div>

  <div class="col-sm-6">
    <label for="description">Descripción</label>
    <textarea id="description"
           name="description"            
           autocomplete="off"
           style="width: 100%;"
           {{ isset($readonly) ? "readonly" : null }}>{{ isset($movement) && $movement->description ? $movement->description : old('description') }}</textarea>
    @error('description')
      <span class="invalid-feedback" role="alert"> isset($movement)
        <strong>{{ $message }}</strong>
      </span>
    @enderror
    <br/>
  </div>
  
  
    <div class="col-sm-6">    
    <label for="observation">Observatión</label>
    <textarea id="observation"
           name="observation"            
           autocomplete="off"
           style="width: 100%;"
           {{ isset($readonly) ? "readonly" : null }}>{{ isset($movement) && $movement->observation ? $movement->observation : old('observation') }}</textarea>
    @error('observation')
      <span class="invalid-feedback" role="alert"> isset($movement)
        <strong>{{ $message }}</strong>
      </span>
    @enderror
  </div>
  
  
  
   <div class="col-sm-6">    
    <label for="received">Tipo Soporte</label>
    <input id="received"
           name="received"
           type="text" 
           autocomplete="off"
           style="width: 100%;"
           value="{{ isset($movement) && $movement->support_type_id ? $movement->support_type_id : old('support_type_id') }}" 
           {{ isset($readonly) ? "readonly" : null }}> 
    @error('name')
      <span class="invalid-feedback" role="alert"> isset($movement)
        <strong>{{ $message }}</strong>
      </span>
    @enderror
  </div>  


 <div class="col-sm-6">    
    <label for="received">Numero de Soporte</label>
    <input id="received"
           name="received"
           type="text" 
           autocomplete="off"
           style="width: 100%;"
           value="{{ isset($movement) && $movement->support_number ? $movement->support_number : old('support_number') }}" 
           {{ isset($readonly) ? "readonly" : null }}> 
    @error('name')
      <span class="invalid-feedback" role="alert"> isset($movement)
        <strong>{{ $message }}</strong>
      </span>
    @enderror
  </div>  

 <div class="col-sm-6">    
    <label for="received">Fecha de Soporte</label>
    <input id="received"
           name="received"
           type="text" 
           autocomplete="off"
           style="width: 100%;"
           value="{{ isset($movement) && $movement->support_date ? $movement->support_date : old('support_date') }}" 
           {{ isset($readonly) ? "readonly" : null }}> 
    @error('name')
      <span class="invalid-feedback" role="alert"> isset($movement)
        <strong>{{ $message }}</strong>
      </span>
    @enderror
  </div>  

</div>



