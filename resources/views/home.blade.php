@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
    @if (Auth::user()->is_admin==1)
        <div class="col-12 col-md-4 mb-4 mb-md-0 d-flex justify-content-center grow">            
          <img src="../../img/user.svg" width="50%" style="margin:auto">
        </div>
        <!--div class="col-12 col-md-4 mb-4 mb-md-0 d-flex justify-content-center grow" href="{{route('register')}}">
            <a href="{{route('user.index')}}" class="card text-center w-75 p-5 text-body">
                <img src="../../img/user.svg" width="50%" style="margin:auto">
                Registrar Usuarios
            </a>
        </div>
        <div class="col-12 col-md-4 d-flex justify-content-center grow">
            <a href="{{route('client.index')}}" class="card text-center w-75 p-5 text-body">
                <img src="../../img/user.svg" width="50%" style="margin:auto">
                Registrar Clientes
            </a>
        </div>
        <div class="col-12 col-md-4 d-flex justify-content-center grow">
            <a href="{{route('voucher.index')}}" class="card text-center w-75 p-5 text-body" align="center">
                <img src="../../img/iconmonstr-search-thin.svg" width="50%" style="margin:auto">
                Consultar Compras
            </a>
        </div>
        <div class="col-12 col-md-4 d-flex justify-content-center grow">
            <a href="{{route('replacement.index')}}" class="card text-center w-75 p-5 text-body" align="center">
                <img src="../../img/iconmonstr-search-thin.svg" width="50%" style="margin:auto">
                Reposición
            </a>
        </div>
        <div class="col-12 col-md-4 d-flex justify-content-center grow">
            <a href="{{route('movement.index')}}" class="card text-center w-75 p-5 text-body" align="center">
                <img src="../../img/iconmonstr-search-thin.svg" width="50%" style="margin:auto">
                Movimientos
            </a>
        </div-->
    @else
    
            <div class="col-12 col-md-4 mb-4 mb-md-0 d-flex justify-content-center grow">            
          <img src="../../img/cart.svg" width="50%" style="margin:auto">
        </div>
    
    <!--div class="col-12 col-md-4 mb-4 mb-md-0 d-flex justify-content-center grow" href="{{route('shopcart.index')}}">
            <a href="{{route('shopcart.index')}}" 
               class="card align-center text-center w-75 p-5 text-body"
               style="display:none;"
               id="a_shopcart">              
               <img src="../../img/cart.svg" width="50%" style="margin:auto">
                Carrito de Compra New
            </a>
        </div>
        <div class="col-12 col-md-4 mb-4 mb-md-0 d-flex justify-content-center grow" href="{{route('register')}}">
            <a href="{{route('register')}}" 
               class="card align-center text-center w-75 p-5 text-body"
               style="display:none;"
               id="a_shopcart">              
               <img src="../../img/cart.svg" width="50%" style="margin:auto">
                Carrito de Compra Old
            </a>
        </div>
        <div class="col-12 col-md-4 d-flex justify-content-center grow">
            <a href="#" class="card text-center w-75 p-5 text-body" style="display:none" id="a_list"  align="center">
                <img src="../../img/iconmonstr-search-thin.svg" width="50%" style="margin:auto">
                Consultar Compras
            </a>
        </div-->        
    @endif

    </div>
</div>
@endsection
@section('after_scripts')
  <script src="{{ asset('js/home.js') }}"></script>
  <script>home('{{ Auth::user()->email }}');</script>
@endsection
