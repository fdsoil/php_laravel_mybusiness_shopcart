@extends('layouts.app')
@section('content')
  <link href="{{ asset('css/grid.css') }}" rel="stylesheet">  
  <div id="app" class="container">
    <div class="row">
      <div class="row justify-content-center align-items-center">
        <div class="col-12 d-flex flex-column shadow" style="background-color: #8c8c88; padding: 1% 1% 1% 1%;">
          <article-component></article-component>
        </div>
      </div>        
    </div>
  </div>  
@endsection

@section('after_scripts')
  <script src="js/appVue.js"></script>  
  <script>
    const app = new Vue({  
      el: '#app',    
    });  
  </script>     
@endsection
