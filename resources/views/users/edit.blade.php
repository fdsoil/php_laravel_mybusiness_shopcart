@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Editar Usuario') }}</div>
                <div class="card-body">                    
                    <form method="POST" action="{{ route('user.update', $user->id) }}">
                        @method('PUT')
                        @include('users.partials.form', ['user' => $user])
                        <div class="text-center"> 
                            <a type="buttom" class="btn btn-success" href="{{ route('user.index') }}">Regresar</a>
                            &nbsp;&nbsp;&nbsp;                       
                            <button class="btn btn-primary">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
