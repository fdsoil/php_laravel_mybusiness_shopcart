@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Detalles del Usuario') }}</div>

                <div class="card-body">
                    
                    {{-- <form method="POST" action="#"> --}}
                        @include('users.partials.form', ['user' => $user, 'readonly' => true])
                         <div class="text-center">
                            <a type="buttom" class="btn btn-success" href="{{ route('user.index') }}">Regresar</a>
                            {{--<button class="btn btn-primary">Registrar</button>--}}
                        </div> 
                    {{-- </form> --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
