@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 d-flex flex-column align-items-center bg-white shadow">
            <div class="d-flex w-100 justify-content-between align-items-center p-3">
                <h1 class="mb-0">Usuarios</h1>
                <a href="{{ route('register') }}" class="btn btn-primary font-weight-bold">Nuevo</a>
            </div>
            <table class="table table-striped">
                <thead>
                    <tr class="table-success text-center">
                        <th scope="col">Nombre</th>
                        <th scope="col">Email</th>
                        <th scope="col">Email Verificado</th>
                        <th scope="col">Es Administrador</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->email_verified_at }}</td>
                        <td>{{ $user->is_admin ? 'yes' : 'no' }}</td>
                        <td class="d-flex justify-content-around">
                            <a href="{{ route('user.show', $user->id) }}" class="btn btn-success font-weight-bold">Ver</a>
                            @if($user->id != 1)
                            <a href="{{ route('user.edit', $user->id) }}" class="btn btn-primary font-weight-bold">Modificar</a>
                            <button class="btn btn-danger font-weight-bold"
                                data-toggle="modal" data-target="{{'#userDeleteModal'.$user->id}}">Eliminar</button>
                                @include('users.partials.modal_delete', ['modalId' => 'userDeleteModal'.$user->id])
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            {!! $users->links() !!}

        </div>
    </div>
</div>
@endsection
