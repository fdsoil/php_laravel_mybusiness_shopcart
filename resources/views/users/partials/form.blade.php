@csrf

<div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

    <div class="col-md-6">
        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name ?? old('name') }}" {{ isset($readonly) ? "readonly" : null }} autocomplete="name" autofocus >

        @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

    <div class="col-md-6">
        <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email ?? old('email') }}" {{ isset($readonly) ? "readonly" : null }} autocomplete="email">

        @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="is_admin" class="col-md-4 text-md-right col-form-label">Rol</label>
    <div class="col-md-6">
        <select name="is_admin" class="form-control" id="is_admin" {{ isset($readonly) ? "readonly" : null }}>
            @foreach($roles as $role)
            <option value="{{$role['value']}}" @isset($user) {{ $user->is_admin == $role['value']  ? "selected" : null }} @endisset>{{$role['name']}}</option>
            @endforeach
        </select>
    </div>
</div>
@if(!isset($readonly))
<div class="form-group row">
    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña') }}</label>

    <div class="col-md-6">
        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" {{ isset($readonly) ? "readonly" : null }} autocomplete="new-password">

        @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>
@endif
@if(!isset($readonly))
<div class="form-group row">
    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmar Contraseña') }}</label>

    <div class="col-md-6">
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" {{ isset($readonly) ? "readonly" : null }} autocomplete="new-password">
    </div>
</div>
@endif
