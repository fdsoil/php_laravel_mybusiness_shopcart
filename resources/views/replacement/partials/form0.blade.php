<input id="id"
           name="id"
           type="hidden" 
           value="{{ isset($replacement) && $replacement->id ? $replacement->id : old('id') }}"> 

<div class="form-group row" align="left">
  <div class="col-sm-6">
    <label for="subject">Sujeto</label>
    <input id="subject"
           name="subject"
           type="text" 
           autocomplete="off"
           style="width: 100%;"
           value="{{ isset($replacement) && $replacement->subject ? $replacement->subject : old('subject') }}" 
           {{ isset($readonly) ? "readonly" : null }}> 
    @error('subject')
      <span class="invalid-feedback" role="alert"> isset($replacement)
        <strong>{{ $message }}</strong>
      </span>
    @enderror
  </div>

  <div class="col-sm-6">
    <label for="description">Descripción</label>
    <textarea id="description"
           name="description"            
           autocomplete="off"
           style="width: 100%;"
           {{ isset($readonly) ? "readonly" : null }}>{{ isset($replacement) && $replacement->description ? $replacement->description : old('description') }}</textarea>
    @error('description')
      <span class="invalid-feedback" role="alert"> isset($replacement)
        <strong>{{ $message }}</strong>
      </span>
    @enderror
    <br/>
  </div>
  
  
    <div class="col-sm-6">    
    <label for="observation">Observatión</label>
    <textarea id="observation"
           name="observation"            
           autocomplete="off"
           style="width: 100%;"
           {{ isset($readonly) ? "readonly" : null }}>{{ isset($replacement) && $replacement->observation ? $replacement->observation : old('observation') }}</textarea>
    @error('observation')
      <span class="invalid-feedback" role="alert"> isset($replacement)
        <strong>{{ $message }}</strong>
      </span>
    @enderror
  </div>
  
  
  
   <div class="col-sm-6">    
    <label for="received">Recibido</label>
    <input id="received"
           name="received"
           type="text" 
           autocomplete="off"
           style="width: 100%;"
           value="{{ isset($replacement) && $replacement->received ? $replacement->received : old('received') }}" 
           {{ isset($readonly) ? "readonly" : null }}> 
    @error('name')
      <span class="invalid-feedback" role="alert"> isset($replacement)
        <strong>{{ $message }}</strong>
      </span>
    @enderror
  </div>  

</div>

