<div id="div_deshacer_modal_article" 
     style="position: fixed;
            padding: 5px;
            background: #E6E6E6;
            color: #333;
            z-index: 2002;
            border: 5px solid;
            border-color: gray;
            border-radius: 10px;
            left: 5%;
            top: 5%;
            width: 85%;
            height: 85%;
            overflow:auto;
            display: none;">
  <div style="float:left;width:40%;text-align:left;">
    <label>Buscar Producto</label>
    <input id="product_search" 
           name="product_search" 
           type="text"
           data-format="uppercase"
           stylse="text-transform:uppercase"
           onkeyup="var start = this.selectionStart;
                    var end = this.selectionEnd;
                    this.value = this.value.toUpperCase();
                    this.setSelectionRange(start, end);
                    Replacement.Detail.Search.get();"
           style="width:100%;">
  </div>
  <div style="float:right;">
    <button id="id_cancel_article" type=button class="btn btn-danger">
        Cancelar
    </button>
  </div>
  <div>
    <table width="100%;" class="table table-striped table-bordered compact" id="id_tab_art">
      <thead class="table-success text-center">       
        <tr>
          <th>Imagen</th>
          <th>Còdigo Interno</th>
          <th>Artículo</th>
          <th>Descripción</th>
        </tr>
      </thead>
      <tbody></tbody>
    </table>
  </div>
</div>
<div id="div_deshacer_fondo_opaco"
     style="position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: #2E2E2E;
            z-index: 2001;
            opacity:.75;
            -moz-opacity: 0.75;
            filter: alpha(opacity=75);
            display: none;">
</div>

