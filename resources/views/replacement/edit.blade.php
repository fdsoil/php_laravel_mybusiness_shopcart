@extends('layouts.app')
@section('content')

<link rel="stylesheet" href="{{ asset('css/fdsoil/Tabs.css') }}"> 
<link rel="stylesheet" href="{{ asset('css/fdsoil/deniedWritingImg.css') }}"> 

<div class="container">
  <div class="row justify-content-center align-items-center">
    <div class="col-24 col-md-16 bg-white shadow p-3">                
      <div class="card-header"> <h4>Actualizar Reposición</h4> </div>
      <div class="card-body">



<div class="div_datos_cod_factura_fec_factura" style="margin-bottom: 5%;" >
    <div style="float:left;aling:left; width:35%;">
        <table class="tabla_datos_usuario_f" width="100%" border ="0">
            <tr>
                <td class='subtitulo_datos_f' width="20%">Número:</td>
                <td class='datos_per_entrada_f' width="80%" align="left" id="id_form_number">{{ isset($replacement) && $replacement->number ? $replacement->number : '' }}</td>
            </tr>
        </table>
    </div>
    <div style="float:right;aling:right; width:35%;">
        <table class="tabla_datos_usuario_f" width="100%" border ="0">
            <tr>
                <td class='subtitulo_datos_f' width="20%">Fecha:</td>
                <td class='datos_per_entrada_f' width="80%" align="left" id="id_form_date">{{ isset($replacement) && $replacement->date_time ? $replacement->date_time : '' }}</td>
            </tr>
        </table>
    </div>                              
</div>

    <div id="panel" style="display: block; clear:both;"> 
      <div class="halfmoon" id ="div_navigation">
          <ul>
            <li style="display:">
              <a id="Tab0" style="cursor:pointer; display:">Datos Básicos
                <img src="{{ asset('img/s_okay.png') }}" width="10" height="10" id="chk0" style="style:hidden" border=0>
              </a>
            </li>
            <li style="display:">
              <a id="Tab1" style="cursor:pointer; display:{TAB_NONE_BLOCK1}">Detalles
                <img src="{{ asset('img/s_okay.png') }}" width="10" height="10" id="chk1" style="style:hidden" border=0>
              </a>
            </li>            
          </ul>
        </div>
        <div id="taps_buttons">
          <table id="buttons" align="center" style="display:none">
            <tr>
              <td>
                <input id="id_back"
                       type="button"
                       value="Atras">
              </td>
              <td>
                <input id="id_forward"
                       type=button
                       value="Adelante">
              </td>
            <tr>
          </table>
        </div>
        <div id="div0"
             class="divSolapa"
	         align="center"
             title="..."
	         label="t"
             style="display:block">@include('replacement.partials.form0')</div>
        <div id="div1"
             class="divSolapa"
             align="center"
             title="..."
	         label="t"
	         style="display:none">@include('replacement.partials.form1')</div>
        <div class="text-center">
          <a type="buttom" class="btn btn-success" href="{{ route('replacement.index') }}">Regresar</a>
          &nbsp;&nbsp;&nbsp;
          <button class="btn btn-primary" id="id_save" onClick="Replacement.update({{ $replacement->id}} );">Guardar</button>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
@section('after_scripts')
<script src="{{ asset('js/jquery-ui-custom.js') }}"></script>
<script src="{{ asset('js/numero.js') }}"></script>
<script src="{{ asset('js/effect.js') }}"></script>
<script src="{{ asset('js/modules/Tabs.js') }}" type="module"></script>
<script src="{{ asset('js/modules/Table.js') }}" type="module"></script>
<script src="{{ asset('js/modules/Data.js') }}" type="module"></script>
<script src="{{ asset('js/fdsoil/Arr.js') }}"></script>
<script src="{{ asset('js/fdsoil/Select.js') }}"></script>
<script src="{{ asset('js/fdsoil/Request.js') }}"></script>

<script src="{{ asset('js/Replacement/Replacement.js') }}"></script>
<script src="{{ asset('js/Replacement/Detail.js') }}"></script>
<script src="{{ asset('js/Replacement/Search.js') }}"></script>
<script>
Tabs.tab(0,2);
//!oJSON ? ProductForm.Add.regist() : ProductForm.Edit.regist(oJSON);
//Product.Add.regist();
const changeTheObDeniedWritingImg = (imgObjId) => {
    const objImg=document.getElementById(imgObjId);
    if (objImg.className=='img_show'){
        objImg.setAttribute('title','Ocultar Panel');
        objImg.setAttribute('class','img_hide');
    }
    else{
        objImg.setAttribute('title','Mostrar Panel');
        objImg.setAttribute('class','img_show');
    }
}

</script>
@endsection
