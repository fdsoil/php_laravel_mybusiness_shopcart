@extends('layouts.app')
@section('content')
    <div class="container" >
        <div class="row justify-content-center align-items-center">
            <div class="col-12 d-flex flex-column  bg-white shadow">
                <div class="d-flex justify-content-between align-items-center p-3">
                    <div class="card-header w-100"><h2 class="mb-0">Lista de Reposiciones</h2></div>         
                    <a href="{{ route('replacement.create') }}" class="btn btn-primary font-weight-normal shadow">Nuevo</a>
                </div>
                <table id="tablaRows" class="table table-striped table-bordered compact shadow" style="display:none">
                    <thead>
                        <tr class="table-success text-center">
                            <th>Número</th>
                            <th>Fecha / Hora</th>       
                            <th>Sujeto</th>
                            <th>Descripción</th>
                            <th>Recibido</th>                                              
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($replacements as $replacement)
                        <tr>                            
                            <td>{{ $replacement['number'] }}</td>
                            <td>{{ $replacement['date_time'] }}</td>    
                            <td>{{ $replacement['subject'] }}</td>
                            <td>{{ $replacement['description'] }}</td>
                            <td>{{ $replacement['received'] }}</td>                                               
                        <td  class="d-flex justify-content-around">
                        @if($replacement['received'])
                           <a href="{{ route('replacement.show', $replacement['id']) }}"
                              class="btn btn-success font-weight-bold shadow">Ver</a>
                        @else
                           <a href="{{ route('replacement.edit', $replacement['id']) }}"
                              class="btn btn-primary font-weight-normal shadow">Modificar</a>
                        @endif
        <!--button class="btn btn-danger font-weight-normal"
                data-toggle="modal"
                data-target="{{'#categoryDeleteModal'.$replacement['id']}}">Eliminar</button-->
                {{--@include('category.partials.modal_delete', ['modalId' => 'categoryDeleteModal' . $category->id])--}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                  <div class="text-center" style="margin:1%;">
                     <a type="buttom" class="btn btn-success shadow" href="{{ route('home') }}">Regresar</a>
                  </div>
            </div>
        </div>
    </div>
@endsection
@section('after_scripts')
<script>
    //http://w3.unpocodetodo.info/css3/adaptativas.php   
    $(document).ready(function () {
        $('#tablaRows').DataTable({
            "bJQueryUI":true,
            "bSort":true,            
            "bPaginate":true,
            "sPaginationType":"full_numbers",
            "iDisplayLength": 5,
            "language": {
                "sSearch": "Buscar",
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Disculpe, nada encontrado",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtrado desde _MAX_ registros totales)",               
                "oPaginate": {
                     "sFirst": "Primera",
                     "sPrevious": "Anterior",
                     "sNext": "Siguiente",
                     "sLast": "Última"
                }
            }                  
        });
        $('#tablaRows').show();
    });
</script>
@endsection
