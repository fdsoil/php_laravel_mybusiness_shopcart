@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Editar Cliente') }}</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('client.update', $client->email) }}">
                        @csrf
                        @method('PUT')
                        @include('clients.partials.form', ['client' => $client])
                        <div class="form-group row justify-content-center">
                            <a type="buttom" class="btn btn-success" href="{{ route('client.index') }}">Regresar</a>
                            &nbsp;&nbsp;&nbsp;
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('after_scripts')
  <script src="{{ asset('js/Service.js') }}"></script>
  <script src="{{ asset('js/fdsoil/Select.js') }}"></script>
  <script src="{{ asset('js/fdsoil/WaitElement.js') }}"></script>
  <script>

    const municipalitySearch = function (stateId) {

      Service.municipality(stateId).then(response => {
        const eMunicipality = document.getElementById('municipality');
        const eParish = document.getElementById('parish');
        Select.fill(eMunicipality, response, '0', 'Seleccione...', {id:'id', text:'description'});
        Select.fill(eParish, [], '0', 'Seleccione...', {id:'id', text:'description'});
      });

    }

    const parishSearch = function (municipalityId) {

      Service.parish(municipalityId).then(response => {
        const eParish = document.getElementById('parish');
        Select.fill(eParish, response, '0', 'Seleccione...', {id:'id', text:'description'});
      });

    }

  </script>
@endsection
