@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Ver Cliente') }}</div>
                <div class="card-body">
                        @include('clients.partials.form', ['client' => $client, 'readonly' => true])
                        <div class="form-group row justify-content-center">
                            <a type="buttom" class="btn btn-success" href="{{ route('client.index') }}">Regresar</a>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('after_scripts')
@endsection
