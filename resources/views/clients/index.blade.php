@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 d-flex flex-column align-items-center bg-white shadow">
                <div class="d-flex w-100 justify-content-between align-items-center p-3">
                    <h1 class="mb-0">Clientes</h1>
                    <a href="{{ route('client.create') }}" class="btn btn-primary font-weight-bold">Nuevo</a>
                </div>
                <table class="table table-striped">
                    <thead>
                        <tr class="table-success text-center">
                            <th scope="col">Tipo</th>
                            <th scope="col">Cédula / Rif</th>
                            <th scope="col">Nombre / Razón Social</th>                            
                            <th scope="col">Teléfono</th>
                            <th scope="col">Email</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($clients as $client)
                        <tr>
                            <td>{{ $client['type'] ? 'Natural':'Jurídica' }}</td>                            
                            <td>{{ $client['ced_rif'] }}</td>
                            <td>{{ $client['business_name'] }}</td>
                            <td>{{ $client['phone'] }}</td>
                            <td>{{ $client['email'] }}</td>
                            <td class="d-flex justify-content-around">
                                <a href="{{ route('client.show', $client['email']) }}" class="btn btn-success font-weight-bold">Ver</a>
                                <a href="{{ route('client.edit', $client['email']) }}" class="btn btn-primary font-weight-bold">Modificar</a>
                                <button class="btn btn-danger font-weight-bold" 
                                        data-toggle="modal"
                                        data-target="{{'#clientDeleteModal'.$client['id']}}">Borrar</button>
                                @include('clients.partials.modal_delete', ['modalId' => 'clientDeleteModal'.$client['id']])
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                {!! $clients->links() !!}

            </div>
        </div>
    </div>
@endsection
