<div class="modal fade" id="{{ isset($modalId) ? $modalId : 'modalDelete' }}" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="{{ isset($modalId) ? $modalId : 'modalDeleteLabel' }}" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="{{ isset($modalId) ? $modalId : 'modalDeleteLabel' }}">Eliminar Usario</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          ¿Está seguro de eliminar a este usuario?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <form method="POST" action="{{ route('client.destroy', $client['email']) }}">
            @method('DELETE')
            @csrf
            <button class="btn btn-primary font-weight-bold">Eliminar</button>
          </form>
        </div>
      </div>
    </div>
  </div>
