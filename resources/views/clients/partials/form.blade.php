<div class="form-group row">
    <div class="col-sm-12">
        <label for="email">Correo electrònico del cliente</label>
        <input type="text"
               class="form-control @error('email') is-invalid @enderror"
               id="email"
               name="email"
               placeholder="Email"
               value="{{ isset($client) && $client->email ? $client->email : old('email') }}"               
               {{ isset($readonly) ? "readonly" : null }}>
        @error('email')
            <span class="invalid-feedback" role="alert"> isset($client)
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-12">
        <label for="type">Tipo de persona:</label><br/>       
        <input type="radio"
               id="id_type_natural"
               name="type"               
               value="t" {{ isset($client) && $client->type ? 'checked' : '' }}> Natural
        <input type="radio"
              id="id_type_juridica"
              name="type"              
              value="f" {{ isset($client) && !$client->type ? 'checked' : '' }}> Jurídica        
        @error('type')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">

    <div class="col-sm-3">
        <label for="type">Cèd / Rif</label>
        <select class="form-control @error('letter') is-invalid @enderror" name="letter" id="letter" {{ isset($readonly) ? "readonly" : null }}>
            <option>Seleccione...</option>
            <option {{ isset($client) && substr($client->ced_rif,0,1)=="V" ? "selected" : null }}>V</option>
            <option {{ isset($client) && substr($client->ced_rif,0,1)=="E" ? "selected" : null }}>E</option>
            <option {{ isset($client) && substr($client->ced_rif,0,1)=="J" ? "selected" : null }}>J</option>
            <option {{ isset($client) && substr($client->ced_rif,0,1)=="G" ? "selected" : null }}>G</option>
            <option {{ isset($client) && substr($client->ced_rif,0,1)=="C" ? "selected" : null }}>C</option>
        </select>
    </div>
    <div class="col-sm-9">
        <label for="type">Nº</label>
        <input type="text" 
               class="form-control @error('ced_rif') is-invalid @enderror"
               id="cedrif"
               name="ced_rif"
               placeholder="Cédula o Rif"               
               value="{{ isset($client) && substr($client->ced_rif, 1, strlen($client->ced_rif)) 
                   ? substr($client->ced_rif, 1, strlen($client->ced_rif))                           
                   : substr(old('ced_rif'), 1, strlen(old('ced_rif'))) }}"
               {{ isset($readonly) ? "readonly" : null }}>
        @error('ced_rif')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-12">
        <label for="name">Nombre o razòn social del cliente:</label>
        <input type="text"
               class="form-control @error('business_name') is-invalid @enderror"
               id="business_name"
               name="business_name"
               value="{{ isset($client) && $client->business_name ? $client->business_name : old('business_name') }}"
               {{ isset($readonly) ? "readonly" : null }}>
        @error('business_name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12">
        <label for="name">Teléfono o celular del cliente:</label>
        <input type="text"
               class="form-control @error('phone') is-invalid @enderror"
               id="phone"
               name="phone"
               value="{{ isset($client) && $client->phone ? $client->phone : old('phone') }}"
               {{ isset($readonly) ? "readonly" : null }}>
        @error('phone')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
<div><hr/><p>Direcciòn de Domicilio</p></div>
<div class="form-group row">
    <div class="col-sm-12">
        <label for="state">Estado:</label>
       
        <select class="form-control @error('state') is-invalid @enderror"
                id="state"
                name="state"
                onchange="municipalitySearch(this.value)" {{ isset($readonly) ? "readonly" : null }}>
            <option>Seleccione...</option>                             
            @foreach ($states as $state)
            <option value="{{ $state['id'] }}"
                {{ isset($client) && $state['id'] == $client->state ? "selected" : null }}>
                {{ $state['description'] }}
            </option>
            @endforeach            
        </select>
        @error('state')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12">
        <label for="municipality">Municipio:</label>
        <select class="form-control @error('municipality') is-invalid @enderror"
                id="municipality"
                name="municipality"
                onchange="parishSearch(this.value)" {{ isset($readonly) ? "readonly" : null }}>
            <option>Seleccione...</option>            
            @foreach ($municipalities as $municipality)
            <option value="{{ $municipality['id'] }}"
                {{ isset($client) && $municipality['id'] == $client->municipality ? "selected" : null }}>
                {{ $municipality['description'] }}
            </option>
            @endforeach
        </select>
        @error('municipality')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12">
        <label for="parish">Parroquia:</label>
        <select class="form-control @error('parish') is-invalid @enderror"
                id="parish"
                name="parish" {{ isset($readonly) ? "readonly" : null }}>
            <option>Seleccione...</option>
            @foreach ($parishes as $parish)
            <option value="{{ $parish['id'] }}"
                {{ isset($client) && $parish['id'] == $client->parish ? "selected" : null }}>
                {{ $parish['description'] }}
            </option>
            @endforeach
        </select>
        @error('parish')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12">
        <label for="parish">Tipo de Zona:</label>
        <select class="form-control @error('zone_type') is-invalid @enderror"
                id="zone_type"
                name="zone_type" {{ isset($readonly) ? "readonly" : null }}>
            <option>Seleccione...</option>
            @foreach ($zoneTypes as $zoneType)
            <option value="{{ $zoneType['id'] }}"
                {{ isset($client) && $zoneType['id'] == $client->zone_type ? "selected" : null }}>
                {{ $zoneType['description'] }}
            </option>
            @endforeach
        </select>
        @error('zone_type')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12">
        <label>Nombre Zona:</label>
        <input type="text" 
               class="form-control @error('zone_desc') is-invalid @enderror"
               id="zone_desc"
               name="zone_desc"               
               value="{{ isset($client) && $client->zone_desc ? $client->zone_desc : old('zone_desc') }}"
               {{ isset($readonly) ? "readonly" : null }}>
        @error('zone_desc')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12">
        <label for="parish">Tipo de Calle:</label>
        <select class="form-control @error('route_type') is-invalid @enderror"
                id="route_type"
                name="route_type" {{ isset($readonly) ? "readonly" : null }}>
            <option>Seleccione...</option>
            @foreach ($routeTypes as $routeType)
            <option value="{{ $routeType['id'] }}"
                {{ isset($client) && $routeType['id'] == $client->route_type ? "selected" : null }}>
                {{ $routeType['description'] }}
            </option>
            @endforeach            
        </select>
        @error('route_type')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12">
        <label>Nombre Calle:</label>
        <input type="text" 
               class="form-control @error('route_desc') is-invalid @enderror"
               id="route_desc"
               name="route_desc"
               value="{{ isset($client) && $client->route_desc ? $client->route_desc : old('route_desc') }}"
               {{ isset($readonly) ? "readonly" : null }}>
        @error('route_desc')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12">
        <label for="domicile_type">Tipo de Domicilio:</label>
        <select class="form-control @error('domicile_type') is-invalid @enderror"
                id="domicile_type"
                name="domicile_type" {{ isset($readonly) ? "readonly" : null }}>
            <option>Seleccione...</option>
            @foreach ($domicileTypes as $domicileType)
            <option value="{{ $domicileType['id'] }}"
                {{ isset($client) && $domicileType['id'] == $client->domicile_type ? "selected" : null }}>
                {{ $domicileType['description'] }}
            </option>
            @endforeach 
        </select>
        @error('domicile_type')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12">
        <label>Nombre del Domicilio:</label>
        <input type="text"
               class="form-control @error('home_desc_desc') is-invalid @enderror"
               id="home_desc"
               name="domicile_desc"               
               value="{{ isset($client) && $client->domicile_desc ? $client->domicile_desc : old('domicile_desc') }}"
               {{ isset($readonly) ? "readonly" : null }}>
        @error('domicile_desc')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12">
    <label>Especifique</label>
        <textarea class="form-control @error('specification') is-invalid @enderror"
        id="specification"
        name="specification"
        rows="3"
        placeholder="Especificación"
        {{ isset($readonly) ? "readonly" : null }}>{{ isset($client) 
            && $client->specification 
            ? $client->specification
            : old('specification') }}
        </textarea>
        @error('specification')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
