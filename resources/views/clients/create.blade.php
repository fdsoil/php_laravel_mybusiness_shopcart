@extends('layouts.app')
@section('messages')
    @isset($message)
        @include('layouts.message', ['message' => $message])
    @endisset
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-6 bg-white shadow p-3">                
                <div class="card-header"> <h4>Registrar Cliente</h4> </div>
                <div class="card-body">
                  <form method="POST" action="{{ route('client.store') }}">
                      @csrf
                      @include('clients.partials.form')
                      <div class="text-center">
                          <a type="buttom" class="btn btn-success" href="{{ route('client.index') }}">Regresar</a>
                              &nbsp;&nbsp;&nbsp;
                          <button class="btn btn-primary">Registrar</button>
                      </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('after_scripts')
  <script src="{{ asset('js/Service.js') }}"></script>
  <script src="{{ asset('js/fdsoil/Select.js') }}"></script>
  <script src="{{ asset('js/fdsoil/WaitElement.js') }}"></script>
  <script>

    const municipalitySearch = function (stateId) {

      Service.municipality(stateId).then(response => {
        const eMunicipality = document.getElementById('municipality');
        const eParish = document.getElementById('parish');
        Select.fill(eMunicipality, response, '0', 'Seleccione...', {id:'id', text:'description'});
        Select.fill(eParish, [], '0', 'Seleccione...', {id:'id', text:'description'});
      });

    }

    const parishSearch = function (municipalityId) {

      Service.parish(municipalityId).then(response => {
        const eParish = document.getElementById('parish');
        Select.fill(eParish, response, '0', 'Seleccione...', {id:'id', text:'description'});
      });

    }


  </script>
@endsection

