<div class="modal" id="edit" align="center">
    <div class="row">
        <div class="col-sm-12">
            <h1></h1>
            <table style="width: 50%" id="main">                
                <tr>
                    <th colspan="">Actualizar opción del menú</th>
                <th colspan="">
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button></th>
                </tr>
                <tr>
                    <td colspan="2">
                        <table id="id_table_padre" style="width: 100%"> 
                            <tr v-for="(parent, key) in parents">
                                <td width="50%">Nivel @{{ key }} </td>
                                <td>( @{{ parent }})</td>                                
                            </tr>
                        </table>
                    </td>                   
                </tr>
                <tr class="lospare">
                    <td colspan="2" >
                        <form method="POST" v-on:submit.prevent="updateMenu">
                        <table width="100%">
                            <tr>
                                <td align="left" id="id_td_descripcion" width="50%">
                                    Opción (nivel @{{ nivel }})
                                </td>
                                <td>
                                    <input type="text" name="menu" class="form-control" v-model="menuOption.title" placeholder="Opction...">
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="50%">
                                    Ruta (nivel @{{ nivel }})
                                </td>
                                <td>
                                    <input type="text" name="path" class="form-control" v-model="menuOption.path" placeholder="Path...">
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="50%">
                                    Orden (nivel @{{ nivel }})
                                </td>
                                <td>
                                    <input type="text" name="sort" class="form-control" v-model="menuOption.sort" placeholder="Sort...">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <input type="submit" class="btn btn-primary" value="Guardar">
                                    <!--span v-for="error in errors" class="text-danger">@{{ error }}</span-->
                                </td>
                            </tr>
                        </table>
                        </form>
                    </td>
                </tr>
            </table>                                           
        </div>
    </div>
</div>














