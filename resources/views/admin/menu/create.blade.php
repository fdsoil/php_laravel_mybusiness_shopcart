<div class="modal" id="create" align="center" tabindex="-1" role="dialog">
    <div class="row">
        <div class="col-sm-12">
            <h1></h1>
            <table style="width: 50%" id="main">                
                <tr>
                    <th colspan="">Crear nueva opción del menú</th>
                <th colspan="">
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button></th>
                </tr>
                <tr>
                    <td colspan="2">
                        <table id="id_table_padre" style="width: 100%">                       
                            <tr v-for="(selTex, index) in selTexs">
                                <td width="50%">@{{ selTex.title }}</td>
                                <td>( nivel @{{ selTex.nivel }} )</td>
                                <td>
                                   <span v-if="index==selTexs.length-1"
                                         title="Retroceder un paso"
                                         style="cursor:pointer"
                                         class="glyphicon glyphicon-step-backward"
                                         @click="stepBackward(selTex.id)"><</span>
                                   <span v-else >&nbsp;</span>
                                </td>
                                <td>                                   
                                   <span v-if="index==selTexs.length-1 && index!=0"
                                         title="Retroceder todos los pasos"
                                         class="glyphicon glyphicon-fast-backward"
                                         style="cursor:pointer"
                                         @click="fastBackward()"><<</span>
                                   <span v-else >&nbsp;</span>
                                </td>
                            </tr>
                        </table>
                    </td>                   
                </tr>
                <tr style="display:{DISPLAY}">
                    <td id="id_td_seleccion" width="50%">Selección (nivel @{{ nivel }})</td>
                    <td>
                        <select style="width: 100%;cursor:pointer;"
                                title="Seleccione una opción"
                                v-model="selVal" 
                                @change="stepFrontward(selVal)">
                            <option v-for="(menu,index) in menus" 
                                    :value="menu.id" 
                                    :xlabel="menu.parent_id"
                                    v-bind:selected="index === 0">@{{ menu.title }}</option>
                        </select>
                    </td>
                </tr>
                <tr class="lospare"><td colspan="2"><hr/></td></tr>
                <tr class="lospare">
                    <td colspan="2" >
                        <form method="POST" v-on:submit.prevent="createMenu">
                        <table width="100%">
                            <tr>
                                <td align="left" id="id_td_descripcion" width="50%">
                                    Opción (nivel @{{ nivel }})
                                </td>
                                <td>
                                    <input type="text" name="menu" class="form-control" v-model="newMenu" placeholder="Opction...">
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="50%">
                                    Ruta (nivel @{{ nivel }})
                                </td>
                                <td>
                                    <input type="text" name="path" class="form-control" v-model="newPath" placeholder="Path...">
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="50%">
                                    Orden (nivel @{{ nivel }})
                                </td>
                                <td>
                                    <input type="text" name="sort" class="form-control" v-model="newSort" placeholder="Sort...">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <input type="submit" class="btn btn-primary" value="Guardar">
                                    <!--span v-for="error in errors" class="text-danger">@{{ error }}</span-->
                                </td>
                            </tr>
                        </table>
                        </form>
                    </td>
                </tr>
            </table>                                           
        </div>
    </div>
</div>


