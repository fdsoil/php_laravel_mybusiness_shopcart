@extends('layouts.app')

@section('content')
<link href="{{ asset('css/grid.css') }}" rel="stylesheet">
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h3>Listas de Menú</h3>            
            <template type="text/x-template" id="grid-template">
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th v-for="key in columns"
                               v-if="key!='id'"
                               @click="sortBy(key)"
                               :class="{ active: sortKey == key }">
                                   @{{ key | capitalize }}
                                   <span class="arrow" 
                                         :class="sortOrders[key] > 0 ? 'asc' : 'dsc'"></span>
                            </th>
                            <th>Action(s)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="entry in filteredData">
                            <td v-for="key in columns" v-if="key!='id'">
                                @{{entry[key]}}
                            </td>
                            <td>
                                <a class="btn btn-sm btn-primary" title="Edit.." v-on:click.prevent="editMenu(entry)" href="#">
                                    Edit 
                                </a>
                                <button class="btn btn-sm btn-danger" title="Delete..." v-on:click.prevent="deleteMenu(entry.id)" href="#">
                                    Delete
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </template>
            <div id="demo">
                Buscar <input name="query" v-model="searchQuery">
                    <a class="btn btn-primary float-right"
                       data-toggle="modal"
                       onclick = '$("#create").modal()'
                       href="#">
                       Nueva Opción
                    </a>                      
                <grid 
                    :data="limitGridData | orderByBusinessRules" 
                    :columns="gridColumns"
                    :filter-key="searchQuery">
                </grid>
                <div id="page-navigation">
                    <button @click=movePages(-1)>Back</button>
                    <p>@{{startRow / rowsPerPage + 1}} out of @{{ Math.floor(gridData.length / rowsPerPage) + 1 }}</p>
                    <button @click=movePages(1)>Next</button>
                </div>
            </div>
        </div>
        @include('admin.menu.create')
        @include('admin.menu.edit')
    </div>

</div>
@endsection

@section('after_scripts')
<script src="{{ asset('js/appVue.js') }}"></script>
<script src="{{ asset('js/admin/menu/list.js') }}"></script>
<script src="{{ asset('js/admin/menu/create.js') }}"></script>
<script src="{{ asset('js/admin/menu/update.js') }}"></script>
@endsection
