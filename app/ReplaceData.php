<?php

namespace App;

class ReplaceData
{

    private static function action($request, $string)
    {
        if (!empty($string) && !empty($request)) {
            foreach ($request as $campo => $valor) {
                $fields = "{fld:" . $campo . "}";
                $string = str_replace($fields, $valor, $string);
            }
        }
        return $string;
    }
   
}
