<?php

namespace App\Exceptions;

//use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
//use Throwable;

class PaymentHandler //extends ExceptionHandler
{

    static public function error($pay)
    {    
        return isset($pay->error_list) 
          ? self::responseErrorList($pay->error_list)
            : self::responseErrorCurl($pay);    
    }

    static private function responseErrorList($errorList) {    
        $response = "";    
        if  ($errorList[0]->error_code == "0210") {
            $response = "Disculpe: Error nº ". $errorList[0]->error_code . "; descripción: " . $errorList[0]->description; // "Disculpe: Error interno.";
        } else if  ($errorList[0]->error_code == "0080") {
            $response = "Disculpe: Numero de tarjeta incorrecto.";
        } else if  ($errorList[0]->error_code == "0341") {
            $response = "Disculpe: Cliente informado no es titular del medio de pago.";
        } else if  ($errorList[0]->error_code == "0376") {
            $response = "Disculpe: Saldo insuficiente";
        } else { //return json_encode($pay->error_list[0]->description);
            //$response = "Disculpe: Error desconocido, reporte este còdigo: ". $errorList[0]->error_code;
            $response = "Disculpe: Error nº ". $errorList[0]->error_code . "; descripción: " . $errorList[0]->description ;            
            //Disculpe: Error desconocido, reporte este còdigo: 9000
            //Disculpe: Error desconocido, reporte este còdigo: 9000des: falla en la lectura del archivo de configuracion
            //Disculpe: Error nº 0160; descripción: Monto de la transaccion errado (en ceros o con mas de 2 decimales)
            //Disculpe: Error nº 0049; descripción: Codigo de comercio no existe o sus datos son inconsistetes
            //"Disculpe: Error nº 9014; descripción: Autenticacion errada puede bloquear usuario se puede bloquear por max. numero de intentos fallidos (twofactor_auth) "
            //"Error: num-> 56, des-> OpenSSL SSL_read: SSL_ERROR_SYSCALL, errno 104, status-> 0."
        }        
        return $response;    
    }
    
    static private function responseErrorCurl($pay)
    {    
        $resp = "";
        $resp = 'Error: num-> ' . $pay->errno . ', des-> '. $pay->error . ', status-> ' . $pay->http_status .'.';
         //      Error: num-> 6, des-> Could not resolve host: apimbu.mercantilbanco.com, status-> 0.
         //      Error: num-> 28, des-> Operation timed out after 30000 milliseconds with 0 bytes received, status-> 0.
        return $resp;
    }

}


