<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;
use App\ArticleDetail;

class ReplacementDetail extends Model
{

    protected $hidden = ['user_insert_id', 'user_update_id', 'created_at', 'updated_at'];    

    static public function getDetail($replacementId)
    {
        $replacementDetails = ReplacementDetail::where("replacement_id", $replacementId)->get();
        foreach ($replacementDetails as $replacementDetail) {
            $article = Article::getDetail($replacementDetail->article_id);
            $replacementDetail['int_cod'] = $article['int_cod'];
            $replacementDetail['name'] = $article['name'];
            $replacementDetail['price'] = $article['price'];
            $replacementDetail['stock_min'] = $article['stock_min'];
            $replacementDetail['stock_max'] = $article['stock_max'];
            $replacementDetail['status'] = $article['status'];
            $replacementDetail['photo'] = $article['photo'];            
            $replacementDetail['presentation_ids'] = $article['presentation_ids'];
            $replacementDetail['descript'] = $article['descript'];    
        }
        return $replacementDetails;
    }
    
    /*
    static public function getDetail($replacementId)
    {
        $replacementDetails = ReplacementDetail::where("replacement_id", $replacementId)->get();
        foreach ($replacementDetails as $replacementDetail) {
            $articleDetail = ArticleDetail::getDetail($replacementDetail->article_id);            
            $replacementDetail['presentation_ids'] = $articleDetail['presentation_ids'];
            $replacementDetail['descript'] = $articleDetail['descript'];
        }
        return $replacementDetails;
    }
    */ 
    
    /*static public function getDetail($replacementId)
    {
        $details = ReplacementDetail::where("replacement_id", $replacementId)->get();
        foreach ($details as $detail) {
            $response = Http::get(config('api_url.product') . 'presentation/id/' . $detail->presentation_id)->json();            
            $detail['product'] = $response[0]['name'] . ' ' . $response[0]['mark'];
            $detail['presentation'] = $response[0]['packing_deployment'];
            $detail['category'] = $response[0]['category'];
            $detail['int_cod'] = $response[0]['int_cod'];
        }
        return $details;
    }*/
    
}

