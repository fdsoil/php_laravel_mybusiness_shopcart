<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Replacement extends Model
{
    protected $hidden = ['user_insert_id', 'user_update_id', 'created_at', 'updated_at'];
}
