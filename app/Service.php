<?php

namespace App;
use Illuminate\Support\Facades\Http;

class Service
{
     static public function client($params = [])
     {
         try {
             return Http::get(config('api_url.client'), $params);
         } catch ( \Exception $e ) {
             throw new \App\Exceptions\CustomException( $e->getMessage() );
         }
     }
     
     static public function clientPost($request)
     {
         return Http::withHeaders([
            'X-Requested-With' => 'XMLHttpRequest'
         ])->post(config('api_url.client') . 'email', $request);
     }
     
     static public function clientPut($email, $request)
     {
         return Http::withHeaders([
             'X-Requested-With' => 'XMLHttpRequest'
         ])->put(config('api_url.client') . 'email/' . $email, $request);
     }
     
     static public function clientDelete($email)
     {
         return Http::delete(config('api_url.client'). 'email/' . $email);
     }
     
     static public function geoLocationState()
     {
         return Http::get(config('api_url.common') . 'geo-location/state');
     }
     
     static public function geoLocationMunicipality($stateId)
     {
         return Http::get(config('api_url.common') . 'geo-location/municipality/' . $stateId);
     }
     
     static public function geoLocationParish($municipalityId)
     {
         return Http::get(config('api_url.common') . 'geo-location/parish/' . $municipalityId);
     }     
     
     static public function geoLocationZoneType()
     {
         return Http::get(config('api_url.common') . 'geo-location/zone-type');
     }
     
     static public function geoLocationRouteType()
     {
         return Http::get(config('api_url.common') . 'geo-location/route-type');
     }
     
     static public function geoLocationDomicileType()
     {
         return Http::get(config('api_url.common') . 'geo-location/domicile-type');
     }

}
