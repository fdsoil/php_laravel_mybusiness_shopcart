<?php

namespace App\Http\Actions\Article;

use App\Http\Validator\Article\StoreArticleValidator;
use App\Http\Requests\Article\StoreArticleRequest;
use App\Article;

class StoreArticleAction
{
 
  static public function execute(StoreArticleRequest $request) { 

      $msg  = 'Los datos proporcionados no son válidos';
      $id   = null;
      $code = 202;      
      
      if ( !StoreArticleValidator::rule( $request )->fails() ) {      
            $id   = Article::create($request->all())->id;
            $msg  = 'Nuevo registro creado';
            $code = 201;
      } 
      
      return response()->json(compact('msg', 'id'), $code);

  }
    
}
