<?php

namespace App\Http\Actions\Article;

use App\Http\Validator\Article\UpdateArticleValidator;
use App\Http\Requests\Article\UpdateArticleRequest;
use App\Article;

class UpdateArticleAction
{
 
  static public function execute(UpdateArticleRequest $request, Article $article) { 

      $msg  = 'Los datos proporcionados no son válidos';
      $code = 202;

      if ( !UpdateArticleValidator::rule( $request )->fails() ) {      
          $article->update( $request->except( '_method', 'id' ) );
          $msg  = 'El registro fue actualizado';
          $code = 200;          
      }      

      return response()->json( compact( 'msg' ), $code );

  }
    
}
