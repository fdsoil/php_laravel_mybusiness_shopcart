<?php

namespace App\Http\Validator\Article;

use Illuminate\Support\Facades\Validator;
use App\Http\Requests\Article\UpdateArticleRequest;

class UpdateArticleValidator
{
  
  static public function rule(UpdateArticleRequest $request) {  
  
        $validator = Validator::make($request->all(), [
            'int_cod' => 'required|min:15|max:15|regex:/^[a-zA-Z0-9]+$/',
            'name' => 'required|max:50',
            'stock_min' => 'required|numeric|gt:0',
            'stock_max' => 'required|numeric|gt:0',
            'status' => 'required',
            'price' => 'required|regex:/^\d*(\.\d{0,2})?$/|not_in:0'
        ]);
        
        return $validator;  
  
  }
    
}
