<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Client extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "email" => $this['email'],
            "type" => $this['type'],
            "ced_rif" => $this['ced_rif'],
            "business_name" => $this['business_name'],
            "phone" => $this['phone'],
            "state" => $this['state'],
            "municipality" => $this['municipality'],
            "parish" => $this['parish'],
            "zone_type" => $this['zone_type'],
            "zone_desc" => $this['zone_desc'],
            "route_type" => $this['route_type'],
            "route_desc" => $this['route_desc'],
            "domicile_type" => $this['domicile_type'],
            "domicile_desc" => $this['domicile_desc'],
            "specification" => $this['specification'],
        ];
    }
}
