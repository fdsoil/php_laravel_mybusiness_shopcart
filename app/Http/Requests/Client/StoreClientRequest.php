<?php

namespace App\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

class StoreClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'type' => 'required|not_in:null',
            'letter' => 'string',
            'ced_rif' => 'required|integer',
            'business_name' => 'required|string',
            'phone' => 'required',
            'state' => 'required|integer',
            'municipality' => 'required|integer',
            'parish' => 'required|integer',
            'zone_type' => 'required|integer',
            'zone_desc' => 'required|string',
            'route_type' => 'required|integer',
            'route_desc' => 'required|string',
            'domicile_type' => 'required|integer',
            'domicile_desc' => 'required|string',
            'specification' => 'required|string',
        ];
    }
}
