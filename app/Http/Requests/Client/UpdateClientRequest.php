<?php

namespace App\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

class UpdateClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'email',
            'type' => 'not_in:null',
            'letter' => 'string',
            'ced_rif' => 'string',
            'business_name' => 'string',
            'state' => 'integer|not_in:0',
            'municipality' => 'integer|not_in:0',
            'parish' => 'integer|not_in:0',
            'zone_type' => 'integer|not_in:0',
            'zone_desc' => 'string',
            'route_type' => 'integer|not_in:0',
            'route_desc' => 'string',
            'domicile_type' => 'integer|not_in:0',
            'domicile_desc' => 'string',
            'specification' => 'string',
        ];
    }
}
