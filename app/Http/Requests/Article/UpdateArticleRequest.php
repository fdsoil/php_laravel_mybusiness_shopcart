<?php

namespace App\Http\Requests\Article;

use Illuminate\Foundation\Http\FormRequest;

class UpdateArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        //    'int_cod' => 'required|min:15|max:15|regex:/^[a-zA-Z0-9]+$/',             
        //    'name' => 'required|max:50',
        //    'stock_min' => 'required|numeric|gt:0',
        //    'stock_max' => 'required|numeric|gt:0',
        //    'status' => 'required',
        //    'price' => 'required|regex:/^\d*(\.\d{0,2})?$/|not_in:0'
        ];
    }
    
}
