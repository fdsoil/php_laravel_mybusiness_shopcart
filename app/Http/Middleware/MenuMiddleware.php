<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Menu;

class MenuMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check())
            session()->put('menu', Menu::show());
        return $next($request);
    }
}
