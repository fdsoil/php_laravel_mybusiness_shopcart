<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Http;
use App\Service;
//use App\Client;
use App\Http\Requests\Client\{StoreClientRequest, UpdateClientRequest};
use App\Http\Resources\{ClientCollection, Client as ClientResource};

use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function index()
    {
        $clientsResponse = Service::client()->json();
        $clients = collect($clientsResponse)->paginate(5);
        return view('clients.index', ['clients' => new ClientCollection($clients)]);
    }

    public function create()
    {        
        $states = Service::geoLocationState()->json();
        $zoneTypes = Service::geoLocationZoneType()->json();
        $routeTypes = Service::geoLocationRouteType()->json();
        $domicileTypes = Service::geoLocationDomicileType()->json();    
        return view('clients.create', [            
            'states'         => $states,
            'municipalities' => [],
            'parishes'       => [],
            'zoneTypes'      => $zoneTypes,
            'routeTypes'     => $routeTypes,
            'domicileTypes'  => $domicileTypes
        ]);
    }
    
    public function edit($email)
    {
        $client = Service::client(['email' => $email])->json();
        $states = Service::geoLocationState()->json();
        $municipalities = Service::geoLocationMunicipality($client['state'])->json();
        $parishes = Service::geoLocationParish($client['municipality'])->json();
        $zoneTypes = Service::geoLocationZoneType()->json();
        $routeTypes = Service::geoLocationRouteType()->json();
        $domicileTypes = Service::geoLocationDomicileType()->json();
        return view('clients.edit', [
            'client'         => new ClientResource((object)$client),
            'states'         => $states,
            'municipalities' => $municipalities,
            'parishes'       => $parishes,
            'zoneTypes'      => $zoneTypes,
            'routeTypes'     => $routeTypes,
            'domicileTypes'  => $domicileTypes
        ]);
    }
    
    public function show($email)
    {        
        $client = Service::client(['email' => $email])->json();
        $states = Service::geoLocationState()->json();
        $municipalities = Service::geoLocationMunicipality($client['state'])->json();      
        $parishes = Service::geoLocationParish($client['municipality'])->json();        
        $zoneTypes = Service::geoLocationZoneType()->json();
        $routeTypes = Service::geoLocationRouteType()->json();
        $domicileTypes = Service::geoLocationDomicileType()->json();         
        return view('clients.show', [
            'client'         => new ClientResource((object)$client),
            'states'         => $states,
            'municipalities' => $municipalities,
            'parishes'       => $parishes,
            'zoneTypes'      => $zoneTypes,
            'routeTypes'     => $routeTypes,
            'domicileTypes'  => $domicileTypes
        ]);
    }

    public function store(StoreClientRequest $request)
    {    
        $clientResponse = Service::clientPost($request->except(['_token']));
        if($clientResponse->failed()){           
            return redirect()->route('client.index')->with('error', 'No se pudo crear al cliente');
        }        
        return redirect()->route('client.index')->with('success', 'El cliente fue creado');
    }
    
    public function update(UpdateClientRequest $request, $email)
    {
        $clientResponse = Service::clientPut($email, $request->except(['_token']));
        if($clientResponse->failed()){
            return redirect()->route('client.index')->with('error', 'No se pudo actualizar al cliente');            
        }        
        return redirect()->route('client.index')->with('success', 'El cliente fue actualizado');
    }

    public function destroy($email)
    {        
        $clientResponse = Service::clientDelete($email);
        if($clientResponse->failed()){
            return redirect()->route('client.index')->with('error', 'No se pudo eliminar al cliente');            
        }        
        return redirect()->route('client.index')->with('success', 'El cliente fue eliminado');
    }
}
