<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Menu;

class MenuController extends Controller
{

    public function index()
    { 

        $baseData = Menu::getRecursive();
        return view('admin.menu.index', compact(['baseData']));
    }

    public function edit($id)
    { 
        $baseData = [];
        return view('admin.menu.edit', compact(['baseData', 'id']));
    }

    public function create()
    {     
        $baseData = [];           
        return view('admin.menu.create', compact([ 'baseData']));
    }

    public function children($parentId)
    {
        return Menu::where('parent_id', $parentId)->get();
    }

    public function get($id)
    {
        return Menu::where('id', $id)->get();
    }

    public function recursive()
    {
        return Menu::getRecursive();
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title'     => 'required',
            'parent_id' => 'required',
            'path'      => 'required',
            'sort'      => 'required'
        ]);
        Menu::create($request->all());
        return;
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'title'     => 'required',
            'path'      => 'required',
            'sort'      => 'required'
        ]);

        Menu::find($request->id)->update($request->all());

        return;
    }

    public function destroy($id)
    {
    	$menu = Menu::findOrFail($id);
        $menu->delete();
    }

}


