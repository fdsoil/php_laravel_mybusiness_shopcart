<?php

namespace App\Http\Controllers;

use App\Replacement;
use App\ReplacementDetail;
use Illuminate\Http\Request;


class ReplacementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $replacements = Replacement::all();       
        return view('replacement.index',compact('replacements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('replacement.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $replacement = new Replacement();
        $replacement->subject     = $request->subject;
        $replacement->description = $request->description;
        $replacement->observation = $request->observation;
        $replacement->received    = $request->received;
        $replacement->save();
        $replacement->refresh();
        $msg = 'Nuevo registro creado';
        return response()->json(compact('msg', 'replacement'), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Replacement  $replacement
     * @return \Illuminate\Http\Response
     */
    public function show(Replacement $replacement)
    {
       $details = ReplacementDetail::getDetail($replacement->id)->toArray();     
       return view('replacement.show',compact('replacement', 'details'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Replacement  $replacement
     * @return \Illuminate\Http\Response
     */
    public function edit(Replacement $replacement)
    {
       $details = ReplacementDetail::getDetail($replacement->id);
       return view('replacement.edit',compact('replacement', 'details'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Replacement  $replacement
     * @return \Illuminate\Http\Response
     */
    public function update(Replacement $replacement, Request $request)
    {     
        $replacement = $replacement->find($request->id);
        $replacement->subject     = $request->subject;
        $replacement->description = $request->description;
        $replacement->observation = $request->observation;
        $replacement->received    = $request->received;
        $replacement->save();        
        $msg = 'El registro fue actualizado';
        return response()->json(compact('msg'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Replacement  $replacement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Replacement $replacement)
    {
        //
    }
}
