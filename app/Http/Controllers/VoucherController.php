<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use App\Voucher;
use App\VoucherDetail;
use App\Http\Controllers\PaymentController;
use App\Exceptions\PaymentHandler;
use App\Article;

class VoucherController extends Controller
{

    public function index()
    {       
        $vouchers = Voucher::paginate(5);        
        foreach ($vouchers as $voucher) {
            $response = Http::get(config('api_url.client').$voucher->client_id )->json();
            $voucher['business_name'] = $response['business_name'];            
        }        
        return view('voucher.index',compact('vouchers'));
    }

    public function show(Request $request)
    {
        $voucher = Voucher::find($request->id);
        $response = Http::get(config('api_url.client').$voucher->client_id )->json();
        $voucher['business_name'] = $response['business_name'];
        $voucherDetails = VoucherDetail::select('article_id', 'price', 'quantity')
            ->where('voucher_id', $request->id)
            ->get();           
        foreach ($voucherDetails as $voucherDetail) {
            $article = Article::getDetail($voucherDetail->article_id)->toArray();
            $voucherDetail['int_cod'] = $article['int_cod'];
            $voucherDetail['name'] = $article['name'];
            $voucherDetail['descript'] = $article['descript'];
        }
        return view('voucher.show',compact('voucher', 'voucherDetails'));
    }  
  
    public function register(Request $request)
    {    
        $request->amount = Voucher::amount(json_decode($request->truck));
        $pay = json_decode(PaymentController::pay($request));            
        if (isset($pay->transaction_response) && $pay->transaction_response->trx_status == "approved") {
            $voucher = Voucher::regist($request);
            return response()->json(compact('voucher'), 201);
        } else
            return response()->json(PaymentHandler::error($pay), 503);
    }

}
