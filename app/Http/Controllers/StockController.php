<?php

namespace App\Http\Controllers;

use App\ViewStock;
use Illuminate\Http\Request;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $stocks = ViewStock::getAll();        
        return view('stock.index',compact('stocks'));
    }
    
}
