<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\User\StoreUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\User;
use Hash;

class UsersController extends Controller
{
    private $roles;

    public function __construct()
    {
        $this->roles = [
            ['name' => 'Usuario', 'value' => false],
            ['name' => 'Administrador', 'value' => true]
        ];
    }

    public function index()
    {
        $users = User::orderBy('name')->paginate(5);
        return view('users.index', compact('users'));
    }

    public function create()
    {
        return view('auth.register', [
            'roles' => $this->roles
        ]);
    }

    public function register(StoreUserRequest $request)
    {
        User::create([
            'name' => $request->name,
            'address' => $request->address,
            'email' => $request->email,
            'is_admin' => !is_null($request->is_admin),
            'password' => Hash::make($request->password),
        ]);
        return redirect()->action('UsersController@index')->with('success', 'El usuario fue creado');
    }

    public function show(User $user)
    {
        return view('users.show', [
            'roles' => $this->roles,
            'user' => $user
        ]);
    }

    public function edit(User $user)
    {
        return view('users.edit', [
            'roles' => $this->roles,
            'user' => $user
        ]);
    }

    public function update(User $user, UpdateUserRequest $request)
    {
        $emailIsTaken = User::where('email', $request->email)->whereNotNull('deleted_at')->first();
        $user->name = $request->name ?? $user->name;
        $user->email = $emailIsTaken ? $user->email : $request->email;
        $user->is_admin = !is_null($request->is_admin);
        $user->password = Hash::make($request->password ?? $user->email);
        $user->save();
        session(['success' => 'El usuario fue actualizado']);

        return view('users.edit', [
            'roles' => $this->roles,
            'user' => $user
        ]);
    }

    public function destroy($email)
    {
        $user = User::where('email', $email)->first();
        $user->email = $user->id. " ".$user->email;
        $user->save();
        $user->delete();
        session(['success' => 'El usuario fue eliminado']);
        return redirect()->route('user.index');
    }
}
