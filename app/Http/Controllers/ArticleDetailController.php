<?php

namespace App\Http\Controllers;

use App\ArticleDetail;
use Illuminate\Http\Request;

class ArticleDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($articleId)
    {             
        $articleDetails = ArticleDetail::getFull($articleId);
        return response(json_encode($articleDetails->toArray()), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $articleDetail = new ArticleDetail();
        $articleDetail->article_id = $request->article_id;
        $articleDetail->presentation_id  = $request->presentation_id;
        $articleDetail->quantity = $request->quantity;     
        $articleDetail->save();
        $articleDetails = ArticleDetail::getFull($request->article_id);
        $msg = 'Nuevo registro creado';
        return response()->json(compact('msg', 'articleDetails'), 201);        
        //return response(json_encode($articleDetails->toArray()), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ArticleDetail  $articleDetail
     * @return \Illuminate\Http\Response
     */
    public function show(ArticleDetail $articleDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ArticleDetail  $articleDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(ArticleDetail $articleDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ArticleDetail  $articleDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ArticleDetail $articleDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ArticleDetail  $articleDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArticleDetail $articleDetail)
    {
        $articleId = $articleDetail->article_id;
        $articleDetail->delete();
        $articleDetails = ArticleDetail::getFull($articleId);
        return response()->json(compact('articleDetails'));        
    }
}
