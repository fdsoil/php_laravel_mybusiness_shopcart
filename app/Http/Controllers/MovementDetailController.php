<?php

namespace App\Http\Controllers;

use App\MovementDetail;
use Illuminate\Http\Request;

class MovementDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MovementDetail  $movementDetail
     * @return \Illuminate\Http\Response
     */
    public function show(MovementDetail $movementDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MovementDetail  $movementDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(MovementDetail $movementDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MovementDetail  $movementDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MovementDetail $movementDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MovementDetail  $movementDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(MovementDetail $movementDetail)
    {
        //
    }
}
