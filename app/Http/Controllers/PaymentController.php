<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use App\Payment;

class PaymentController extends Controller
{   
    
    static public function pay(Request $request)    
    {    
        $payment = self::store($request);        
        $request->payment_number = $payment->number;
        
        //invoice_number/////////////////////////////////////////////////////////////////
        //--------------para cifrar el cvv y el codigo telefonico---------------------
        $cvv  = mb_convert_encoding("1234", "UTF-8");
           
        //# Clave secreta enviada por el Banco
        $keybank = mb_convert_encoding('A9279120481620090701AA30', "UTF-8");
           
        //# Generacion del hash a partir de la clave secreta del banco
        $keybankhash = hash("sha256", $keybank, true);
        $sub =substr($keybankhash, 0, 16);
          
        $encodedEncryptedData = base64_encode(openssl_encrypt($cvv, "aes-128-ecb", $sub, OPENSSL_PKCS1_PADDING));
        //print_r($encodedEncryptedData);echo "---->hola";exit;


        $fields = array(
            "merchant_identify" =>array(    
                "integratorId" => 1,
                "merchantId" => 150408, //"merchantId" => 150411,
                "terminalId" => '1',
            ),
            "client_identify" =>array(    
                "ipaddress" => '192.168.81.234', //$model->ip,
                "browser_agent" => 'firefox',//$navegador['nombre'],
                "mobile"=>array(
                    "manufacturer" =>"",
                    "model"=>"",
                    "os_version"=> 'php',//$navegador['plataforma'],
                    //"location"=>array(
                       //"lat"=> 37.422,
                       //"lng"=>-122.084
                    //)
                )
            ),
            /*
              "twofactor_auth" => '2RTFT9ScEcXxiofuCbq+ow==',                
              "cvv" => 'oDc3GLigRsHaaj1+dtILCg==',
              "currency" => 'ves',
            */
            "transaction" =>array(    
                "trx_type" => 'compra',
                "payment_method" => $request->payment_method, //'TDD'
                "card_number" =>  '501878200066287386', //$request->card_number
                "customer_id" => 'V18366876', //$request->customer_id
                "invoice_number" => $request->payment_number, //'14',
                "account_type" => $request->account_type, // 'CC',
                "twofactor_auth" => '2RTFT9ScEcXxiofuCbq+ow==',
                "expiration_date" => $request->expiration_date,//"2021/11"
                "cvv" => 'oDc3GLigRsHaaj1+dtILCg==',
                "currency" => 'ves',
                "amount" => $request->amount //'10.10',
            )
            
            
            /*"transaction" =>array(    
                "trx_type" => 'compra',
                "payment_method" => 'TDD',
                "card_number" =>  '501878200066287386', //$request->card_number
                "customer_id" => 'V18366876', //$request->customer_id
                "invoice_number" => '15',
                "account_type" => 'CC',
                "twofactor_auth" => '2RTFT9ScEcXxiofuCbq+ow==',
                "expiration_date" => '2021/11',
                "cvv" => 'oDc3GLigRsHaaj1+dtILCg==',
                "currency" => 'ves',
                "amount" => '10.10'
            )*/
            
            
            
        );

        $json=  json_encode($fields);
        //print_r($json);exit;

        $url = 'https://apimbu.mercantilbanco.com/mercantil-banco/sandbox/v1/payment/pay';

        //Mostrar las cabeceras del servidor HTTP junto al código HTML.
        $cliente = curl_init();
        curl_setopt_array($cliente, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            //CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $json,
            //CURLOPT_USERAGENT => $_SERVER["HTTP_USER_AGENT"],
            CURLOPT_SSL_VERIFYPEER => false,
            //CURLOPT_SSLVERSION => 5,
            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "content-type: application/json",
                //"x-ibm-client-id: 9860e0f2-ed46-495e-a25f-ef377ea645f6"
                "x-ibm-client-id: 81188330-c768-46fe-a378-ff3ac9e88824"                            
            ),
        ));
        // $respuesta = curl_exec($cliente);
        $response = curl_exec($cliente);
        $err = curl_error($cliente);
        $curl_errno= curl_errno($cliente);
        $http_status = curl_getinfo($cliente, CURLINFO_HTTP_CODE);
        curl_close($cliente);
        
        return $err 
          ? '{"error":"'.$err.'", "errno":"'.$curl_errno.'", "http_status":"'. $http_status .'"}'
            : self::update($response, $payment);        

    }    
   
    static private function store(Request $request)
    {
        $payment = new Payment();
        $payment->trx_type        = 'compra';
        $payment->payment_method  = $request->payment_method; //'TDD'
        $payment->card_number     = '501878200066287386'; //$request->card_number
        $payment->customer_id     = 'V18366876'; //$request->customer_id
        $payment->account_type    = $request->account_type; // 'CC',
        $payment->expiration_date = $request->expiration_date;//"2021/11"
        $payment->amount          = $request->amount; //'10.10',
        $payment->save();
        $payment->refresh();
        return $payment;
    }
    
    static private function update($response, Payment $payment)
    {
        $pay = json_decode($response);
        if (isset($pay->transaction_response) && $pay->transaction_response->trx_status == "approved") {            
            $payment->success = true;
            $payment->save();
        }
        return $response;
    }

}
