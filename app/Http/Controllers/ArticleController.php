<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use App\Http\Requests\Article\StoreArticleRequest;
use App\Http\Actions\Article\StoreArticleAction;
use App\Http\Actions\Article\UpdateArticleAction;
use App\Http\Requests\Article\UpdateArticleRequest;

class ArticleController extends Controller
{

    public function index()
    {   
        return view('article.index');
    }
    
    public function get()
    {            
        $articles = Article::withTrashed()->get();
        return response(json_encode($articles->toArray()), 200);        
    }
    
    public function search(Request $request)
    {   
        $articles = Article::search($request->value);
        return response(json_encode($articles->toArray()), 200);
    }
    
    public function getByPresentation(Request $request)
    {        
        $articles = Article::getByPresentation($request->ids);
        return response(json_encode($articles), 200);
    }

    public function create(){}

    public function store(StoreArticleRequest $request)
    {
      return StoreArticleAction::execute($request);
    }
    /*public function store(Request $request)
    {        
        $article = new Article();
        $article->int_cod     = $request->int_cod;
        $article->name        = $request->name;
        $article->stock_min   = $request->stock_min;
        $article->stock_max   = $request->stock_max;
        $article->status      = $request->status;
        $article->price       = $request->price;
        $article->save();
        $article->refresh();
	    $articleId = $article->id;
        $msg = 'Nuevo registro creado';
        return response()->json(compact('msg', 'articleId'), 201);
    }*/

    public function show(Article $article){}

    public function edit(Article $article){}

    public function update(UpdateArticleRequest $request, Article $article)
    {
        return UpdateArticleAction::execute($request, $article);
    }

    public function destroy(Article $article)
    {
        $article->delete();      
        return response(json_encode(204));       
    }
    
    public function restore(Request $request)
    {
        Article::onlyTrashed()->find($request->id)->restore();     
        return response(json_encode(201));       
    }
    
    public function photo(Request $request)
    {        
        $ext = $request->photo->getClientOriginalExtension();
        $name = 'articleId_' . $request->article_id;   
        $request->photo->storeAs('public/article', $name . '.' . $ext); 
        $article = Article::find($request->article_id);
        $fileName = $name . '.' . $ext;
        $article->photo = $fileName;
        $article->save();
        $msg = 'Archivo guardado con éxito !!';
        return response()->json(compact('msg', 'fileName'), 201);        
    }            
    
}
