<?php

namespace App\Http\Controllers;

use App\ReplacementDetail;
use Illuminate\Http\Request;

class ReplacementDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         
        $replacementDetail = new ReplacementDetail();
        $replacementDetail->replacement_id   = $request->replacement_id;
        $replacementDetail->article_id  = $request->article_id;
        $replacementDetail->amount_requested = $request->amount_requested;        
        $replacementDetail->amount_received = $request->amount_received;        
        $replacementDetail->save();
        $replacementDetails = ReplacementDetail::getDetail($request->replacement_id);        
        $msg = 'Nuevo registro creado';
        return response()->json(compact('msg', 'replacementDetails'), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReplacementDetail  $replacementDetail
     * @return \Illuminate\Http\Response
     */
    public function show(ReplacementDetail $replacementDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReplacementDetail  $replacementDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(ReplacementDetail $replacementDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReplacementDetail  $replacementDetail
     * @return \Illuminate\Http\Response
     */
    public function update(ReplacementDetail $replacementDetail, Request $request)
    {                    
        $replacementDetail = $replacementDetail->find($request->id);
        $replacementDetail->replacement_id   = $request->replacement_id;
        $replacementDetail->article_id  = $request->article_id;
        $replacementDetail->amount_requested = $request->amount_requested;        
        $replacementDetail->amount_received = $request->amount_received; 
        $replacementDetail->save();        
        $replacementDetails = ReplacementDetail::getDetail($request->replacement_id);
        $msg = 'El registro fue actualizado';
        return response()->json(compact('msg', 'replacementDetails'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReplacementDetail  $replacementDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReplacementDetail $replacementDetail)
    {
        $replacementId = $replacementDetail->replacement_id;
        $replacementDetail->delete();
        $replacementDetails = ReplacementDetail::getDetail($replacementId);
        return response()->json(compact('replacementDetails'));
    }
}
