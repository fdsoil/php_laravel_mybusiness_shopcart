<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Voucher;
use App\VoucherDetail;
use App\ViewStock;
use App\Article;
use PDF;

class ShopCartController extends Controller
{
    public function index()
    {
        $stocks=ViewStock::availables();
        return view('shopcart.index', compact('stocks'));
    }
 
    public function list(Request $request)
    {
        $vouchers = Voucher::where('client_id', $request->id)->paginate(5);        

        foreach ($vouchers as $voucher) {
            $response = Http::get(config('api_url.client').$voucher->client_id )->json();
            $voucher['business_name'] = $response['business_name'];            
        }
        
        return view('shopcart.list',compact('vouchers'));
    }
    
    public function show(Request $request)
    {        
        $voucher = Voucher::find($request->id);        
        $response = Http::get(config('api_url.client').$voucher->client_id )->json();
        $voucher['business_name'] = $response['business_name'];              
        $voucherDetails = VoucherDetail::select('article_id', 'price', 'quantity')
            ->where('voucher_id', $request->id)
            ->get();
        foreach ($voucherDetails as $voucherDetail) {       
            $article = Article::getDetail($voucherDetail->article_id)->toArray();
            $voucherDetail['int_cod'] = $article['int_cod'];
            $voucherDetail['name'] = $article['name'];
            $voucherDetail['descript'] = $article['descript'];
        }
        return view('shopcart.show', compact('voucher', 'voucherDetails'));
    }
    
    public function pdf(Request $request)
    {
        $voucher = Voucher::find($request->id);        
        $response = Http::get(config('api_url.client').$voucher->client_id )->json();
        $voucher['business_name'] = $response['business_name'];              
        $voucherDetails = VoucherDetail::select('article_id', 'price', 'quantity')
            ->where('voucher_id', $request->id)
            ->get();
        foreach ($voucherDetails as $voucherDetail) {
            $article = Article::getDetail($voucherDetail->article_id)->toArray();
            $voucherDetail['int_cod'] = $article['int_cod'];
            $voucherDetail['name'] = $article['name'];
            $voucherDetail['descript'] = $article['descript'];
        }        
        $view = \View::make('shopcart.pdf', ['voucher' => $voucher, 'voucherDetails' => $voucherDetails]);
        $html_content = $view->render();        
        PDF::SetTitle("List of users");
        PDF::AddPage();
        PDF::writeHTML($html_content, true, false, true, false, '');        
        //PDF::Output('voucherlist.pdf');
        PDF::Output('voucherlist.pdf', 'D');    
    }
    
}
