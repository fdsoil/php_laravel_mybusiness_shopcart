<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Menu extends Model
{
    protected $fillable = [ 'title', 'parent_id', 'path', 'sort' ];
    
    static private function getAux($dataOld, $dataNew, $parent_id)
    {        
      foreach ($dataOld as $obj) 
        if ($parent_id == $obj->id and !array_search($obj, $dataNew))
          $dataNew[]=$obj;
      return $dataNew;
    }

    static public function get()
    {
       $roleId = Auth::user()->is_admin ? 1 : 2;       
       $roleIds = json_decode(
           DB::select(
               "SELECT array_to_json(array_agg(menu_id)) AS arr
                   FROM public.role_menus WHERE role_id=:role_id",[
                       'role_id' => $roleId,
                   ]
           )[0]->arr
       );
           
       $dataOld = DB::select("SELECT id, title, parent_id, path, sort FROM public.menus ORDER BY 5");       
       foreach ($dataOld as $obj) {
         if (in_array($obj->id, $roleIds)) {
           $dataNew[]=$obj;
           $dataNew = self::getAux($dataOld, $dataNew, $obj->parent_id);         
         }         
       }      
       foreach ($dataNew as $obj)
           $arr[] = json_decode(json_encode($obj), true);
       return $arr;
    }

    static public function getRecursive()
    { //https://laraveldaily.com/eloquent-recursive-hasmany-relationship-with-unlimited-subcategories/
      //https://www.itechempires.com/2019/09/how-to-define-laravel-hasmany-recursive-relationship-with-subitems/amp/
      //https://www.it-swarm.dev/es/mysql/como-crear-una-consulta-recursiva-jerarquica-de-mysql/1042885356/
      //https://www.it-swarm.dev/es/mysql/como-hacer-la-consulta-recursiva-select-en-mysql/1073719458/
      //https://es.stackoverflow.com/questions/63494/consulta-mysql-recursiva
        $sql = "WITH RECURSIVE menus_recursivo(id, title, familia, padre_id, nivel, path, sort) AS 
                   (SELECT raiz.id,
                       raiz.title,
                       ''::character varying::text || raiz.title::text AS familia,
                       raiz.parent_id,
                       1 AS nivel,
                       raiz.path,
                       raiz.sort
                    FROM menus raiz WHERE raiz.parent_id = 0 
                    UNION ALL
                    SELECT hijo.id,
                        hijo.title,
                        concat(concat(padre.familia, ' / '), hijo.title) AS concat,
                        hijo.parent_id,
                        padre.nivel + 1 AS nivel,
                        hijo.path,
                        hijo.sort
                    FROM menus_recursivo padre, menus hijo
                    WHERE padre.id = hijo.parent_id)
                SELECT menus_recursivo.id,
                    menus_recursivo.title,
                    menus_recursivo.familia,
                    menus_recursivo.padre_id,
                    menus_recursivo.nivel,
                    menus_recursivo.path,
                    menus_recursivo.sort
                FROM menus_recursivo
                ORDER BY 5,7;"; // TODO
                //ORDER BY 4,7;"; So works only for user profile
                //ORDER BY 3,7;"; So works only for menu list
        $data = DB::select($sql);
        foreach ($data as $obj)
            $arr[] = json_decode(json_encode($obj), true);
        return json_encode($arr);
    }

    static private function showChildren($data, $row)
    {
        $children = [];
        foreach ($data as $row1)
            if ($row['id'] == $row1['parent_id'])
                $children = array_merge($children, [ array_merge($row1, ['submenu' => self::showChildren($data, $row1) ]) ]);
        return $children;
    }

    static function show()
    {
        $menuAll = [];
        $data    = self::get();
        foreach ($data as $row)
            if ($row['parent_id']==0) {
                $item = [ array_merge($row, ['submenu' => self::showChildren($data, $row) ]) ];
                $menuAll = array_merge($menuAll, $item);
            }
        return $menuAll;
    }    
    
}

