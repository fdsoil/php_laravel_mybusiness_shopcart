<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
//use App\ReplaceData;

class Voucher extends Model
{

    static public function amount($truck)
    {    
         $amount = 0;
         $tax = 0;
         foreach ($truck as $item) 
             $amount += $item[1] * $item[2];         
         $tax = ($amount * 10) / 100;         
         return round($amount + $tax, 2);
    }
    
    static public function regist($request)
    {    
        $truck = str_replace(']', '}', str_replace('[', '{', $request->truck));        
        $vouchersRegist = DB::select('SELECT public.vouchers_regist(:client_id, :subject, :description, :payment_number, :truck)', [
          'client_id'      => $request->client_id,
          'subject'        => $request->subject,
          'description'    => $request->description,
          'payment_number' => $request->payment_number,
          'truck'          => $truck,
        ]);
        return json_decode($vouchersRegist[0]->vouchers_regist);
    }
}


