<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;
use App\Article;

class ArticleDetail extends Model
{

    protected $hidden = ['user_insert_id', 'user_update_id', 'created_at', 'updated_at'];

    public function main()
    {
       return $this->belongTo(Article::class);
    } 
    
    static public function getFull($articleId)
    {
        $articleDetails = ArticleDetail::select()
          ->where('article_id', $articleId)->get();          
        foreach ($articleDetails as $key => $value) {
            $response = Http::get(config('api_url.product') . 'presentation/id/' . $value['presentation_id'])[0];
            $articleDetails[$key]['category'] = $response['category'];
            $articleDetails[$key]['name'] = $response['name'];
            $articleDetails[$key]['mark'] = $response['mark'];
            $articleDetails[$key]['packing_deployment'] = $response['packing_deployment'];                                  
        }
        
        return $articleDetails;    
    }
   
    static public function getDetail($articleId) 
    {
        $articleDetails = ArticleDetail::select('presentation_id','quantity')
            ->where('article_id', $articleId)
            ->get();
            
        $presentation_ids = [];
        $descript = "";
        
        foreach ($articleDetails as $articleDetail) {
            $response = Http::get(config('api_url.product') . 'presentation/id/' . $articleDetail->presentation_id)[0];
            $descript .= ArticleDetail::getDesc($response, $articleDetail->quantity);            
            array_push ($presentation_ids, $response['id']);                        
        }

        return [
            "presentation_ids" => json_encode($presentation_ids),
            "descript"         => substr($descript, 0, strlen($descript) - 3)
        ];
    }
    
    static private function getDesc($presentation, $quantity)
    {
       return $presentation['category'] 
        .' '. $presentation['name'] 
        .' '. $presentation['mark']
        .' '. $quantity
        .' '. $presentation['packing_deployment']
        .' + ' ;        
    }

}
