<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;

class MovementDetail extends Model
{
    static public function getDetail($movementId)
    {
        $movementDetails = MovementDetail::where("movement_id", $movementId)->get();
        foreach ($movementDetails as $movementDetail) {
            $article = Article::getDetail($movementDetail->article_id);
            $movementDetail['int_cod'] = $article['int_cod'];
            $movementDetail['name'] = $article['name'];
            $movementDetail['price'] = $article['price'];
            $movementDetail['stock_min'] = $article['stock_min'];
            $movementDetail['stock_max'] = $article['stock_max'];
            $movementDetail['status'] = $article['status'];
            $movementDetail['photo'] = $article['photo'];            
            $movementDetail['presentation_ids'] = $article['presentation_ids'];
            $movementDetail['descript'] = $article['descript'];   
	}
        return $movementDetails;
    }


    /*static public function getDetail($movementId)
    {
        $details = MovementDetail::where("movement_id", $movementId)->get();
        foreach ($details as $detail) {
            $response = Http::get(config('api_url.product') . 'presentation/id/' . $detail->presentation_id)->json();            
            $detail['product'] = $response[0]['name'] . ' ' . $response[0]['mark'];
            $detail['presentation'] = $response[0]['packing_deployment'];
            $detail['category'] = $response[0]['category'];
            $detail['int_cod'] = $response[0]['int_cod'];
        }
        return $details;
    }*/





}
