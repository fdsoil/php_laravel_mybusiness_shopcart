<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\ArticleDetail;

class Article extends Model
{
    use SoftDeletes; //if ($flight->trashed()) {...} To determine if $flight was logically deleted  
    
    protected $hidden = ['id_user_insert', 'id_user_update', 'created_at', 'updated_at'];
    
    protected $fillable = ['int_cod', 'name', 'stock_min', 'stock_max', 'status', 'price'];

    public function articleDetails()
    {
       return $this->hasMany(ArticleDetail::class);
    } //$articles = Article::find(1)->articleDetails;
        
    static public function getFull()
    {
        $articles = Article::get();
        foreach ($articles as $key => $article) {
            $getFull = ArticleDetail::getFull($article->id);
            $articles[$key]['presentation_ids'] = $getFull['presentation_ids'];
            $articles[$key]['descript']         = $getFull['descript'];
        }        
        return json_encode($articles->toArray());
    }
    
    static public function getDetail($articleId)
    {
        $article = Article::find($articleId);
        $detail = ArticleDetail::getDetail($articleId);        
        $article['presentation_ids'] = $detail['presentation_ids'];
        $article['descript'] = $detail['descript'];
        return $article;
    }
    
    static public function search($value)
    {
        $value = strtoupper($value);
        $articles = Article::select('id', 'photo', 'int_cod', 'name')
          ->where('name', 'like', '%' . $value . '%')
          ->orWhere('int_cod', 'like', '%' . $value . '%')
          ->get();         
        foreach ($articles as $key => $article) {
          $detail = ArticleDetail::getDetail($article->id);          
          $articles[$key]['descript'] = $detail['descript'];
        }
        return $articles;
    }
    
    static public function getByPresentation($ids)
    { 
        $arr = explode(',', $ids);
        $articleDetails = ArticleDetail::select('article_id')
            ->whereIn('presentation_id', $arr)
            ->get()
            ->toArray(); 
        foreach ($articleDetails as $articleDetail)
            $articleId[] = $articleDetail['article_id'];
        $articleId = array_unique($articleId); 
        $articles = Article::whereIn('id', $articleId)->get()->toArray(); 
        foreach ($articles as $key => $article) {
            $getDetail = ArticleDetail::getDetail($article['id']); 
            $articles[$key]['descript'] = $getDetail['descript'];
        } 
        return $articles;
    }
    
    
    

}
