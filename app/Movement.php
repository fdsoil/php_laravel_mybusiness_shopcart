<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movement extends Model
{
    public function getSupportTypeIdAttribute($value)
    {
        return $value == 1 ? 'REPOSICION' : 'COMPRA';
    }
}
