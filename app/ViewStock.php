<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;

class ViewStock extends Model
{
    static public function getAll()
    {
        $stocks = ViewStock::all();        
        foreach ($stocks as $stock) {
            $article = Article::getDetail($stock->article_id);
            $stock['int_cod'] = $article['int_cod'];
            $stock['name'] = $article['name'];
            $stock['photo'] = $article['photo'];            
            $stock['descript'] = $article['descript'];  
        }
        return $stocks;
    }


    /*
    static public function getAll()
    {
        $stocks = ViewStock::all();        
        foreach ($stocks as $stock) {
            $response = Http::get(config('api_url.product') . 'presentation/id/' . $stock->presentation_id)->json();            
            $stock['product'] = $response[0]['category'].' '.$response[0]['name'].' '.$response[0]['mark'];
            $stock['presentation'] = $response[0]['packing_deployment'];
        }
        return $stocks;
    }
     */




    static public function availables()
    {
        $stocks = ViewStock::select('article_id', 'total')->where('total','>','0')->get(); 
	foreach ($stocks as $stock) {
            $article = Article::getDetail($stock->article_id);
            $stock['int_cod'] = $article['int_cod'];
            $stock['name'] = $article['name'];
            $stock['photo'] = $article['photo'];            
            $stock['descript'] = $article['descript'];  
            $stock['presentation_ids'] = $article['presentation_ids'];
	}
        return json_encode($stocks->toArray());
    }
    
    /*static public function availables()
    {
        $stocks = ViewStock::select('presentation_id')->where('total','>','0')->get();
        $availables = [];
        foreach ($stocks as $stock) {
            $availables[] = $stock["presentation_id"];
        }     
        return json_encode($availables);
    }*/
    
}
